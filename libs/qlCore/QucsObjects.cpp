#include "QucsObjects.h"


/* Symbol:
* A Qucs schematic document can have an embedded symbol,
* in that case, the symbol is used to represent this schematic
* in an outer schematic.
* See fet.sch and fet_noise.sch in the example folder:
* fet.sch is a small-signal equivalent of a microwave FET implemented
* as a schematic using RLCs and a VCCS. Being a sub-schematic, it has
* 3 port "components":
*  - name=P1, num=1, type=analog
*  - name=P2, num=2, type=analog
*  - name=P3, num=3, type=analog
* This is actually equivalent to the GraphicalRepresentation and it's PortMapping
* Now Qucs doesn't have a clear concept of ParameterMapping and ParameterDefinition
* instead it relies on the equation blocks:
*  - User defines parameter name/values using an equation block
*  - these parameters can then be referenced in the component's parameter values
*/
