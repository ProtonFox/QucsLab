/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtCore/QDebug>
#include <QtCore/QHashFunctions>

class ObjectUid
{
    Q_DECL_CONSTEXPR explicit ObjectUid(quint64 id) Q_DECL_NOEXCEPT
        : m_value(id)
    {}
public:
    Q_DECL_CONSTEXPR ObjectUid() Q_DECL_NOEXCEPT
        : m_value(0)
    {}

    static ObjectUid createId() Q_DECL_NOEXCEPT;

    Q_DECL_CONSTEXPR bool isNull() const Q_DECL_NOEXCEPT
    {
        return m_value == 0;
    }

    Q_DECL_CONSTEXPR bool operator ==(ObjectUid other) const Q_DECL_NOEXCEPT
    {
        return other.m_value == m_value;
    }

    Q_DECL_CONSTEXPR bool operator !=(ObjectUid other) const Q_DECL_NOEXCEPT
    {
        return !operator ==(other);
    }

    Q_DECL_CONSTEXPR bool operator <(ObjectUid other) const Q_DECL_NOEXCEPT
    {
        return m_value < other.m_value;
    }

    Q_DECL_CONSTEXPR bool operator >(ObjectUid other) const Q_DECL_NOEXCEPT
    {
        return m_value > other.m_value;
    }

    Q_DECL_CONSTEXPR quint64 value() const Q_DECL_NOEXCEPT
    {
        return m_value;
    }

    Q_DECL_CONSTEXPR operator bool() const Q_DECL_NOEXCEPT
    {
        return m_value != 0;
    }

private:
    quint64 m_value;
    friend QDataStream &operator<<(QDataStream &out, const ObjectUid &uid);
    friend QDataStream &operator>>(QDataStream &in, ObjectUid &uid);
};

Q_DECLARE_TYPEINFO(ObjectUid, Q_PRIMITIVE_TYPE);
Q_DECLARE_METATYPE(ObjectUid)

QDebug operator<<(QDebug d, ObjectUid id);
QDataStream &operator<<(QDataStream &out, const ObjectUid &uid);
QDataStream &operator>>(QDataStream &in, ObjectUid &uid);

inline Q_DECL_CONSTEXPR uint qHash(ObjectUid uid, uint seed = 0) Q_DECL_NOEXCEPT
{
    using QT_PREPEND_NAMESPACE(qHash);
    return qHash(uid.value(), seed);
}


class Object: public QObject
{
    Q_OBJECT
    Q_PROPERTY(ObjectUid uid READ uid NOTIFY uidChanged)

public:
    Object(Object *parent = nullptr);
    ~Object();

    Object *parentObject();
    const Object *parentObject() const;

    QList<Object *> childObjects();
    QList<const Object *> childObjects() const;

    ObjectUid uid() const;

    virtual Object *clone() const = 0;

signals:
    void uidChanged(ObjectUid uid);

protected:
    virtual void copyProperties(Object *other) const;

private:
    ObjectUid m_uid;

    // Needed to restore uid for undo/redo
    friend class SymbolDocument;
    void setUid(ObjectUid uid);
};
