
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../libs/qlCore/release/ -lqlCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../libs/qlCore/debug/ -lqlCore
else:unix: LIBS += -L$$OUT_PWD/../../libs/qlCore/ -lqlCore

INCLUDEPATH += $$PWD/../../libs
DEPENDPATH += $$PWD/../../libs/qlCore

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../libs/qlCore/release/libqlCore.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../libs/qlCore/debug/libqlCore.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../libs/qlCore/release/qlCore.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../libs/qlCore/debug/qlCore.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../libs/qlCore/libqlCore.a


win32:CONFIG(release, debug|release): LIBS += -L$$top_builddir/libs/qlCore/release/ -lqlCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$top_builddir/libs/qlCore/debug/ -lqlCore
else:unix: LIBS += -L$$top_builddir/libs/qlCore/ -lqlCore

