#ifndef SCHEMATIC_H
#define SCHEMATIC_H

#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QPointF>

#include "DrawingObjects.h"

namespace sch {


    // TBD: Designable interface
    // QString designator()
    // QString preferredPrefix();

    // TBD: Connectable interface
    // => Port definition, Port mapping, ...

    // TBD: Configurable interface
    // => parameter definition, parameter mapping, parameter values

    // TBD: Visual interface
    // => position, rotation, mirrored, visible, locked, ...

    // TBD: Or use ECS approach, where the entity is a cell and components of an entity are Views

    class DesignatorLabel
    {
    public:
        QPointF position;
        QString text;
    };

    class Parameter
    {
    public:
        QString name;
        bool showName;
        QString value;
        bool showValue;
    };

    class ParameterTable
    {
    public:
        QPointF location;
        QList<Parameter> items;
    };

    class Component
    {
    public:
        QPointF location;
        qreal rotation = 0.0;
        bool isXmirrored = false;

        QString designator;
        ParameterTable parameterTable;

        QString libraryName;
        QString componentName;
        QString symbolName;
    };

    // FIXME: rotation
    class NetLabel
    {
    public:
        QString net;
        QPointF pos;
    };

    // FIXME: mirror
    class NetTie
    {
    public:
        QPointF pos;
        qreal rotation = 0.0;
        bool isXmirrored = false;

        QString net;
        Qs::TieStyle style;
        QColor color;
    };

    class OffSheetPort
    {
    public:
        QPointF pos;
        qreal rotation = 0.0;
        bool isXmirrored = false;

        QString net;
        Qs::PortStyle style;
        QColor color;
    };

    class Wire
    {
    public:
        QPointF p1;
        QPointF p2;
        NetLabel *netLabel = nullptr;
    };

    class Junction
    {
    public:
        QPointF pos;
        NetLabel *netLabel = nullptr;
        QList<Wire *> wires;
        QList<NetTie *> netTies;
        QList<Component *> components;
    };

    class Directive
    {
    public:
        QPointF pos;
        qreal rotation = 0.0;
        bool isXmirrored = false;
        QString designator;
        QString typeName;
        QString text; // FIXME
        QMap<QString, QPair<QString, bool> > parameters; // FIXME
    };

    class Diagram
    {
    public:
        QPointF pos;
        QSizeF size;
        QString text; // FIXME
    };

    class Schematic : public QObject
    {
        Q_OBJECT
    public:
        explicit Schematic(QObject *parent = 0);

        // Hierachical, structural, visual design
        void addComponent(Component *component);
        QList<Component *> components();
        void addWire(Wire *wire);
        QList<Wire *> wires();
        void addJunction(Junction *junction);
        QList<Junction *> junctions();
        void addNetLabel(NetLabel *label);
        QList<NetLabel *> netLabels();
        void addNetTie(NetTie *tie);
        QList<NetTie *> netTies();
        void addDrawing(draw::DrawingObject *drawing);
        QList<draw::DrawingObject *> drawings();

        void setSheetFormat(Qs::SheetFormat format);
        Qs::SheetFormat sheetFormat() const;
        void setSheetTemplateName(const QString &name);
        QString sheetTemplateName() const;
        void setSheetTemplateProperties(const QMap<QString, QString> &properties);
        QMap<QString, QString> sheetTemplateProperties() const;

        // TODO: meta-data:. eg. Author(s), license, ...
        // TODO: Library defines Identification, Attribution and Documentation

        // TODO: Directives and other specials: Measurement, Instrumentation, Simulation, Optimisation, Equations, Diagrams, ...
        void addDirective(Directive *directive);
        QList<Directive *> directives();

        void addDiagram(Diagram *diagram);
        QList<Diagram *> diagrams();

        // TODO: Annotation

        // TODO: Schematic as sub-schematic
        //  - parameter definitions => lib::Parameter and lib::ParameterMapping => Table with pos and items?
        //  - port definitions => lib::Port and lib::PortMapping => Table with pos and items?

        void addPort(OffSheetPort *port);
        QList<OffSheetPort *> ports();
        void addParameter();

    signals:

    public slots:

    private:
        Qs::SheetFormat m_sheetFormat;
        QString m_sheetTemplateName;
        QMap<QString, QString> m_sheetTemplateProperties;
        QList<Component *> m_components;
        QList<Wire *> m_wires;
        QList<Junction *> m_junctions;
        QList<NetLabel *> m_netLabels;
        QList<NetTie *> m_netTies;
        QList<OffSheetPort *> m_ports;
        QList<draw::DrawingObject *> m_drawings;
        QList<Directive *> m_directives;
        QList<Diagram *> m_diagrams;
    };

} // namespace sch

Q_DECLARE_METATYPE(sch::Component*)

#endif // SCHEMATIC_H
