/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "qlCore/QlCore.h"

#include <QtCore/QScopedPointer>
#include <QtGui/QColor>

class QIODevice;
class QString;
class QXmlStreamReader;

namespace sym
{
    class Symbol;
}

class Object;

// TODO: Get rid of this. Use converter instead
class ISymbolReader
{

public:
    ISymbolReader();
    virtual ~ISymbolReader();

    virtual bool readSymbol(QIODevice *device) = 0;
    virtual sym::Symbol *takeSymbol() = 0;

    virtual bool hasError() const = 0;
    virtual QString errorString() const = 0;

};

// TODO: Fragment reader: take a QXmlStreamReader
// and expect it to point to a symbol XML element
class SymbolReaderPrivate;
class SymbolReader: public ISymbolReader
{
    Q_DECLARE_PRIVATE(SymbolReader)
    QScopedPointer<SymbolReaderPrivate> d_ptr;

public:
    SymbolReader();
    ~SymbolReader();

    virtual bool readSymbol(QIODevice *device) override;
    virtual sym::Symbol *takeSymbol() override;
    virtual bool hasError() const override;
    virtual QString errorString() const override;

    bool readSymbol(QXmlStreamReader *xml);
    bool readObjects(QIODevice *device);
    QList<Object *> objects() const;
};

// QucsSymbolReader should returns a QucsSymbol.
// QucsSymbol to Symbol convertion should be handled by a
// QucsSymbolConverter (or a QucsConverter)
class QucsSymbolReader: public ISymbolReader
{
public:
    QucsSymbolReader();
    ~QucsSymbolReader();

    virtual bool readSymbol(QIODevice *device) override;
    virtual sym::Symbol *takeSymbol() override;

    virtual bool hasError() const override;
    virtual QString errorString() const override;

private:
    void createArrow(const QString &line);
    void createRectangle(const QString &line);
    void createEllipse(const QString &line);
    void createArc(const QString &line);
    void createPort(const QString &line);
    void createLine(const QString &line);
    void createText(const QString &line);
    void handleId(const QString &line);
    static qreal distance(int value);
    static Qs::StrokeStyle strokeStyle(int value);
    static Qs::StrokeWidth strokeWidth(int value);
    static QColor color(const QString &value);
    static Qs::FillStyle fillStyle(int value, int filled);
    QIODevice *m_device = nullptr;
    QString m_errorString;
    QScopedPointer<sym::Symbol> m_symbol;
};
