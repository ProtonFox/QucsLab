/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "qlCore/DrawingObjects.h"

namespace sym
{

    // TBD: Use sequence number instead of name, does a name makes sense for a symbol?
    class Port: public draw::DrawingObject
    {
        Q_OBJECT
        Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)

    public:
        explicit Port(Object *parent = nullptr);
        ~Port();

        QString name() const;

    public slots:
        void setName(const QString &name);

    signals:
        void nameChanged(const QString &name);

    private:
        QString m_name;

        // Object interface
    public:
        virtual Object *clone() const override;
    };

    // FIXME: Split b/w Object and DrawingObject
    // In the symbol editor context, Symbol has no location, rotation, ...
    // In the schematics editor context, a symbol *instance* has location, rotation, ...
    class Symbol: public Object
    {
        Q_OBJECT
        Q_PROPERTY(QString caption READ caption WRITE setCaption NOTIFY captionChanged)
        Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)

    public:
        explicit Symbol(Object *parent = nullptr);
        ~Symbol();

        QString caption() const;
        QString description() const;

        // FIXME
        void addDrawing(draw::DrawingObject *drawing);
        QList<draw::DrawingObject *> drawings();

    public slots:
        void setCaption(QString caption);
        void setDescription(QString description);

    signals:
        void captionChanged(QString caption);
        void descriptionChanged(QString description);

    private:
        QString m_caption;
        QString m_description;

        // Object interface
    public:
        virtual Object *clone() const override;
    };

}

// FIXME: UndoCommand is currently Ssym::Symbol specific

// Proxies a read-only Object's properties, and generate
// UpdateCommand when properties are changed
// Allowing to transparently change the properties of a document object
// w/o bypassing the undo framework
class UndoCommand;
class UpdateObjectCommand;
class ObjectProxy: public QObject
{
    Q_OBJECT
public:
    ObjectProxy(QObject *parent = nullptr);
    ~ObjectProxy();

    void setObject(const Object *object);
    const Object *object() const;

    UndoCommand *takeCommand();

signals:
    void commandAvailable();

private:
    const Object *m_object = nullptr;
    QScopedPointer<UpdateObjectCommand> m_command;

    // QObject interface
public:
    virtual bool event(QEvent *event) override;
};
