#ifndef LIBRARYMANAGER_H
#define LIBRARYMANAGER_H

#include <QtCore/QObject>

namespace lib
{
    class Cell;
    class Library;
}

namespace sym
{
    class Symbol;
}

// TODO:
//  - foxus only on library file management
//  - monitor folders for removed/added libraries, emit signals
//  - moniotor loaded library for modification/delete, emit signals
//  - remove cell listing (?)
//  - remove symbol creation

class LibraryManager : public QObject
{
    Q_OBJECT
public:
    explicit LibraryManager(QObject *parent = 0);
    ~LibraryManager();

    QList<QString> systemPathList() const;
    QList<QString> userPathList() const;
    void loadLibraries();
    bool isLoadNeeded() const;

    int categoryCount();
    QList<QString> categories() const;
    QList<lib::Cell*> cells(QString &category) const;
    lib::Cell *cellByName(const QString &modelName, const QString &type = QString());
    lib::Cell *cellByUid(const QString &uid) const;
    sym::Symbol *createSymbol(lib::Cell *cell, const QString &which = "Default") const;
    sym::Symbol *createSymbol(const QString &modelName, const QString &symbolName = "Default") const;

signals:
    void aboutToLoadLibraries();
    void librariesLoaded();
    void loadNeededChanged(bool needed);

public slots:
    void setSystemPathList(const QList<QString> &list);
    void addSystemPath(const QString &path);
    void removeSystemPath(const QString &path);
    void setUserPathList(const QList<QString> &list);
    void addUserPath(const QString &path);
    void removeUserPath(const QString &path);

private:
    void setLoadNeeded(bool needed);
    QList<QString> listLibraryFiles(const QString &path);
    QList<lib::Library *> loadLibraries(const QList<QString> &path);
    void loadLibrary(const QString &path);

    bool m_loadNeeded = false;
    QList<QString> m_systemPathList;
    QList<QString> m_userPathList;
    QList<lib::Library *> m_libraries;
};

#endif // LIBRARYMANAGER_H
