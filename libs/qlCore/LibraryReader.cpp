/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "LibraryObjects.h"
#include "LibraryReader.h"
#include "SymbolReader.h"

#include <QtCore/QDebug>
#include <QtCore/QUuid>
#include <QtCore/QXmlStreamReader>
#include <QtCore/QXmlStreamAttributes>

namespace lib
{

    CellStreamReader::CellStreamReader()
    {

    }

    void CellStreamReader::setXml(QXmlStreamReader *xml)
    {
        m_xml = xml;
    }

    Library *CellStreamReader::readLibrary()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "library");

        m_library = new Library();

        for (const auto &attribute: m_xml->attributes()) {
            if (attribute.name() == "version")
                qt_noop();
            else
                skipAttribute(attribute);
        }

        while (m_xml->readNextStartElement()) {
            if (m_xml->name() == "components") // FIXME: tag name should be "cells"
                m_library->cells = readCells();
            else
                skipCurrentElement();
        }

        return m_library;
    }

    // TODCell
    QList<Cell *> CellStreamReader::readCells()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "components");

        QList<Cell *> cells;

        for (const auto &attribute: m_xml->attributes()) {
            if (attribute.name() == "count")
                qt_noop();
            else
                skipAttribute(attribute);
        }

        while (m_xml->readNextStartElement()) {
            if (m_xml->name() == "component")
                cells.append(readCell());
            else
                skipCurrentElement();
        }

        return cells;
    }

   Cell *CellStreamReader::readCell()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "component");

        m_cell = new Cell();

        for (const auto &attribute: m_xml->attributes()) {
            if (attribute.name() == "uuid")
                m_cell->identification.uid = attribute.value().toString();
            else if (attribute.name() == "caption")
                m_cell->identification.caption = attribute.value().toString();
            else if (attribute.name() == "name")
                qt_noop(); // FIXME
            else if (attribute.name() == "model")
                qt_noop(); // FIXME
            else
                skipAttribute(attribute);
        }

        while (m_xml->readNextStartElement()) {
            if (m_xml->name() == "description")
                m_cell->identification.description = m_xml->readElementText();
            else if (m_xml->name() == "attribution")
                m_cell->attribution = readAttribution();
            else if (m_xml->name() == "documentation")
                m_cell->documentation.text = m_xml->readElementText();
            else if (m_xml->name() == "interface") {
                auto interface = readInterface();
                m_cell->ports = interface.first;
                m_cell->parameters = interface.second;
            }
            else if (m_xml->name() == "implementations") {
                for (auto view: readImplementations())
                m_cell->m_views.append(view);
            }
            else if (m_xml->name() == "representations") {
                for (auto view: readRepresentations())
                    m_cell->m_views.append(view);
            }
            else
                skipCurrentElement();
        }

        return m_cell;
    }

    Attribution CellStreamReader::readAttribution()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "attribution");

        Attribution attribution;

        skipCurrentAttributes();

        while (m_xml->readNextStartElement()) {
            if (m_xml->name() == "statement")
                attribution.statement = m_xml->readElementText();
            else if (m_xml->name() == "license")
                attribution.license = m_xml->readElementText();
            else
                skipCurrentElement();
        }
        return attribution;
    }

    Documentation CellStreamReader::readDocumentation()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "documentation");

        Documentation documentation;

        skipCurrentAttributes();
        documentation.text = m_xml->readElementText();

        return documentation;

    }

    QPair<QList<Port*>, QList<Parameter*> > CellStreamReader::readInterface()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "interface");

        QPair<QList<Port*>, QList<Parameter*> > interface;

        skipCurrentAttributes();

        while (m_xml->readNextStartElement()) {
            if (m_xml->name() == "ports")
                interface.first = readPorts();
            else if (m_xml->name() == "parameters")
                interface.second = readParameters();
            else
                skipCurrentElement();
        }

        return interface;
    }

    QList<SourceCodeView *> CellStreamReader::readImplementations()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "implementations");

        QList<SourceCodeView *> implementations;

        skipCurrentAttributes();

        while (m_xml->readNextStartElement()) {
            if (m_xml->name() == "implementation")
                implementations.append(readImplementation());
            else
                skipCurrentElement();
        }

        return implementations;
    }

    SourceCodeView *CellStreamReader::readImplementation()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "implementation");

        SourceCodeView *implementation = new SourceCodeView;

        for (const auto &attribute: m_xml->attributes()) {
            if (attribute.name() == "caption")
                implementation->identification.caption = attribute.value().toString();
            else
                skipAttribute(attribute);
        }

        while (m_xml->readNextStartElement()) {
            if (m_xml->name() == "description")
                implementation->identification.description = m_xml->readElementText();
            else if (m_xml->name() == "code") {
                for (auto aa: m_xml->attributes())
                    qDebug() << aa.name() << aa.value();
                if (!m_xml->attributes().hasAttribute("language"))
                    raiseError("Code element has no 'language' attribute");
                else
                    implementation->language = m_xml->attributes().value("language").toString();
                implementation->code = m_xml->readElementText();
            }
            else
                skipCurrentElement();
        }

        return implementation;
    }

    QList<SymbolView *> CellStreamReader::readRepresentations()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "representations");

        QList<SymbolView *> representations;

        skipCurrentAttributes();

        while (m_xml->readNextStartElement()) {
            if (m_xml->name() == "representation")
                representations.append(readRepresentation());
            else
                skipCurrentElement();
        }

        return representations;
    }

    SymbolView *CellStreamReader::readRepresentation()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "representation");

        SymbolView *representation = new SymbolView();
        skipCurrentAttributes();

        while (m_xml->readNextStartElement()) {
            if (m_xml->name() == "graphics") {
                SymbolReader reader;
                reader.readSymbol(m_xml);
                sym::Symbol *symbol = reader.takeSymbol();
                representation->identification.caption = symbol->caption();
                representation->identification.description = symbol->description();
                representation->drawings = symbol->drawings();
            }
            else
                skipCurrentElement();
        }

        return representation;
    }

    QList<Port *> CellStreamReader::readPorts()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "ports");

        QList<Port *> ports;

        skipCurrentAttributes();

        while (m_xml->readNextStartElement()) {
            if (m_xml->name() == "port")
                ports.append(readPort());
            else
                skipCurrentElement();
        }

        return ports;
    }

    Port *CellStreamReader::readPort()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "port");

        Port *port = new Port;

        for (const auto &attribute: m_xml->attributes()) {
            if (attribute.name() == "name")
                port->setName(attribute.value().toString());
            else if (attribute.name() == "direction")
                port->setDirection(portDirection(attribute.value().toString()));
            else
                skipAttribute(attribute);
        }

        while (m_xml->readNextStartElement()) {
            skipCurrentElement();
        }

        return port;
    }

    QList<Parameter *> CellStreamReader::readParameters()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "parameters");

        QList<Parameter *> parameters;

        skipCurrentAttributes();

        while (m_xml->readNextStartElement()) {
            if (m_xml->name() == "parameter")
                parameters.append(readParameter());
            else
                skipCurrentElement();
        }

        return parameters;
    }

    Parameter *CellStreamReader::readParameter()
    {
        Q_ASSERT(m_xml->isStartElement() && m_xml->name() == "parameter");

        auto parameter = new Parameter;

        for (const auto &attribute: m_xml->attributes()) {
            if (attribute.name() == "name")
                parameter->setName(attribute.value().toString());
            else if (attribute.name() == "type")
                parameter->setDataType(dataType(attribute.value().toString()));
            else if (attribute.name() == "display")
                parameter->setVisible(boolean(attribute.value().toString()));
            else if (attribute.name() == "unit")
                parameter->setUnit(attribute.value().toString());
            else
                skipAttribute(attribute);
        }

        while (m_xml->readNextStartElement()) {
            if (m_xml->name() == "description")
                parameter->setDocumentation(m_xml->readElementText());
            else if (m_xml->name() == "value")
                parameter->setDefaultValue(m_xml->readElementText());
            else if (m_xml->name() == "restriction")
                parameter->setRestriction(m_xml->readElementText());
            else
                skipCurrentElement();
        }

        return parameter;
    }

    void CellStreamReader::skipCurrentAttributes()
    {
        for (const auto &attribute: m_xml->attributes()) {
            skipAttribute(attribute);
        }
    }

    void CellStreamReader::skipAttribute(QXmlStreamAttribute attribute)
    {
        qWarning() << QString("Skipping unknown attribute '%1'").arg(attribute.name().toString());
    }

    void CellStreamReader::skipCurrentElement()
    {
        qWarning() << QString("Skipping unknown element '%1'").arg(m_xml->name().toString());
        m_xml->skipCurrentElement();
    }

    void CellStreamReader::raiseError(const QString &reason)
    {
        m_xml->raiseError(reason);
    }

    Qs::PortDirection CellStreamReader::portDirection(const QString &text)
    {
        if (text == "input")
            return Qs::InputPort;
        if (text == "output")
            return Qs::OutputPort;
        if (text == "inout")
            return Qs::InputOutputPort;

        raiseError(QString("Invalid port direction: '%1'").arg(text));
        return Qs::InputOutputPort;
    }

    Parameter::DataType CellStreamReader::dataType(const QString &text)
    {
        if (text == "bool")
            return Parameter::BooleanData;
        if (text == "int")
            return Parameter::IntegerData;
        if (text == "real")
            return Parameter::FloatData;
        if (text == "enum")
            return Parameter::EnumData; // FIXME
        if (text == "string")
            return Parameter::SingleLineTextData;
        if (text == "text")
            return Parameter::MultiLineTextData;

        raiseError(QString("Invalid data type: '%1'").arg(text));
        return Parameter::SingleLineTextData;
    }

    bool CellStreamReader::boolean(const QString &text)
    {
        if (text == "true")
            return true;
        if (text == "false")
            return false;

        raiseError(QString("Invalid boolean: '%1'").arg(text));
        return false;
    }

}
