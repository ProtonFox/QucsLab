/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QUuid>

#include "qlCore/SymbolObjects.h"

namespace lib
{
    // TBD: Same as symbol object/document approach, or something more ECS-like?

    class LibObject: public Object
    {
        Q_OBJECT

    public:
        LibObject();
    };

    class Library;
    class Cell;
    class View;
    class SourceCodeView;
    class SymbolView;
    class Parameter;
    class Port;

    // Ultra-simplified port/parameter mapping
    typedef QMap<QString, QString> ParameterMapping;
    typedef QMap<QString, QString> PortMapping;

    class Identification
    {
    public:
        QString uid;
        QString caption;
        QString description;
    };

    class Attribution
    {
    public:
        QString statement;
        QString license;
    };

    class Documentation
    {
    public:
        QString text;
    };

    class Library
    {
    public:
        enum Origin { // Could be moved into Qs NS, as Resource origin?
            UnknownOrigin = 0,
            SystemLibrary,
            UserLibrary,
            ProjectLibrary
        };
        Origin origin = UnknownOrigin;
        QString path;
        Identification identification;
        Attribution attribution;
        Documentation documentation;
        QList<Cell *> cells;
    };

    class View
    {
    public:
        virtual int type() const = 0;

        Identification identification;
        Attribution attribution;
        Documentation documentation;
        ParameterMapping parameterMapping;
        PortMapping portMapping;
    };

    class Cell
    {
    public:
        Identification identification;
        Attribution attribution;
        Documentation documentation;
        QList<Parameter *> parameters;
        QList<Port *> ports;
        QList<View *> m_views;

        template<class T>
        inline QList<T *> views()
        {
            QList<T *> result;
            for (auto view: m_views) {
                if (view->type() != T::Type)
                    continue;
                result.append(static_cast<T*>(view));
            }
            return result;
        }
    };



    // TODO: Parameter data types:
    //  - URL (file)
    //  - RefDes
    // TODO: flags: stored, user, Writable
    // TODO: Name is the identifier, Caption is for a short description, Documentation is for (long) explanation
    class Parameter : public QObject
    {
        Q_OBJECT
        Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
        Q_PROPERTY(DataType dataType READ dataType WRITE setDataType NOTIFY dataTypeChanged)
        Q_PROPERTY(QString unit READ unit WRITE setUnit NOTIFY unitChanged)
        Q_PROPERTY(bool visible READ visible WRITE setVisible NOTIFY visibleChanged)
        Q_PROPERTY(QString documentation READ documentation WRITE setDocumentation NOTIFY documentationChanged)
        Q_PROPERTY(QString restriction READ restriction WRITE setRestriction NOTIFY restrictionChanged)
        Q_PROPERTY(QString defaultValue READ defaultValue WRITE setDefaultValue NOTIFY defaultValueChanged)

    public:
        explicit Parameter(QObject *parent = 0);

        enum DataType {
            BooleanData = 0,
            IntegerData,
            FloatData,
            EnumData,
            SingleLineTextData,
            MultiLineTextData
        };
        Q_ENUM(DataType)

        QString name() const;
        DataType dataType() const;
        QString unit() const;
        bool visible() const;
        QString documentation() const;
        QString defaultValue() const;
        QString restriction() const;

    signals:
        void nameChanged(QString name);
        void dataTypeChanged(DataType dataType);
        void unitChanged(QString unit);
        void visibleChanged(bool visible);
        void documentationChanged(QString documentation);
        void defaultValueChanged(QString defaultValue);
        void restrictionChanged(QString restriction);

    public slots:
        void setName(QString name);
        void setDataType(DataType dataType);
        void setUnit(QString unit);
        void setVisible(bool visible);
        void setDocumentation(QString documentation);
        void setDefaultValue(QString defaultValue);
        void setRestriction(QString restriction);

    private:
        QString m_name;
        DataType m_dataType = SingleLineTextData;
        QString m_unit;
        bool m_visible = true;
        QString m_documentation;
        QString m_defaultValue;
        QString m_restriction;
    };

    class Port : public QObject
    {
        Q_OBJECT

        Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
        Q_PROPERTY(Qs::PortDirection direction READ direction WRITE setDirection NOTIFY directionChanged)
        Q_PROPERTY(QString documentation READ documentation WRITE setDocumentation NOTIFY documentationChanged)

    public:
        explicit Port(QObject *parent = 0);

        Qs::PortDirection direction() const;
        QString name() const;
        QString documentation() const;

    signals:
        void directionChanged(Qs::PortDirection direction);
        void nameChanged(QString name);
        void documentationChanged(QString documentation);

    public slots:
        void setDirection(Qs::PortDirection direction);
        void setName(QString name);
        void setDocumentation(QString documentation);

    private:
        Qs::PortDirection m_direction;
        QString m_name;
        QString m_documentation;
    };


    inline bool operator==(const Identification &lhs, const Identification &rhs)
    {
        return lhs.uid == rhs.uid &&
                lhs.caption == rhs.caption &&
                lhs.description == rhs.description;
    }

    inline bool operator==(const Attribution &lhs, const Attribution &rhs)
    {
        return lhs.statement == rhs.statement &&
                lhs.license == rhs.license;
    }

    inline bool operator==(const Documentation &lhs, const Documentation &rhs)
    {
        return lhs.text == rhs.text;
    }

    class SourceCodeView: public View
    {
    public:
        enum {
            Type= 0x1000 + 0x10
        };

        int type() const override
        {
            return Type;
        }

        // TODO: returns an source code object somehow (?!?)
        QString language;
        QString code;
    };

    class SymbolView: public View
    {
    public:
        enum {
            Type= 0x1000 + 0x20
        };

        int type() const override
        {
            return Type;
        }

        // TODO: returns an sym::Symbol object somehow
        QList<draw::DrawingObject *> drawings;
    };

    class SchematicView: public View
    {
    public:
        enum {
            Type= 0x1000 + 0x20
        };

        int type() const override
        {
            return Type;
        }

        // TODO: returns an sch::Schematic object somehow
    };


}

Q_DECLARE_METATYPE(lib::Identification)
Q_DECLARE_METATYPE(lib::Attribution)
Q_DECLARE_METATYPE(lib::Documentation)
