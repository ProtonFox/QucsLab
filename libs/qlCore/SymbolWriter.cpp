/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "SymbolObjects.h"
#include "SymbolWriter.h"

#include <QtCore/QDebug>
#include <QtCore/QXmlStreamWriter>

#include <cmath>


ISymbolWriter::ISymbolWriter()
{

}

ISymbolWriter::~ISymbolWriter()
{

}

class SymbolWriterPrivate
{
public:
    SymbolWriterPrivate()
        : writer(new QXmlStreamWriter())
    {}

    void writeSymbol(const sym::Symbol *object);
    void writeEllipse(const draw::Ellipse *object);
    void writeLine(const draw::Line *object);
    void writeRectangle(const draw::Rectangle *object);
    void writeLabel(const draw::Label *object);
    void writePort(const sym::Port *object);
    void writeDrawing(const draw::DrawingObject *object);
    void writeStroke(const draw::DrawingObject *object);
    void writeStrokeStyle(Qs::StrokeStyle style);
    void writeStrokeWidth(Qs::StrokeWidth width);
    void writeFill(const draw::DrawingObject *object);
    void writeFillStyle(Qs::FillStyle style);
    void writeEllipseStyle(Qs::EllipseStyle style);
    void writeBoolean(const char *tag, bool value);
    void writeDouble(const char *tag, qreal value);
    void writeColor(const char *tag, const QColor &color);

    QScopedPointer<QXmlStreamWriter> writer;
};

SymbolWriter::SymbolWriter():
    d_ptr(new SymbolWriterPrivate)
{
}

SymbolWriter::~SymbolWriter()
{

}

// FIXME: Error handling?
bool SymbolWriter::writeSymbol(QIODevice *device, const sym::Symbol *symbol)
{
    Q_D(SymbolWriter);

    d->writer->setDevice(device);
    d->writer->setAutoFormatting(true);
    d->writer->writeStartDocument("1.0");
    d->writer->writeDefaultNamespace("http://www.leda.org/xdl");
    d->writer->writeStartElement("symbol");
    d->writer->writeAttribute("version", "1.0");
    d->writer->writeTextElement("caption", symbol->caption());
    d->writer->writeTextElement("description", symbol->description());
    for (const auto object: symbol->childObjects()) {
        auto line = qobject_cast<const draw::Line*>(object);
        auto ellipse = qobject_cast<const draw::Ellipse*>(object);
        auto rectangle = qobject_cast<const draw::Rectangle*>(object);
        auto label = qobject_cast<const draw::Label*>(object);
        auto port = qobject_cast<const sym::Port*>(object);
        if (line != nullptr)
            d->writeLine(line);
        else if (ellipse != nullptr)
            d->writeEllipse(ellipse);
        else if (rectangle != nullptr)
            d->writeRectangle(rectangle);
        else if (label != nullptr)
            d->writeLabel(label);
        else if (port != nullptr)
            d->writePort(port);
        else
            Q_UNREACHABLE();
    }
    d->writer->writeEndElement(); // symbol
    d->writer->writeEndDocument();
    return !d->writer->hasError();
}

bool SymbolWriter::hasError() const
{
    return false; // ?!?
}

QString SymbolWriter::errorString() const
{
    Q_D(const SymbolWriter);

    return d->writer->device()->errorString();
}

// FIXME: Duplicated code with write()
bool SymbolWriter::writeObjects(QIODevice *device, const QList<const QObject *> &objects)
{
    Q_D(SymbolWriter);

    d->writer->setDevice(device);
    d->writer->setAutoFormatting(true);
    for (const auto object: objects) {
        auto line = qobject_cast<const draw::Line*>(object);
        auto ellipse = qobject_cast<const draw::Ellipse*>(object);
        auto rectangle = qobject_cast<const draw::Rectangle*>(object);
        auto label = qobject_cast<const draw::Label*>(object);
        auto port = qobject_cast<const sym::Port*>(object);
        if (line != nullptr)
            d->writeLine(line);
        else if (ellipse != nullptr)
            d->writeEllipse(ellipse);
        else if (rectangle != nullptr)
            d->writeRectangle(rectangle);
        else if (label != nullptr)
            d->writeLabel(label);
        else if (port != nullptr)
            d->writePort(port);
        else
            Q_UNREACHABLE();
    }
    return true;
}

void SymbolWriterPrivate::writeEllipse(const draw::Ellipse *object)
{
    writer->writeStartElement("ellipse");
    writeDrawing(object);
    writeDouble("width", object->size().width());
    writeDouble("height", object->size().height());
    writeEllipseStyle(object->style());
    writeDouble("startAngle", object->startAngle());
    writeDouble("spanAngle", object->spanAngle());
    writeStroke(object);
    writeFill(object);
    writer->writeEndElement();
}

void SymbolWriterPrivate::writeLine(const draw::Line *object)
{
    writer->writeStartElement("line");
    writeDrawing(object);
    writeDouble("x1", object->p1().x() + object->location().x());
    writeDouble("y1", object->p1().y() + object->location().y());
    writeDouble("x2", object->p2().x() + object->location().x());
    writeDouble("y2", object->p2().y() + object->location().y());
    writeStroke(object);
    writer->writeEndElement();
}

void SymbolWriterPrivate::writeRectangle(const draw::Rectangle *object)
{
    writer->writeStartElement("rectangle");
    writeDrawing(object);
    writeDouble("width", object->size().width());
    writeDouble("height", object->size().height());
    writeStroke(object);
    writeFill(object);
    writer->writeEndElement();
}

void SymbolWriterPrivate::writeLabel(const draw::Label *object)
{
    writer->writeStartElement("label");
    writeDrawing(object);
    writer->writeAttribute("text", object->text());
    writeDouble("size", object->size());
    writer->writeAttribute("color", object->color().name());
    writer->writeEndElement();
}

void SymbolWriterPrivate::writePort(const sym::Port *object)
{
    writer->writeStartElement("port");
    writeDrawing(object);
    writer->writeAttribute("name", object->name());
    writer->writeEndElement();
}

void SymbolWriterPrivate::writeDrawing(const draw::DrawingObject *object)
{
    writeDouble("x", object->location().x());
    writeDouble("y", object->location().y());
    if (object->rotation() != 0.0)
        writeDouble("rotation", object->rotation());
    if (object->isLocked())
        writeBoolean("locked", object->isLocked());
    if (object->isMirrored())
        writeBoolean("mirrored", object->isMirrored());
    if (!object->isVisible())
        writeBoolean("visible", object->isVisible());
}

void SymbolWriterPrivate::writeStroke(const draw::DrawingObject *object)
{
    writer->writeStartElement("stroke");
    writeStrokeStyle(object->property("strokeStyle").value<Qs::StrokeStyle>());
    writeStrokeWidth(object->property("strokeWidth").value<Qs::StrokeWidth>());
    writeColor("color", object->property("strokeColor").value<QColor>());
    writer->writeEndElement();
}

void SymbolWriterPrivate::writeFill(const draw::DrawingObject *object)
{
    writer->writeStartElement("fill");
    writeFillStyle(object->property("fillStyle").value<Qs::FillStyle>());
    writeColor("color", object->property("fillColor").value<QColor>());
    writer->writeEndElement();
}

void SymbolWriterPrivate::writeFillStyle(Qs::FillStyle style)
{
    const char *value;
    switch (style) {
        case Qs::SolidFill:
            value = "solid";
            break;
        case Qs::HorLineFill:
            value = "hor";
            break;
        case Qs::VerLineFill:
            value = "ver";
            break;
        case Qs::HorVerLineFill:
            value = "horver";
            break;
        case Qs::FwLineFill:
            value = "fw";
            break;
        case Qs::BwLineFill:
            value = "bw";
            break;
        case Qs::FwBwLineFill:
            value = "fwbw";
            break;
        case Qs::NoFill:
        default:
            value = "none";
            break;
    }
    writer->writeAttribute("style", value);
}

void SymbolWriterPrivate::writeStrokeStyle(Qs::StrokeStyle style)

{
    const char *value;
    switch (style) {
        case Qs::DashStroke:
            value = "dash";
            break;
        case Qs::DotStroke:
            value = "dot";
            break;
        case Qs::DashDotStroke:
            value = "dashdot";
            break;
        case Qs::DasDotDotStroke:
            value = "dashdotdot";
            break;
        case Qs::NoStroke:
            value = "none";
            break;
        case Qs::SolidStroke:
        default:
            value = "solid";
            break;
    }
    writer->writeAttribute("style", value);
}

void SymbolWriterPrivate::writeStrokeWidth(Qs::StrokeWidth width)
{
    const char *value;
    switch (width) {
        case Qs::SmallestStroke:
            value = "smallest";
            break;
        case Qs::SmallStroke:
            value = "small";
            break;
        case Qs::LargeStroke:
            value = "large";
            break;
        case Qs::LargestStroke:
            value = "largest";
            break;
        case Qs::MediumStroke:
        default:
            value = "medium";
            break;
    }
    writer->writeAttribute("width", value);
}

void SymbolWriterPrivate::writeEllipseStyle(Qs::EllipseStyle style)
{
    static const char *name = "style";
    switch (style) {
        case Qs::EllipsoidalArc:
            writer->writeAttribute(name, "arc");
            break;
        case Qs::EllipsoidalPie:
            writer->writeAttribute(name, "pie");
            break;
        case Qs::EllipsoidalChord:
            writer->writeAttribute(name, "chord");
            break;
        case Qs::FullEllipsoid:
            writer->writeAttribute(name, "full");
            break;
    }
}

void SymbolWriterPrivate::writeBoolean(const char *tag, bool value)
{
    writer->writeAttribute(tag, value ? "true" : "false");
}

void SymbolWriterPrivate::writeDouble(const char *tag, qreal value)
{
    writer->writeAttribute(tag, QString::number(value));
}

void SymbolWriterPrivate::writeColor(const char *tag, const QColor &color)
{
    writer->writeAttribute(tag, color.name());
}

QucsSymbolWriter::QucsSymbolWriter()
{

}

QucsSymbolWriter::~QucsSymbolWriter()
{

}

bool QucsSymbolWriter::writeSymbol(QIODevice *device, const sym::Symbol *symbol)
{
    m_device = device;
    m_device->write("<Symbol>\n");
    writeId(symbol);
    for (auto object: symbol->childObjects()) {
        const auto ellipse = qobject_cast<const draw::Ellipse*>(object);
        const auto rectangle = qobject_cast<const draw::Rectangle*>(object);
        const auto port = qobject_cast<const sym::Port*>(object);
        const auto label = qobject_cast<const draw::Label*>(object);
        const auto line = qobject_cast<const draw::Line*>(object);
        if (ellipse != nullptr)
            writeEllipse(ellipse);
        else if (rectangle != nullptr)
            writeRectangle(rectangle);
        else if (port != nullptr)
            writePort(port);
        else if (line != nullptr)
            writeLine(line);
        else if (label != nullptr)
            writeLabel(label);
        else
            qWarning() << QString("Unsupported document object type: %1").arg(object->metaObject()->className());
    }
    m_device->write("</Symbol>\n");
    return true;
}

bool QucsSymbolWriter::hasError() const
{
    return !m_errorString.isEmpty();
}

QString QucsSymbolWriter::errorString() const
{
    return m_errorString;
}

void QucsSymbolWriter::writeId(const sym::Symbol *object)
{
    m_device->write(QString("  <.ID %1 %2 %3>\n")
                    .arg(0)//.arg(object->location().x()) FIXME: designator location
                    .arg(0)//.arg(object->location().y())
                    .arg(object->caption())
                    .toUtf8());
}

void QucsSymbolWriter::writeRectangle(const draw::Rectangle *object)
{
    m_device->write(QString("  <Rectangle %1 %2 %3 %4 %5 %6 %7 %8 %9 %10>\n")
                    .arg(distance(object->location().x()-object->size().width()/2.0))
                    .arg(distance(object->location().y()-object->size().height()/2.0))
                    .arg(distance(object->size().width()))
                    .arg(distance(object->size().height()))
                    .arg(color(object->strokeColor()))
                    .arg(strokeWidth(object->strokeWidth()))
                    .arg(strokeStyle(object->strokeStyle()))
                    .arg(color(object->fillColor()))
                    .arg(fillStyle(object->fillStyle()))
                    .arg(filled(object->fillStyle()))
                    .toUtf8());
}

void QucsSymbolWriter::writeEllipse(const draw::Ellipse *object)
{
    if (object->style() == Qs::FullEllipsoid)
        m_device->write(QString("  <Ellipse %1 %2 %3 %4 %5 %6 %7 %8 %9 %10>\n")
                        .arg(distance(object->location().x()-object->size().width()/2.0))
                        .arg(distance(object->location().y()-object->size().height()/2.0))
                        .arg(distance(object->size().width()))
                        .arg(distance(object->size().height()))
                        .arg(color(object->strokeColor()))
                        .arg(strokeWidth(object->strokeWidth()))
                        .arg(strokeStyle(object->strokeStyle()))
                        .arg(color(object->fillColor()))
                        .arg(fillStyle(object->fillStyle()))
                        .arg(filled(object->fillStyle()))
                        .toUtf8());
    else
        m_device->write(QString("  <EArc %1 %2 %3 %4 %5 %6 %7 %8 %9 %10>\n")
                        .arg(distance(object->location().x()-object->size().width()/2.0))
                        .arg(distance(object->location().y()-object->size().height()/2.0))
                        .arg(distance(object->size().width()))
                        .arg(distance(object->size().height()))
                        .arg(color(object->strokeColor()))
                        .arg(strokeWidth(object->strokeWidth()))
                        .arg(strokeStyle(object->strokeStyle()))
                        .arg(color(object->fillColor()))
                        .arg(fillStyle(object->fillStyle()))
                        .arg(filled(object->fillStyle()))
                        .toUtf8());
}

void QucsSymbolWriter::writeLine(const draw::Line *object)
{
    m_device->write(QString("  <Line %1 %2 %3 %4 %5 %6 %7>\n")
                    .arg(distance(object->location().x()+object->p1().x()))
                    .arg(distance(object->location().y()+object->p1().y()))
                    .arg(distance(object->location().x()+object->p2().x()))
                    .arg(distance(object->location().y()+object->p2().y()))
                    .arg(color(object->strokeColor()))
                    .arg(strokeWidth(object->strokeWidth()))
                    .arg(strokeStyle(object->strokeStyle()))
                    .toUtf8());
}

void QucsSymbolWriter::writeLabel(const draw::Label *object)
{
    m_device->write(QString("  <Text %1 %2 %3 %4 %5 \"%6\">\n")
                    .arg(distance(object->location().x()))
                    .arg(distance(object->location().y()))
                    .arg(textSize(object->size()))
                    .arg(color(object->color()))
                    .arg(angle(object->rotation()))
                    .arg(object->text())
                    .toUtf8());
}

void QucsSymbolWriter::writePort(const sym::Port *object)
{
    m_device->write(QString("  <.PortSym %1 %2 %3 %4>\n")
                    .arg(distance(object->location().x()))
                    .arg(distance(object->location().y()))
                    .arg(object->name())
                    .arg("0") // ?
                    .toUtf8());
}

QString QucsSymbolWriter::distance(qreal value)
{
    return QString::number(std::round(value*5.0));
}

QString QucsSymbolWriter::angle(qreal value)
{
    return QString::number(std::round(value*16));
}

QString QucsSymbolWriter::textSize(qreal value)
{
    return QString::number(std::round(value/2.0)); // FIXME
}

QString QucsSymbolWriter::color(const QColor &value)
{
    return value.name();
}

QString QucsSymbolWriter::strokeWidth(Qs::StrokeWidth value)
{
    switch (value) {
        case Qs::SmallestStroke:
            return "1";
        case Qs::SmallStroke:
            return "2";
        case Qs::MediumStroke:
            return "3";
        case Qs::LargeStroke:
            return "4";
        case Qs::LargestStroke:
            return "5";
        default:
            return "3";
    }
}

QString QucsSymbolWriter::strokeStyle(Qs::StrokeStyle value)
{
    switch (value) {
        case Qs::DashStroke:
            return "2";
        case Qs::DotStroke:
            return "3";
        case Qs::DashDotStroke:
            return "4";
        case Qs::DasDotDotStroke:
            return "5";
        case Qs::NoStroke:
            return "0";
        case Qs::SolidStroke:
        default:
            return "1";
    }
}

QString QucsSymbolWriter::fillStyle(Qs::FillStyle value)
{
    switch (value) {
        case Qs::SolidFill:
            return "1";
        case Qs::HorLineFill:
            return "9";
        case Qs::VerLineFill:
            return "10";
        case Qs::FwLineFill:
            return "13";
        case Qs::BwLineFill:
            return "12";
        case Qs::HorVerLineFill:
            return "11";
        case Qs::FwBwLineFill:
            return "14";
        case Qs::NoFill:
        default:
            return "0";
    }
}

QString QucsSymbolWriter::filled(Qs::FillStyle value)
{
    if (value != Qs::NoFill)
        return "1";
    return "0";
}
