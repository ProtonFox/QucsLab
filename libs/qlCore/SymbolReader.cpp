/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "SymbolObjects.h"
#include "SymbolReader.h"

#include <QtCore/QDebug>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QXmlStreamReader>

ISymbolReader::ISymbolReader()
{

}

ISymbolReader::~ISymbolReader()
{

}

class SymbolReaderPrivate
{
public:
    SymbolReaderPrivate()
    {}

    bool read(QIODevice *device);
    void readSymbol();
    void readLine();
    void readEllipse();
    void readRectangle();
    void readLabel();
    void readPort();
    void readStroke(Object *object);
    void readFill(Object *object);

    Qs::StrokeStyle toStrokeStyle(QStringRef text);
    Qs::StrokeWidth toStrokeWidth(QStringRef text);
    Qs::FillStyle toFillStyle(QStringRef text);
    Qs::EllipseStyle toEllipseStyle(QStringRef text);

    qreal toReal(QStringRef text);
    QColor toColor(QStringRef text);

    void skipCurrentElement();
    void skipAttribute(const QXmlStreamAttribute &attribute);

    QXmlStreamReader *xml;
    QScopedPointer<sym::Symbol> symbol;
};

SymbolReader::SymbolReader()
    : d_ptr(new SymbolReaderPrivate())
{

}

SymbolReader::~SymbolReader()
{

}

bool SymbolReader::readSymbol(QIODevice *device)
{
    Q_D(SymbolReader);
    d->xml = new QXmlStreamReader(device);
    return d->read(device);
}

sym::Symbol *SymbolReader::takeSymbol()
{
    Q_D(SymbolReader);

    return d->symbol.take();
}

bool SymbolReader::hasError() const
{
    Q_D(const SymbolReader);
    return d->xml->hasError();
}

QString SymbolReader::errorString() const
{
    Q_D(const SymbolReader);

    return QObject::tr("%1\nLine %2, column %3")
            .arg(d->xml->errorString())
            .arg(d->xml->lineNumber())
            .arg(d->xml->columnNumber());
}

bool SymbolReader::readSymbol(QXmlStreamReader *xml)
{
    Q_D(SymbolReader);
    d->xml = xml;

    d->symbol.reset(new sym::Symbol());
    while (d->xml->readNextStartElement()) {
        if (d->xml->name() == "caption")
            d->symbol->setCaption(xml->readElementText());
        else if (d->xml->name() == "description")
            d->symbol->setDescription(xml->readElementText());
        else if (d->xml->name() == "ellipse")
            d->readEllipse();
        else if (d->xml->name() == "line")
            d->readLine();
        else if (d->xml->name() == "rectangle")
            d->readRectangle();
        else if (d->xml->name() == "label")
            d->readLabel();
        else if (d->xml->name() == "port")
            d->readPort();
        else
            d->skipCurrentElement();
    }
    return d->xml->hasError();
}

// FIXME: quick hack for copy/paste
bool SymbolReader::readObjects(QIODevice *device)
{
    return readSymbol(device);
}

QList<Object *> SymbolReader::objects() const
{
    Q_D(const SymbolReader);

    return d->symbol->childObjects();
}

bool SymbolReaderPrivate::read(QIODevice *device)
{
    xml->setDevice(device);

    if (xml->readNextStartElement()) {
        if (xml->name() == "symbol" && xml->attributes().value("version") == "1.0")
            readSymbol();
        else
            xml->raiseError(QObject::tr("The file is not an QUCS Symbol version 1.0 file."));
    }
    return !xml->hasError();
}

void SymbolReaderPrivate::readSymbol()
{
    Q_ASSERT(xml->isStartElement() && xml->name() == "symbol");

    symbol.reset(new sym::Symbol());
    while (xml->readNextStartElement()) {
        if (xml->name() == "caption")
            symbol->setCaption(xml->readElementText());
        else if (xml->name() == "description")
            symbol->setDescription(xml->readElementText());
        else if (xml->name() == "ellipse")
            readEllipse();
        else if (xml->name() == "line")
            readLine();
        else if (xml->name() == "rectangle")
            readRectangle();
        else if (xml->name() == "label")
            readLabel();
        else if (xml->name() == "port")
            readPort();
        else
            skipCurrentElement();
    }
    // TODO: check attributes and emit warning
}

void SymbolReaderPrivate::readLine()
{
    Q_ASSERT(xml->isStartElement() && xml->name() == "line");

    auto line = new draw::Line(symbol.data());

    QPointF p1;
    QPointF p2;
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "x1")
            p1.setX(toReal(attribute.value()));
        else if (attribute.name() == "y1")
            p1.setY(toReal(attribute.value()));
        else if (attribute.name() == "x2")
            p2.setX(toReal(attribute.value()));
        else if (attribute.name() == "y2")
            p2.setY(toReal(attribute.value()));
        else
            skipAttribute(attribute);
    }
    line->setP1(p1);
    line->setP2(p2);
    while (xml->readNextStartElement()) {
        if (xml->name() == "stroke")
            readStroke(line);
        else
            skipCurrentElement();
    }
}

void SymbolReaderPrivate::readEllipse()
{
    Q_ASSERT(xml->isStartElement() && xml->name() == "ellipse");

    auto ellipse = new draw::Ellipse(symbol.data());
    QSizeF size;
    QPointF pos;
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "width")
            size.setWidth(toReal(attribute.value()));
        else if (attribute.name() == "height")
            size.setHeight(toReal(attribute.value()));
        else if (attribute.name() == "x")
            pos.setX(toReal(attribute.value()));
        else if (attribute.name() == "y")
            pos.setY(toReal(attribute.value()));
        else if (attribute.name() == "startAngle")
            ellipse->setStartAngle(toReal(attribute.value()));
        else if (attribute.name() == "spanAngle")
            ellipse->setSpanAngle(toReal(attribute.value()));
        else if (attribute.name() == "style")
            ellipse->setStyle(toEllipseStyle(attribute.value()));
        else
            skipAttribute(attribute);
    }
    ellipse->setSize(size);
    ellipse->setLocation(pos);

    while (xml->readNextStartElement()) {
        if (xml->name() == "stroke")
            readStroke(ellipse);
        else if (xml->name() == "fill")
            readFill(ellipse);
        else
            skipCurrentElement();
    }
}

void SymbolReaderPrivate::readRectangle()
{
    Q_ASSERT(xml->isStartElement() && xml->name() == "rectangle");

    auto rectangle = new draw::Rectangle(symbol.data());
    QSizeF size;
    QPointF pos;
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "width")
            size.setWidth(toReal(attribute.value()));
        else if (attribute.name() == "height")
            size.setHeight(toReal(attribute.value()));
        else if (attribute.name() == "x")
            pos.setX(toReal(attribute.value()));
        else if (attribute.name() == "y")
            pos.setY(toReal(attribute.value()));
        else
            skipAttribute(attribute);
    }
    rectangle->setSize(size);
    rectangle->setLocation(pos);

    while (xml->readNextStartElement()) {
        if (xml->name() == "stroke")
            readStroke(rectangle);
        else if (xml->name() == "fill")
            readFill(rectangle);
        else
            skipCurrentElement();
    }
}

void SymbolReaderPrivate::readLabel()
{
    Q_ASSERT(xml->isStartElement() && xml->name() == "label");

    auto label = new draw::Label(symbol.data());
    QPointF pos;
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "x")
            pos.setX(toReal(attribute.value()));
        else if (attribute.name() == "y")
            pos.setY(toReal(attribute.value()));
        else if (attribute.name() == "size")
            label->setSize(toReal(attribute.value()));
        else if (attribute.name() == "text")
            label->setText(attribute.value().toString());
        else if (attribute.name() == "color")
            label->setColor(toColor(attribute.value()));
        else
            skipAttribute(attribute);
    }
    label->setLocation(pos);

    while (xml->readNextStartElement()) {
        skipCurrentElement();
    }
}

void SymbolReaderPrivate::readPort()
{
    Q_ASSERT(xml->isStartElement() && xml->name() == "port");

    auto port = new sym::Port(symbol.data());
    QPointF pos;
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "x")
            pos.setX(toReal(attribute.value()));
        else if (attribute.name() == "y")
            pos.setY(toReal(attribute.value()));
        else if (attribute.name() == "name")
            port->setName(attribute.value().toString());
        else
            skipAttribute(attribute);
    }
    port->setLocation(pos);

    while (xml->readNextStartElement()) {
        skipCurrentElement();
    }
}

void SymbolReaderPrivate::readStroke(Object *object)
{
    Q_ASSERT(xml->name() == "stroke");
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "width")
            object->setProperty("strokeWidth", toStrokeWidth(attribute.value()));
        else if (attribute.name() == "style")
            object->setProperty("strokeStyle", toStrokeStyle(attribute.value()));
        else if (attribute.name() == "color")
            object->setProperty("strokeColor", toColor(attribute.value()));
        else
            skipAttribute(attribute);
    }

    while (xml->readNextStartElement()) {
        skipCurrentElement();
    }
}

void SymbolReaderPrivate::readFill(Object *object)
{
    Q_ASSERT(xml->name() == "fill");
    for (const auto &attribute: xml->attributes()) {
        if (attribute.name() == "style")
            object->setProperty("fillStyle", toFillStyle(attribute.value()));
        else if (attribute.name() == "color")
            object->setProperty("fillColor", toColor(attribute.value()));
        else
            skipAttribute(attribute);
    }

    while (xml->readNextStartElement()) {
        skipCurrentElement();
    }
}

Qs::StrokeStyle SymbolReaderPrivate::toStrokeStyle(QStringRef text)
{
    if (text == "none")
        return Qs::NoStroke;
    if (text == "solid")
        return Qs::SolidStroke;
    if (text == "dash")
        return Qs::DashStroke;
    if (text == "dot")
        return Qs::DotStroke;
    if (text == "dashdot")
        return Qs::DashDotStroke;
    if (text == "dashdotdot")
        return Qs::DasDotDotStroke;

    xml->raiseError(QString("Unknown stroke style: '%1'").arg(text.toString()));
    return Qs::SolidStroke;
}

Qs::StrokeWidth SymbolReaderPrivate::toStrokeWidth(QStringRef text)
{
    if (text == "smallest")
        return Qs::SmallestStroke;
    if (text == "small")
        return Qs::SmallStroke;
    if (text == "medium")
        return Qs::MediumStroke;
    if (text == "large")
        return Qs::LargeStroke;
    if (text == "largest")
        return Qs::LargestStroke;

    xml->raiseError(QString("Unknown stroke width: '%1'").arg(text.toString()));
    return Qs::MediumStroke;
}

Qs::FillStyle SymbolReaderPrivate::toFillStyle(QStringRef text)
{
    if (text == "none")
        return Qs::NoFill;
    if (text == "solid")
        return Qs::SolidFill;
    if (text == "hor")
        return Qs::HorLineFill;
    if (text == "ver")
        return Qs::VerLineFill;
    if (text == "horver")
        return Qs::HorVerLineFill;
    if (text == "fw")
        return Qs::FwLineFill;
    if (text == "bw")
        return Qs::BwLineFill;
    if (text == "fwbw")
        return Qs::FwBwLineFill;

    xml->raiseError(QString("Unknown fill style: '%1'").arg(text.toString()));
    return Qs::NoFill;
}

Qs::EllipseStyle SymbolReaderPrivate::toEllipseStyle(QStringRef text)
{
    if (text == "full")
        return Qs::FullEllipsoid;
    if (text == "arc")
        return Qs::EllipsoidalArc;
    if (text == "pie")
        return Qs::EllipsoidalPie;
    if (text == "chord")
        return Qs::EllipsoidalChord;

    xml->raiseError(QString("Unknown ellipse style: '%1'").arg(text.toString()));
    return Qs::FullEllipsoid;
}

qreal SymbolReaderPrivate::toReal(QStringRef text)
{
    bool ok;
    qreal result = text.toDouble(&ok);
    if (!ok)
        xml->raiseError(QString("Malformed real number: '%1'").arg(text.toString()));
    return result;
}

QColor SymbolReaderPrivate::toColor(QStringRef text)
{
    if (QColor::isValidColor(text.toString()))
        return QColor(text.toString());

    xml->raiseError(QString("Malformed color: '%1'").arg(text.toString()));
    return QColor();
}

void SymbolReaderPrivate::skipCurrentElement()
{
    qWarning() << QString("Line %1, column %2: Skipping unknown element: %3")
                  .arg(xml->lineNumber()).arg(xml->columnNumber()).arg(xml->name().toString());
    xml->skipCurrentElement();
}

void SymbolReaderPrivate::skipAttribute(const QXmlStreamAttribute &attribute)
{
    qWarning() << QString("Line %1, column %2: Skipping unknown attribute: %3::%4")
                  .arg(xml->lineNumber()).arg(xml->columnNumber()).arg(xml->name().toString()).arg(attribute.name().toString());
}


#include <cmath>

QucsSymbolReader::QucsSymbolReader()
{

}

QucsSymbolReader::~QucsSymbolReader()
{

}

bool QucsSymbolReader::readSymbol(QIODevice *device)
{
    m_device = device;
    const auto data = QString::fromUtf8(device->readAll());
    const auto lines = data.split('\n');

    enum State {
        WaitForBeginSymbol = 0,
        WaitForEndSymbol,
        EndOfFile
    };
    State state = WaitForBeginSymbol;

    m_symbol.reset(new sym::Symbol);

    for (const auto input: lines) {
        auto line = input.trimmed();

        if (line.isEmpty())
            continue;

        if (state == EndOfFile) {
            m_errorString = QString("Spurious data after end of symbol: '%1").arg(line);
            return false;
        }

        if (state == WaitForBeginSymbol) {
            if (line == "<Symbol>") {
                state = WaitForEndSymbol;
                continue;
            }
            m_errorString = QString("Expected '<Symbol', got '%1").arg(line);
            return false;
        }

        if (line.startsWith("<Rectangle "))
            createRectangle(line);
        else if (line.startsWith("<Ellipse "))
            createEllipse(line);
        else if (line.startsWith("<EArc "))
            createArc(line);
        else if (line.startsWith("<PortSym ") || line.startsWith("<.PortSym "))
            createPort(line);
        else if (line.startsWith("<Text "))
            createText(line);
        else if (line.startsWith("<Line "))
            createLine(line);
        else if (line.startsWith("<Arrow "))
            createArrow(line);
        else if (line.startsWith("<.ID ")) {
            handleId(line);
        }
        else if (line == "</Symbol>")
            state = EndOfFile;
        else
            qWarning() << "Ignoring data" << line;
    }
    return true;
}

void QucsSymbolReader::createArrow(const QString &line)
{
    auto data = line;
    const auto tokens = data.remove('<').remove('>').split(' ');
    if (tokens.count() != 10) {
        qWarning() << "Ignoring malformed arrow input";
        return;
    }
    int i = 1;
    const qreal x1 = distance(tokens[i++].toInt());
    const qreal y1 = distance(tokens[i++].toInt());
    qreal x2 = distance(tokens[i++].toInt());
    qreal y2 = distance(tokens[i++].toInt());
    qreal x3 = distance(tokens[i++].toInt());
    qreal y3 = distance(tokens[i++].toInt());
    const QString pc = tokens[i++];
    const int pw = tokens[i++].toInt();
    const int ps = tokens[i++].toInt();

    auto beta = std::atan2(y3, x3);
    auto phi = std::atan2(y2, x2);
    auto Length = std::sqrt(y3*y3 + x3*x3);
    x2 = x2 + x1;
    y2 = y2 + y1;
    auto w = beta+phi;
    x3= x2 - Length*std::cos(w);
    y3= y2 - Length*std::sin(w);
    w = phi-beta;
    const qreal x4 = x2 - Length*std::cos(w);
    const qreal y4 = y2 - Length*std::sin(w);

    auto object = new draw::Line(m_symbol.data());
    object->setP1(QPointF(x1, y1));
    object->setP2(QPointF(x2, y2));
    object->setStrokeStyle(strokeStyle(ps));
    object->setStrokeWidth(strokeWidth(pw));
    object->setStrokeColor(color(pc));
    object = new draw::Line(m_symbol.data());
    object->setP1(QPointF(x2, y2));
    object->setP2(QPointF(x3, y3));
    object->setStrokeStyle(strokeStyle(ps));
    object->setStrokeWidth(strokeWidth(pw));
    object->setStrokeColor(color(pc));
    object = new draw::Line(m_symbol.data());
    object->setP1(QPointF(x2, y2));
    object->setP2(QPointF(x4, y4));
    object->setStrokeStyle(strokeStyle(ps));
    object->setStrokeWidth(strokeWidth(pw));
    object->setStrokeColor(color(pc));
}

void QucsSymbolReader::createRectangle(const QString &line)
{
    auto data = line;
    const auto tokens = data.remove('<').remove('>').split(' ');
    if (tokens.count() != 11) {
        qWarning() << "Ignoring malformed rectangle input";
        return;
    }
    auto object = new draw::Rectangle(m_symbol.data());
    int i = 1;
    const int x = tokens[i++].toInt();
    const int y = tokens[i++].toInt();
    const int w = tokens[i++].toInt();
    const int h = tokens[i++].toInt();
    const QString pc = tokens[i++];
    const int pw = tokens[i++].toInt();
    const int ps = tokens[i++].toInt();
    const QString bc = tokens[i++];
    const int bs = tokens[i++].toInt();
    const int bf = tokens[i++].toInt();
    object->setLocation(QPointF(distance(x+w/2.0), distance(y+h/2.0)));
    object->setSize(QSizeF(distance(w), distance(h)));
    object->setStrokeStyle(strokeStyle(ps));
    object->setStrokeWidth(strokeWidth(pw));
    object->setStrokeColor(color(pc));
    object->setFillStyle(fillStyle(bs, bf));
    object->setFillColor(color(bc));
}

void QucsSymbolReader::createEllipse(const QString &line)
{
    auto data = line;
    const auto tokens = data.remove('<').remove('>').split(' ');
    if (tokens.count() != 11) {
        qWarning() << "Ignoring malformed ellipse input";
        return;
    }
    auto object = new draw::Ellipse(m_symbol.data());
    object->setStyle(Qs::FullEllipsoid);
    int i = 1;
    const int x = tokens[i++].toInt();
    const int y = tokens[i++].toInt();
    const int w = tokens[i++].toInt();
    const int h = tokens[i++].toInt();
    const QString pc = tokens[i++];
    const int pw = tokens[i++].toInt();
    const int ps = tokens[i++].toInt();
    const QString bc = tokens[i++];
    const int bs = tokens[i++].toInt();
    const int bf = tokens[i++].toInt();
    object->setLocation(QPointF(distance(x+w/2.0), distance(y+h/2.0)));
    object->setSize(QSizeF(distance(w), distance(h)));
    object->setStrokeStyle(strokeStyle(ps));
    object->setStrokeWidth(strokeWidth(pw));
    object->setStrokeColor(color(pc));
    object->setFillStyle(fillStyle(bs, bf));
    object->setFillColor(color(bc));
}

void QucsSymbolReader::createArc(const QString &line)
{
    auto data = line;
    const auto tokens = data.remove('<').remove('>').split(' ');
    if (tokens.count() != 13) {
        qWarning() << "Ignoring malformed arc input";
        return;
    }
    auto object = new draw::Ellipse(m_symbol.data());
    object->setStyle(Qs::EllipsoidalArc);
    int i = 1;
    const int x = tokens[i++].toInt();
    const int y = tokens[i++].toInt();
    const int w = tokens[i++].toInt();
    const int h = tokens[i++].toInt();
    const int sta = tokens[i++].toInt();
    const int spa = tokens[i++].toInt();
    const QString pc = tokens[i++];
    const int pw = tokens[i++].toInt();
    const int ps = tokens[i++].toInt();
    const QString bc = tokens[i++];
    const int bs = tokens[i++].toInt();
    const int bf = tokens[i++].toInt();
    object->setLocation(QPointF(distance(x+w/2.0), distance(y+h/2.0)));
    object->setSize(QSizeF(distance(w), distance(h)));
    object->setStartAngle(sta/16.0);
    object->setSpanAngle(spa/16.0);
    object->setStrokeStyle(strokeStyle(ps));
    object->setStrokeWidth(strokeWidth(pw));
    object->setStrokeColor(color(pc));
    object->setFillStyle(fillStyle(bs, bf));
    object->setFillColor(color(bc));
}

void QucsSymbolReader::createPort(const QString &line)
{
    auto data = line;
    const auto tokens = data.remove('<').remove('>').split(' ');
    if (tokens.count() != 5) {
        qWarning() << "Ignoring malformed port input";
        return;
    }
    auto object = new sym::Port(m_symbol.data());
    int i = 1;
    const int x = tokens[i++].toInt();
    const int y = tokens[i++].toInt();
    const int n = tokens[i++].toInt();
    // const int u = tokens[i++].toInt(); // Unknown last field: rotation?
    object->setLocation(QPointF(distance(x), distance(y)));
    object->setName(QString::number(n));
}

void QucsSymbolReader::createLine(const QString &line)
{
    auto data = line;
    const auto tokens = data.remove('<').remove('>').split(' ');
    if (tokens.count() != 8) {
        qWarning() << "Ignoring malformed line input";
        return;
    }
    auto object = new draw::Line(m_symbol.data());
    int i = 1;
    const int x1 = tokens[i++].toInt();
    const int y1 = tokens[i++].toInt();
    const int x2 = tokens[i++].toInt();
    const int y2 = tokens[i++].toInt();
    const QString pc = tokens[i++];
    const int pw = tokens[i++].toInt();
    const int ps = tokens[i++].toInt();
    object->setP1(QPointF(distance(x1), distance(y1)));
    object->setP2(QPointF(distance(x1+x2), distance(y1+y2)));
    object->setStrokeStyle(strokeStyle(ps));
    object->setStrokeWidth(strokeWidth(pw));
    object->setStrokeColor(color(pc));
}

// <Text x y size color rotation [^>]+>
void QucsSymbolReader::createText(const QString &line)
{
    auto data = line;
    const auto tokens = data.remove('<').remove('>').split(' ');
    if (tokens.count() < 7) {
        qWarning() << "Ignoring malformed text input";
        return;
    }
    auto object = new draw::Label(m_symbol.data());
    int i = 1;
    const int x = tokens[i++].toInt();
    const int y = tokens[i++].toInt();
    const int s = tokens[i++].toInt();
    const QString c = tokens[i++];
    const int r = tokens[i++].toInt();
    QString text = tokens.mid(i).join(" ");
    if (text.startsWith("\""))
        text.remove(0, 1);
    if (text.endsWith("\""))
        text.remove(text.length()-1, 1);
    object->setLocation(QPointF(distance(x), distance(y)));
    object->setRotation(r/16.0);
    object->setSize(s*2);
    object->setText(text);
    object->setColor(c);
}

void QucsSymbolReader::handleId(const QString &line)
{
    auto data = line;
    const auto tokens = data.remove('<').remove('>').split(' ');
    if (tokens.count() < 4) {
        qWarning() << "Ignoring malformed id input";
        return;
    }
    int i = 1;
    const int x = tokens[i++].toInt();
    const int y = tokens[i++].toInt();
    const QString c = tokens[i++];

    m_symbol->setCaption(c);
    //m_symbol->setLocation(QPointF(x, y)); // FIXME: designator location

    if (tokens.count() > 4) {
        qWarning() << "Ignoring extra id data" << tokens.mid(4);
        return;
    }
}

sym::Symbol *QucsSymbolReader::takeSymbol()
{
    return m_symbol.take();
}

bool QucsSymbolReader::hasError() const
{
    return !m_errorString.isEmpty();
}

QString QucsSymbolReader::errorString() const
{
    return m_errorString;
}

qreal QucsSymbolReader::distance(int value)
{
    return value/5.0;
}

Qs::StrokeStyle QucsSymbolReader::strokeStyle(int value)
{
    if (value == Qt::NoPen)
        return Qs::NoStroke;
    if (value == Qt::SolidLine)
        return Qs::SolidStroke;
    if (value == Qt::DashLine)
        return Qs::DashStroke;
    if (value == Qt::DotLine)
        return Qs::DotStroke;
    if (value == Qt::DashDotLine)
        return Qs::DashDotStroke;
    if (value == Qt::DashDotDotLine)
        return Qs::DasDotDotStroke;
    qWarning() << "Unknown pen style:" << value;
    return Qs::SolidStroke;
}

Qs::StrokeWidth QucsSymbolReader::strokeWidth(int value)
{
    if (value <= 0) {
        qWarning() << "Invalid stroke width:" << value;
        return Qs::MediumStroke;
    }
    if (value <= 1)
        return Qs::SmallestStroke;
    if (value <= 2)
        return Qs::SmallStroke;
    if (value <= 3)
        return Qs::MediumStroke;
    if (value <= 4)
        return Qs::LargeStroke;
    if (value <= 5)
        return Qs::LargestStroke;

    qWarning() << "Invalid stroke width:" << value;
    return Qs::MediumStroke;
}

QColor QucsSymbolReader::color(const QString &value)
{
    return QColor(value);
}

Qs::FillStyle QucsSymbolReader::fillStyle(int value, int filled)
{
    if (filled == 0)
        return Qs::NoFill;
    if (value == Qt::NoBrush)
        return Qs::NoFill;
    if (value == Qt::SolidPattern)
        return Qs::SolidFill;
    if (value == Qt::HorPattern)
        return Qs::HorLineFill;
    if (value == Qt::VerPattern)
        return Qs::VerLineFill;
    if (value == Qt::CrossPattern)
        return Qs::HorVerLineFill;
    if (value == Qt::BDiagPattern)
        return Qs::BwLineFill;
    if (value == Qt::FDiagPattern)
        return Qs::FwLineFill;
    if (value == Qt::DiagCrossPattern)
        return Qs::FwBwLineFill;
    qWarning() << "Unknown brush style:" << value;
    return Qs::NoFill;
}
