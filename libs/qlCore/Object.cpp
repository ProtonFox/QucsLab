/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "Object.h"

#include <QtCore/QVariant>
#include <QtCore/QDataStream>

ObjectUid ObjectUid::createId() Q_DECL_NOEXCEPT
{
#if !defined(Q_ATOMIC_INT64_IS_SUPPORTED)
#error "No atomic support for 64 bits unsigned int"
#endif
    static QBasicAtomicInteger<quint64> next = Q_BASIC_ATOMIC_INITIALIZER(0);
    return ObjectUid(next.fetchAndAddRelaxed(1) + 1);
}

QDebug operator<<(QDebug debug, ObjectUid uid)
{
    debug << uid.value();
    return debug;
}

QDataStream &operator<<(QDataStream &out, const ObjectUid &uid)
{
    out << uid.m_value;
    return out;
}

QDataStream &operator>>(QDataStream &in, ObjectUid &uid)
{
    in >> uid.m_value;
    return in;
}


Object::Object(Object *parent)
    : QObject(parent)
    , m_uid(ObjectUid::createId())
{

}

Object::~Object()
{

}

QList<Object *> Object::childObjects()
{
    return findChildren<Object*>(QString(), Qt::FindDirectChildrenOnly);
}

QList<const Object *> Object::childObjects() const
{
    return findChildren<const Object*>(QString(), Qt::FindDirectChildrenOnly);
}

ObjectUid Object::uid() const
{
    return m_uid;
}

void Object::copyProperties(Object *other) const
{
    Q_UNUSED(other);
}

void Object::setUid(ObjectUid uid)
{
    if (m_uid == uid)
        return;
    m_uid = uid;
    emit uidChanged(m_uid);
}
