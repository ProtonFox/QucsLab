#ifndef QUCSOBJECTS_H
#define QUCSOBJECTS_H

#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QRect>
#include <QtCore/QSize>
#include <QtCore/QString>
#include <QtCore/QPoint>

namespace qucs {
    namespace drawing
    {
        class Pen
        {
        public:
            int style = 0; // Qt::PenStyle
            int width = 0;
            QString color;
        };
        class Brush
        {
        public:
            int style = 0; // Qt::BrushStyle
            QString color;
            bool filled = false;
        };
        class Text
        {
        public:
            QPoint topLeft;
            int pointSize = 12;
            QString color;
            int angle = 0;
            QString text;
        };
        class Rectangle
        {
        public:
            QPoint topLeft;
            QSize size;
            Pen pen;
            Brush brush;
        };
        class Ellipse
        {
        public:
            QPoint topLeft;
            QSize size;
            Pen pen;
            Brush brush;
        };
        class Arc
        {
        public:
            QPoint topLeft;
            QSize size;
            int startAngle = 0;
            int spanAngle = 0;
            Pen pen;
            Brush brush;
        };
        class Line
        {
        public:
            QPoint p1;
            QPoint p2;
            Pen pen;
        };
        class Arrow
        {
        public:
            QPoint tailPos;
            QPoint headPos;
            QSize headSize;
            int headStyle = 1;
            Pen pen;
        };
        class Drawings
        {
        public:
            QList<Rectangle *> rectangles;
            QList<Ellipse *> ellipses;
            QList<Arc *> arcs;
            QList<Text *> texts;
            QList<Arrow *> arrows;
            QList<Line *> lines;
        };
    }
    namespace symbol
    {
        class Port
        {
        public:
            QPoint topLeft;
            QString name;
            int unknownField = 0;
        };
        class Property
        {
        public:
            bool isDisplayed = false;
            QString name;
            QString value;
            QString description;
        };
        class Symbol
        {
        public:
            QPoint topLeft;
            QString identifier;
            QList<Port*> ports;
            QList<Property*> properties;
            drawing::Drawings drawings;
        };
    }
    namespace diagram
    {
        class Marker
        {
        public:
            QList<qreal> values;
            QPoint position;
            int precision = 3;
            int numberType = 0;
            bool isTransparent = false;
        };
        class Graph
        {
        public:
            QString var;
            QString color;
            int thickness = 0;
            int style = 0; // see Qucs enum graphstyle_t
            int precision = 3;
            int numberType = 0; // 0=cartesian, 1=polar/degree, 2=polar/radian
            int yAxisPosition = 0; // // 0=left, 1=right
            QList<Marker *> markers;
        };
        class Axis
        {
        public:
            QString label;
            bool isLogarithmic = false;
            bool isAutoscale = false;
            qreal min = 0.0;
            qreal step = 0.0;
            qreal max = 0.0;
            int rotation = 0;
        };
        class Diagram
        {
        public:
            QString name;
            QPoint leftCenter;
            QSize size;
            int flags = 0; // xAxis.GridOn = 1, hideLines = 2
            QString gridColor;
            int gridStyle = 0; // Qt::PenStyle
            Axis xAxis;
            Axis yAxis;
            Axis zAxis;
            QList<Graph *> graphs;
        };
    }
    namespace schematic
    {
        class Property
        {
        public:
            explicit Property(const QString &value = QString(), bool isDisplayed = false)
                : value(value), isDisplayed(isDisplayed)
            {}

            QString value;
            bool isDisplayed = false;
        };
        class Component
        {
        public:
            explicit Component(const QString &model = QString(), const QString &reference = QString())
                : model(model), reference(reference)
            {}
            QString model;
            QString reference;
            int flags = 0;
            QPoint position;
            QPoint textPosition;
            bool isMirrored = false;
            int rotation = 0;
            // The order of the properties define the mapping with the model
            QList<Property> properties;
        };
        class Label
        {
        public:
            QString name;
            QString value;
            QPoint topLeft;
            int delta = 0;
        };
        class Wire
        {
        public:
            QPoint p1;
            QPoint p2;
            Label net;
        };
        class Schematic
        {
        public:
            // Document
            QString version;
            QRect extent;
            // view
            QPoint viewPos = QPoint(0, 0);
            int viewScale = 1;
            // Grid
            QSize gridSize = QSize(10, 10);
            bool showGrid = true;
            // Frame
            int frameFormat = 0;
            QMap<QString, QString> frameTexts;
            // Extra properties
            QMap<QString, QString> properties;
            // Optional sub-circuit symbol
            symbol::Symbol *symbol = nullptr;
            // The schematic itself
            QList<Component *> components;
            QList<Wire *> wires;
            // Drawings
            drawing::Drawings drawings;
            // Diagrams
            QList<diagram::Diagram *> diagrams;
        };
    }
    namespace library
    {
        class Model
        {
        public:
            QString code;
            QStringList includes;
            // TBD: QMap<QString, QString> properties;
        };
        class Component
        {
        public:
            QString description;
            Model qucsatorModel;
            Model spiceModel;
            symbol::Symbol *symbol = nullptr;
        };
        class Library
        {
        public:
            QString version;
            QString name;
            symbol::Symbol *defaultSymbol;
            QList<Component *> components;
        };
    }
}

#endif // QUCSOBJECTS_H
