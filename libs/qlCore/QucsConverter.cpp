#include "LibraryManager.h"
#include "LibraryObjects.h"
#include "QucsConverter.h"
#include "QucsObjects.h"
#include "SchematicObjects.h"


#include <QtCore/QMap>

#include <cmath>

namespace qucs
{
    Converter::Converter()
    {

    }

    void Converter::setLibraryManager(const LibraryManager *manager)
    {
        m_libraryManager = manager;
    }

    const LibraryManager *Converter::libraryManager() const
    {
        return m_libraryManager;
    }

    sch::Schematic *Converter::convert(const qucs::schematic::Schematic *qucs)
    {
        m_schematic.reset(new sch::Schematic());

        /* TODO:
        schematic->version;
        schematic->properties;
        schematic->viewPos;
        schematic->viewScale;
        schematic->showGrid;
        schematic->gridSize;
        */

        m_schematic->setSheetFormat(sheetFormat(qucs->frameFormat));
        if (qucs->frameFormat != 0) {
            m_schematic->setSheetTemplateName("qucs-0.0.x");
            m_schematic->setSheetTemplateProperties(qucs->frameTexts);
        }
        QPointF topLeft = location(qucs->extent.topLeft());
        m_locationOffset = QPointF(topLeft.x() > 0 ? std::ceil(topLeft.x()) : std::floor(topLeft.x()),
                                   topLeft.y() > 0 ? std::ceil(topLeft.y()) : std::floor(topLeft.y()));

        // TODO: schematic->symbol;

        for (auto qucsComponent: qucs->components) {
            auto tie = createNetTie(qucsComponent);
            if (tie != nullptr) {
                m_schematic->addNetTie(tie);
                continue;
            }
            auto port = createOffSheetPort(qucsComponent);
            if (port != nullptr) {
                m_schematic->addPort(port);
                continue;
            }
            auto directive = createDirective(qucsComponent);
            if (directive != nullptr) {
                m_schematic->addDirective(directive);
                continue;
            }
            auto component = createComponent(qucsComponent);
            if (component != nullptr) {
                m_schematic->addComponent(component);
                continue;
            }
            qWarning() << QString("Unknown component '%1' of model '%2'")
                          .arg(qucsComponent->reference, qucsComponent->model);
        }

        for (auto drawing: createDrawings(qucs->drawings))
            m_schematic->addDrawing(drawing);

        for (auto wire: qucs->wires)
            m_schematic->addWire(createWire(wire));

        // TODO: junctions

        for (auto dia: qucs->diagrams)
            m_schematic->addDiagram(createDiagram(dia));

        return m_schematic.take();
    }

    sch::Wire *Converter::createWire(const schematic::Wire *wire)
    {
        auto result = new sch::Wire();
        result->p1 = location(wire->p1);
        result->p2 = location(wire->p2);
        if (!wire->net.name.isEmpty()) {
            result->netLabel = new sch::NetLabel();
            result->netLabel->pos = location(wire->net.topLeft);
            result->netLabel->net = wire->net.name;
        }
        return result;
    }

    sch::Component *Converter::createComponent(schematic::Component *qucsComponent)
    {
        static const auto hasSubType = QStringList()
                << "BJT" << "_BJT" << "EDD" << "EKV26MOS" << "hic0_full" << "hicumL0V1p2" << "hicumL0V1p2g"
                << "hicumL0V1p3" << "JFET" << "MOSFET" << "_MOSFET" << "RFEDD" << "RFEDD2P" << "vTRRANDOM";
        static const auto hasMultiSymbols = QStringList()
                << "Diode" << "R" << "C" << "MCROSS" << "MTEE";

        // Type is always first, this is Qucs way of dealing with this
        QString subTypeName = "";
        if (hasSubType.contains(qucsComponent->model)) {
            if (qucsComponent->properties.isEmpty()) {
                qWarning() << QString("Component '%1' of model '%2' should not have an empty parameter list"
                                      ", since it requires at least a 'Type' property")
                              .arg(qucsComponent->reference, qucsComponent->model);
            }
            else
                subTypeName = qucsComponent->properties.first().value;
        }

        // Symbol is always last, this is Qucs way of dealing with this
        QString symbolName = "";
        if (hasMultiSymbols.contains(qucsComponent->model)) {
            if (qucsComponent->properties.isEmpty()) {
                qWarning() << QString("Component '%1' of model '%2' should not have an empty parameter list"
                                      ", since it requires at least a 'Symbol' property")
                              .arg(qucsComponent->reference, qucsComponent->model);
            }
            else
                symbolName = qucsComponent->properties.last().value;
        }

        // position, rotation, mirrored
        auto schComponent = new sch::Component();
        schComponent->location = location(qucsComponent->position);
        schComponent->rotation = rotation(qucsComponent);
        schComponent->isXmirrored = qucsComponent->isMirrored;
        schComponent->libraryName = "";
        schComponent->componentName = QString("qucs:%1:%2:").arg(qucsComponent->model, subTypeName);
        schComponent->symbolName = symbolName;
        schComponent->designator = qucsComponent->reference;
        populateParameters(schComponent, qucsComponent);
        //schComponent->parameters; // FIXME
        return schComponent;
    }

    sch::NetTie *Converter::createNetTie(schematic::Component * qucsComponent)
    {
        static const auto names = QList<QString>() << "GND";

        if (names.contains(qucsComponent->model)) {
            auto tie = new sch::NetTie();
            tie->net = "gnd";
            tie->pos = location(qucsComponent->position);
            tie->rotation = rotation(qucsComponent);
            tie->isXmirrored = qucsComponent->isMirrored;
            tie->style = Qs::GroundTie;
            tie->color = QColor(); // ?
            return tie;
        }
        return nullptr;
    }

    sch::OffSheetPort *Converter::createOffSheetPort(schematic::Component * qucsComponent)
    {
        static const auto names = QList<QString>() << "Port";

        if (names.contains(qucsComponent->model)) {
            Q_ASSERT(qucsComponent->properties.count() >= 2);
            auto port = new sch::OffSheetPort();
            port->net = qucsComponent->properties.first().value;
            port->pos = location(qucsComponent->position);
            port->rotation = rotation(qucsComponent);
            port->isXmirrored = qucsComponent->isMirrored;
            port->style = Qs::CirclePort;
            port->color = QColor(); // ?
            return port;
        }
        return nullptr;
    }

    sch::Directive *Converter::createDirective(schematic::Component *qucsComponent)
    {
        static const QMap<QString, QString> mapping {
            {
                {QStringLiteral(".AC"), QStringLiteral("AC Simulation")},
                {QStringLiteral(".DC"), QStringLiteral("DC Simulation")},
                {QStringLiteral(".HB"), QStringLiteral("Harmonic balance\nSimulation")},
                {QStringLiteral(".SW"), QStringLiteral("Parameter\nSweep")},
                {QStringLiteral(".SP"), QStringLiteral("S-parameter\nSimulation")},
                {QStringLiteral(".TR"), QStringLiteral("Transient\nSimulation")},
                {QStringLiteral(".Opt"), QStringLiteral("Optimisation")},
                {QStringLiteral("Eqn"), QStringLiteral("Equations")},
            }};

        if (mapping.keys().contains(qucsComponent->model)) {
            auto dir = new sch::Directive();
            dir->designator = qucsComponent->reference;
            dir->pos = location(qucsComponent->position);
            dir->typeName = qucsComponent->model; // FIXME: Map model name to Text
            dir->text = mapping.value(qucsComponent->model);
            dir->rotation = rotation(qucsComponent);
            int i=1;
            for (const auto &property: qucsComponent->properties) {
                dir->parameters.insert(QString::number(i++), qMakePair(property.value, property.isDisplayed)); // FIXME
            }
            return dir;
        }
        return nullptr;
    }

    sch::Diagram *Converter::createDiagram(diagram::Diagram *qucsDiagram)
    {
        static const QMap<QString, QString> mapping {
            {
                { QStringLiteral("Curve"), QStringLiteral("Locus curve diagram") },
                { QStringLiteral("Smith"), QStringLiteral("Smith Impedance\ndiagram") },
                { QStringLiteral("ySmith"), QStringLiteral("Smith admittance\ndiagram") },
                { QStringLiteral("PS"), QStringLiteral("Smith/Polar\ndiagram") },
                { QStringLiteral("SP"), QStringLiteral("Smith/Polar (upper)\ndiagram") },
                { QStringLiteral("Polar"), QStringLiteral("Polar diagram") },
                { QStringLiteral("Rect3D"), QStringLiteral("3D-Cartesian diagram") },
                { QStringLiteral("Rect"), QStringLiteral("2D-Cartesian diagram") },
                { QStringLiteral("Tab"), QStringLiteral("Tabular diagram") },
                { QStringLiteral("Time"), QStringLiteral("Timing diagram") },
                { QStringLiteral("Truth"), QStringLiteral("Truth-table diagram") },
            }};

        if (mapping.keys().contains(qucsDiagram->name)) {
            auto dia = new sch::Diagram();
            dia->pos = location(qucsDiagram->leftCenter-QPoint(0, qucsDiagram->size.height()));
            dia->size = size(qucsDiagram->size);
            dia->text = mapping.value(qucsDiagram->name);
            return dia;
        }
        return nullptr;
    }

    QList<draw::DrawingObject *> Converter::createDrawings(const drawing::Drawings &qucs)
    {
        QList<draw::DrawingObject *> result;
        for (auto drawing: qucs.arcs)
            result.append(createDrawing(drawing));
        for (auto drawing: qucs.arrows)
            result.append(createDrawing(drawing));
        for (auto drawing: qucs.ellipses)
            result.append(createDrawing(drawing));
        for (auto drawing: qucs.lines)
            result.append(createDrawing(drawing));
        for (auto drawing: qucs.rectangles)
            result.append(createDrawing(drawing));
        for (auto drawing: qucs.texts)
            result.append(createDrawing(drawing));
        return result;
    }

    draw::DrawingObject *Converter::createDrawing(drawing::Rectangle *qucs)
    {
        auto object = new draw::Rectangle();
        const int x = qucs->topLeft.x();
        const int y = qucs->topLeft.y();
        const int w = qucs->size.width();
        const int h = qucs->size.height();
        object->setLocation(location(x+w/2.0, y+h/2.0));
        object->setSize(size(qucs->size));
        object->setStrokeStyle(strokeStyle(qucs->pen.style));
        object->setStrokeWidth(strokeWidth(qucs->pen.width));
        object->setStrokeColor(color(qucs->pen.color));
        object->setFillStyle(fillStyle(qucs->brush.style, qucs->brush.filled));
        object->setFillColor(color(qucs->brush.color));
        return object;
    }

    draw::DrawingObject *Converter::createDrawing(drawing::Ellipse *qucs)
    {
        auto object = new draw::Ellipse();
        const int x = qucs->topLeft.x();
        const int y = qucs->topLeft.y();
        const int w = qucs->size.width();
        const int h = qucs->size.height();
        object->setStyle(Qs::FullEllipsoid);
        object->setLocation(location(x+w/2.0, y+h/2.0));
        object->setSize(size(qucs->size));
        object->setStrokeStyle(strokeStyle(qucs->pen.style));
        object->setStrokeWidth(strokeWidth(qucs->pen.width));
        object->setStrokeColor(color(qucs->pen.color));
        object->setFillStyle(fillStyle(qucs->brush.style, qucs->brush.filled));
        object->setFillColor(color(qucs->brush.color));
        return object;
    }

    draw::DrawingObject *Converter::createDrawing(drawing::Arc *qucs)
    {
        auto object = new draw::Ellipse();
        const int x = qucs->topLeft.x();
        const int y = qucs->topLeft.y();
        const int w = qucs->size.width();
        const int h = qucs->size.height();
        object->setStyle(Qs::EllipsoidalArc);
        object->setLocation(location(x+w/2.0, y+h/2.0));
        object->setSize(size(qucs->size));
        object->setStartAngle(qucs->startAngle/16.0);
        object->setSpanAngle(qucs->spanAngle/16.0);
        object->setStrokeStyle(strokeStyle(qucs->pen.style));
        object->setStrokeWidth(strokeWidth(qucs->pen.width));
        object->setStrokeColor(color(qucs->pen.color));
        object->setFillStyle(fillStyle(qucs->brush.style, qucs->brush.filled));
        object->setFillColor(color(qucs->brush.color));
        return object;
    }

    draw::DrawingObject *Converter::createDrawing(drawing::Line *qucs)
    {
        auto object = new draw::Line();
        object->setP1(location(qucs->p1));
        object->setP2(location(qucs->p1 + qucs->p2)); // Symbol vs schematic?
        object->setStrokeStyle(strokeStyle(qucs->pen.style));
        object->setStrokeWidth(strokeWidth(qucs->pen.width));
        object->setStrokeColor(color(qucs->pen.color));
        return object;
    }

    draw::DrawingObject *Converter::createDrawing(drawing::Arrow *qucs)
    {
        auto object = new draw::Arrow();
        object->setP1(location(qucs->tailPos));
        //object->setP2(location(qucs->tailPos + qucs->headPos)); // Symbol vs schematic?
        object->setP2(location(qucs->headPos)); // Symbol vs schematic?
        object->setStyle(arrowStyle(qucs->headStyle));
        object->setStrokeStyle(strokeStyle(qucs->pen.style));
        object->setStrokeWidth(strokeWidth(qucs->pen.width));
        object->setStrokeColor(color(qucs->pen.color));
        return object;
    }

    draw::DrawingObject *Converter::createDrawing(drawing::Text *qucs)
    {
        auto object = new draw::Label();
        object->setLocation(location(qucs->topLeft));
        object->setRotation(qucs->angle); // Degrees not 16th of degrees
        object->setSize(textSize(qucs->pointSize));
        object->setText(text(qucs->text));
        object->setColor(color(qucs->color));
        return object;
    }

    qreal Converter::distance(int value)
    {
        return value/5.0;
    }

    qreal Converter::angle(int value)
    {
        return value/16.0;
    }

    // FIXME: Should be done in library generation? (Qucs exporter from within Qucs sources?)
    qreal Converter::rotation(schematic::Component *component)
    {
        static const auto needFixList =
                QStringList() << "Vac" << "Vdc" << "Vnoise" << "Vexp" << "VFile" << "Vpulse" << "Vrect"
                              << "Idc" << "Iac" << "Inoise" << "Iexp" << "Ifile" << "Ipulse" << "Irect"
                              << "Pac" << "ECVS";

        int rotation = 0;
        if (needFixList.contains(component->model)) {
            if (component->isMirrored)
                rotation = (component->rotation+1) % 4;
            else
                rotation = (4-component->rotation+1);
        }
        else {
            if (component->isMirrored)
                rotation = component->rotation;
            else
                rotation = -component->rotation;
        }
        return rotation*90.0;
    }

    QPointF Converter::location(const QPoint &value)
    {
        return QPointF(distance(value.x())-m_locationOffset.x(), distance(value.y())-m_locationOffset.y());
    }

    QSizeF Converter::size(const QSize &value)
    {
        return QSizeF(distance(value.width()), distance(value.height()));
    }

    qreal Converter::textSize(int value)
    {
        return value;
    }

    QString Converter::text(const QString &value)
    {
        return value;
    }

    Qs::ArrowStyle Converter::arrowStyle(int value)
    {
        if (value == 0)
            return Qs::FilledHead;
        return Qs::HollowHead;
    }

    Qs::StrokeStyle Converter::strokeStyle(int value)
    {
        if (value == Qt::NoPen)
            return Qs::NoStroke;
        if (value == Qt::SolidLine)
            return Qs::SolidStroke;
        if (value == Qt::DashLine)
            return Qs::DashStroke;
        if (value == Qt::DotLine)
            return Qs::DotStroke;
        if (value == Qt::DashDotLine)
            return Qs::DashDotStroke;
        if (value == Qt::DashDotDotLine)
            return Qs::DasDotDotStroke;
        qWarning() << "Invalid pen style:" << value;
        return Qs::SolidStroke;
    }

    Qs::StrokeWidth Converter::strokeWidth(int value)
    {
        if (value <= 0) {
            qWarning() << "Invalid stroke width:" << value;
            return Qs::MediumStroke;
        }
        if (value <= 1)
            return Qs::SmallestStroke;
        if (value <= 2)
            return Qs::SmallStroke;
        if (value <= 3)
            return Qs::MediumStroke;
        if (value <= 4)
            return Qs::LargeStroke;
        if (value <= 5)
            return Qs::LargestStroke;

        qWarning() << "Invalid stroke width:" << value;
        return Qs::MediumStroke;
    }

    QColor Converter::color(const QString &value)
    {
        return QColor(value);
    }

    Qs::FillStyle Converter::fillStyle(int value, int filled)
    {
        if (filled == 0)
            return Qs::NoFill;
        if (value == Qt::NoBrush)
            return Qs::NoFill;
        if (value == Qt::SolidPattern)
            return Qs::SolidFill;
        if (value == Qt::HorPattern)
            return Qs::HorLineFill;
        if (value == Qt::VerPattern)
            return Qs::VerLineFill;
        if (value == Qt::CrossPattern)
            return Qs::HorVerLineFill;
        if (value == Qt::BDiagPattern)
            return Qs::BwLineFill;
        if (value == Qt::FDiagPattern)
            return Qs::FwLineFill;
        if (value == Qt::DiagCrossPattern)
            return Qs::FwBwLineFill;
        qWarning() << "Invalid brush style:" << value;
        return Qs::NoFill;
    }

    Qs::SheetFormat Converter::sheetFormat(int value)
    {
        if (value == 0)
            return Qs::AutoSheetFormat;
        if (value == 1)
            return Qs::A5Landscape;
        if (value == 2)
            return Qs::A5Portrait;
        if (value == 3)
            return Qs::A4Landscape;
        if (value == 4)
            return Qs::A4Portrait;
        if (value == 5)
            return Qs::A3Landscape;
        if (value == 6)
            return Qs::A3Portrait;
        if (value == 7)
            return Qs::LetterLandscape;
        if (value == 8)
            return Qs::LetterPortrait;
        qWarning() << "Invalid sheet fromat:" << value;
        return Qs::AutoSheetFormat;

    }

    void Converter::populateParameters(sch::Component *schComponent, const schematic::Component *qucsComponent)
    {
        Q_ASSERT(m_libraryManager != nullptr);
        auto libComponent = m_libraryManager->cellByUid(schComponent->componentName);
        if (libComponent == nullptr) {
            qWarning() << "Could not resolve component" << qucsComponent->reference << qucsComponent->model;
            return;
        }
        if (libComponent->parameters.count() != qucsComponent->properties.count()) {
            qDebug() << qucsComponent->model << qucsComponent->reference << "param/prop mismatch";
            return;
        }
        for (int i=0; i<libComponent->parameters.count(); ++i) {
            auto paramDef = libComponent->parameters.value(i);
            auto qucsProp = qucsComponent->properties.value(i);
            sch::Parameter schParam;
            schParam.name = paramDef->name();
            schParam.showName = schParam.showValue = qucsProp.isDisplayed;
            schParam.value = qucsProp.value;
            schComponent->parameterTable.items.append(schParam);
        }
    }

}
