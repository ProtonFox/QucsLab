#ifndef QUCSREADER_H
#define QUCSREADER_H

#include "QucsObjects.h"

#include <QtCore/QScopedPointer>

class QIODevice;

namespace qucs
{

    class Reader
    {
    public:
        explicit Reader(QIODevice *device = nullptr);
        ~Reader();

        void setDevice(QIODevice *device);
        QIODevice *device() const;

        schematic::Schematic *readSchematic();
        library::Library *readLibrary();
        symbol::Symbol *readSymbol();

        bool hasError() const;
        QString errorString() const;
        int lineNumber() const;
        int columnNumber() const;
        int byteOffset() const;

    private:
        void raiseError(const QString &reason);
        void raiseWarning(const QString &reason);
        QIODevice *m_device = nullptr;
        QString m_errorString;
        QScopedPointer<schematic::Schematic> m_schematic;
        QScopedPointer<library::Library> m_library;
        QScopedPointer<symbol::Symbol> m_symbol;

        bool isKnownSchematicGroup(const QString &name);
        bool readSchematicGroup(const QString &name, QStringList &content);
        bool readSchematicProperties(const QStringList &content);
        bool readSchematicSymbol(const QStringList &content);
        bool readSchematicComponents(const QStringList &content);
        bool readSchematicWires(const QStringList &content);
        bool readSchematicDiagrams(const QStringList &content);
        bool readDrawings(const QStringList &content);
    };
}

#endif // QUCSREADER_H
