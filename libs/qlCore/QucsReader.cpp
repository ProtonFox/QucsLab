#include "QucsReader.h"

#include <QtCore/QDebug>
#include <QtCore/QIODevice>
#include <QtCore/QRegularExpression>

namespace qucs
{

    Reader::Reader(QIODevice *device)
        : m_device(device)
    {

    }

    Reader::~Reader()
    {

    }

    void Reader::setDevice(QIODevice *device)
    {
        m_device = device;
    }

    QIODevice *Reader::device() const
    {
        return m_device;
    }

    schematic::Schematic *Reader::readSchematic()
    {
        using namespace schematic;

        static const QString header("<Qucs Schematic 0.0.19>");

        m_errorString.clear();

        const auto data = QString::fromUtf8(m_device->readAll());
        const auto lines = data.split('\n');

        enum State {
            WaitForHeader = 0,
            WaitForBeginElement,
            WaitForEndElement
        };
        State state = WaitForHeader;
        QString elementName;
        QStringList elementContent;
        QString endTag;

        m_schematic.reset(new Schematic());

        for (const QString &input: lines) {
            const QString line = input.trimmed();

            if (line.isEmpty())
                continue;

            switch (state) {
                case WaitForHeader:
                    if (line == header) {
                        state = WaitForBeginElement;
                        break;
                    }
                    raiseError(QString("Expected '%2', got '%1' instead").arg(line, header));
                    return nullptr;
                case WaitForBeginElement:
                    if (!(line.startsWith('<') && line.endsWith('>'))) {
                        raiseError(QString("Expected start element, got '%1' instead").arg(line));
                        return nullptr;
                    }
                    elementName = line.mid(1, line.length() - 2);
                    if (!isKnownSchematicGroup(elementName)) {
                        raiseError(QString("Unkown element '%1").arg(elementName));
                        return nullptr;
                    }
                    elementContent.clear();
                    endTag = QString("</%1>").arg(elementName);
                    state = WaitForEndElement;
                    break;
                case WaitForEndElement:
                    if (line == endTag) {
                        state = WaitForBeginElement;
                        if (!readSchematicGroup(elementName, elementContent))
                            return nullptr;
                        break;
                    }
                    elementContent.append(line);
                    break;
            }
        }
        if (state != WaitForBeginElement) {
            raiseError(QString("Premature end of file while reading content for element '%1'").arg(elementName));
            return nullptr;
        }

        return m_schematic.data();

    }

    library::Library *Reader::readLibrary()
    {
        return nullptr;
    }

    symbol::Symbol *Reader::readSymbol()
    {
        return nullptr;
    }

    QString Reader::errorString() const
    {
        return m_errorString;
    }

    void Reader::raiseError(const QString &reason)
    {
        qCritical() << reason;
        m_errorString = reason;
    }

    void Reader::raiseWarning(const QString &reason)
    {
        qWarning() << reason;
    }

    bool Reader::isKnownSchematicGroup(const QString &name)
    {
        static const QList<QString> names = QList<QString>()
                << "Properties" << "Components" << "Wires"
                << "Diagrams" << "Paintings" << "Symbol";
        return names.contains(name);
    }

    bool Reader::readSchematicGroup(const QString &name, QStringList &content)
    {
        if (name == "Properties")
            return readSchematicProperties(content);
        if (name == "Symbol")
            return readSchematicSymbol(content);
        if (name == "Components")
            return readSchematicComponents(content);
        if (name == "Wires")
            return readSchematicWires(content);
        if (name == "Diagrams")
            return readSchematicDiagrams(content);
        if (name == "Paintings")
            return readDrawings(content);
        Q_UNREACHABLE();
        return false;
    }

    bool Reader::readSchematicProperties(const QStringList &content)
    {
        using namespace schematic;

        for (const QString &line: content) {
            if (line.startsWith("<View")) {
                // <View=x1,y1,x2,y2,scale,xpos,ypos>
                static const QRegularExpression re("^<View=(-?\\d+),(-?\\d+),(\\d+),(\\d+),(\\d+\\.?\\d*),(-?\\d+),(-?\\d+)>$",
                                                   QRegularExpression::OptimizeOnFirstUsageOption);
                Q_ASSERT(re.isValid());
                const auto match = re.match(line);
                if (!match.hasMatch()) {
                    raiseError(QString("Malformed View peoperty: '%1'").arg(line));
                    return false;
                }
                int index = 1;
                const auto x = match.captured(index++).toInt();
                const auto y = match.captured(index++).toInt();
                const auto w = match.captured(index++).toInt();
                const auto h = match.captured(index++).toInt();
                m_schematic->extent = QRect(QPoint(x, y), QSize(w, h));
            }
            else if (line.startsWith("<showFrame")) {
                static const QRegularExpression re("^<showFrame=([0-8])>$",
                                                   QRegularExpression::OptimizeOnFirstUsageOption);
                Q_ASSERT(re.isValid());
                const auto match = re.match(line);
                if (!match.hasMatch()) {
                    raiseError(QString("Malformed frame property: '%1'").arg(line));
                    return false;
                }
                m_schematic->frameFormat = match.captured(1).toInt();
                qt_noop();
            }
            else if (line.startsWith("<FrameText")) {
                static const QRegularExpression re("^<FrameText([0-9])=(.*)>$",
                                                   QRegularExpression::OptimizeOnFirstUsageOption);
                Q_ASSERT(re.isValid());
                const auto match = re.match(line);
                if (!match.hasMatch()) {
                    raiseError(QString("Malformed frame property: '%1'").arg(line));
                    return false;
                }
                const auto name = match.captured(1);
                const auto value = match.captured(2);
                m_schematic->frameTexts.insert(name, value);
            }
            else if (line.startsWith("<Grid ")) {
            }

            else
                raiseWarning(QString("Ignoring property line: '%1'").arg(line));
        }

        return true;
    }

    bool Reader::readSchematicSymbol(const QStringList &content)
    {
        using namespace schematic;
        Q_UNUSED(content)
        raiseWarning("qucs::Reader::readSchematicSymbol(): not implemented");
        return true;
    }

    bool Reader::readSchematicComponents(const QStringList &content)
    {
        using namespace schematic;
        static const QRegularExpression compRe("^<([a-zA-Z0-9_\\.]+) ([a-zA-Z0-9_\\.*]+) "
                                               "(\\d+) (-?\\d+) (-?\\d+) (-?\\d+) (-?\\d+) "
                                               "([01]) ([0-3])"
                                               "(( \"([^\"]*)\" ([01]))*)>$",
                                               QRegularExpression::OptimizeOnFirstUsageOption);
        Q_ASSERT(compRe.isValid());
        static const QRegularExpression propRe(" \"([^\"]*)\" ([01])",
                                               QRegularExpression::OptimizeOnFirstUsageOption);
        Q_ASSERT(propRe.isValid());

        for (const auto &line: content) {
            const auto compMatch = compRe.match(line);
            if (!compMatch.hasMatch()) {
                raiseError(QString("Malformed component entry: '%1'").arg(line));
                return false;
            }
            auto component = new Component();
            int index = 1;
            component->model = compMatch.captured(index++);
            component->reference = compMatch.captured(index++);
            component->flags = compMatch.captured(index++).toInt();
            const auto cx = compMatch.captured(index++).toInt();
            const auto cy = compMatch.captured(index++).toInt();
            component->position = QPoint(cx, cy);
            const auto tx = compMatch.captured(index++).toInt();
            const auto ty = compMatch.captured(index++).toInt();
            component->textPosition = QPoint(tx, ty);
            component->isMirrored = compMatch.captured(index++).toInt() == 1;
            component->rotation = compMatch.captured(index++).toInt();
            Q_ASSERT(index == 10);
            const auto properties = compMatch.captured(10);
            QRegularExpressionMatchIterator propIter = propRe.globalMatch(properties);
            while (propIter.hasNext()) {
                QRegularExpressionMatch propMatch = propIter.next();
                Q_ASSERT(propMatch.hasMatch()); // Enforced by main regexp
                Property property;
                property.value = propMatch.captured(1);
                property.isDisplayed = propMatch.captured(2).toInt() == 1;
                component->properties.append(property);
            }
            m_schematic->components.append(component);
        }
        return true;
    }

    bool Reader::readSchematicWires(const QStringList &content)
    {
        using namespace schematic;

        static const QRegularExpression re("^<(-?\\d+) (-?\\d+) (-?\\d+) (-?\\d+) "
                                           "\"([^\"]*)\" (-?\\d+) (-?\\d+) (-?\\d+)"
                                           "( \"([^\"]*)\")?>$",
                                           QRegularExpression::OptimizeOnFirstUsageOption);
        Q_ASSERT(re.isValid());

        for (const auto &line: content) {
            const auto match = re.match(line);
            if (!match.hasMatch()) {
                m_errorString = QString("Malformed wire entry: '%1'").arg(line);
                return false;
            }
            auto wire = new Wire();
            int index = 1;
            const auto x1 = match.captured(index++).toInt();
            const auto y1 = match.captured(index++).toInt();
            wire->p1 = QPoint(x1, y1);
            const auto x2 = match.captured(index++).toInt();
            const auto y2 = match.captured(index++).toInt();
            wire->p2 = QPoint(x2, y2);
            wire->net.name = match.captured(index++);
            const auto tx = match.captured(index++).toInt();
            const auto ty = match.captured(index++).toInt();
            wire->net.topLeft = QPoint(tx, ty);
            wire->net.delta = match.captured(index++).toInt();
            wire->net.value = match.captured(index++);
            Q_ASSERT(index=10);
            m_schematic->wires.append(wire);
        }
        return true;
    }

    bool Reader::readSchematicDiagrams(const QStringList &content)
    {
        using namespace diagram;

        static const auto names = QStringList() << QStringLiteral("Curve")
                                                << QStringLiteral("Smith")
                                                << QStringLiteral("ySmith")
                                                << QStringLiteral("PS")
                                                << QStringLiteral("SP")
                                                << QStringLiteral("Polar")
                                                << QStringLiteral("Rect3D")
                                                << QStringLiteral("Rect")
                                                << QStringLiteral("Tab")
                                                << QStringLiteral("Time")
                                                << QStringLiteral("Truth");
        static const QRegularExpression re(QString("^<(%1) (-?\\d+) (-?\\d+) (\\d+) (\\d+)"
                                           " .*>$").arg(names.join("|")),
                                           QRegularExpression::OptimizeOnFirstUsageOption);
        Q_ASSERT(re.isValid());
        for (const auto &line: content) {
            const auto match = re.match(line);
            if (!match.hasMatch()) {
                continue; // FIXME:
            }
            int index = 1;
            auto dia = new Diagram();
            dia->name = match.captured(index++);
            const auto cx = match.captured(index++).toInt();
            const auto cy = match.captured(index++).toInt();
            const auto x2 = match.captured(index++).toInt();
            const auto y2 = match.captured(index++).toInt();
            dia->leftCenter = QPoint(cx, cy);
            dia->size = QSize(x2, y2);
            m_schematic->diagrams.append(dia);
        }
        return true;
    }

    bool Reader::readDrawings(const QStringList &content)
    {
        using namespace drawing;

        for (const QString &line: content) {
            if (line.startsWith("<Text ")) {
                // cx cy size color angle "text"
                // text can contains quotes, and they are not escaped in any way!?!
                static const QRegularExpression re("^<Text (-?\\d+) (-?\\d+) (\\d+) (#[0-9a-f]{6}) (\\d+) "
                                                   "\"(.*)\">$",
                                                   QRegularExpression::OptimizeOnFirstUsageOption);
                Q_ASSERT(re.isValid());
                const auto match = re.match(line);
                if (!match.hasMatch()) {
                    raiseError(QString("Malformed text entry: '%1'").arg(line));
                    return false;
                }
                auto text = new Text();
                int index = 1;
                const auto cx = match.captured(index++).toInt();
                const auto cy = match.captured(index++).toInt();
                text->topLeft = QPoint(cx, cy);
                text->pointSize = match.captured(index++).toInt();
                text->color = match.captured(index++);
                text->angle = match.captured(index++).toInt();
                text->text = match.captured(index++).replace("\\n", "\n");
                Q_ASSERT(index=6);
                m_schematic->drawings.texts.append(text);
            }
            else if (line.startsWith("<Rectangle ")) {
                // cx cy x2 y2 strokeColor strokeWidth strokeStyle brushColor brushStyle isFilled
                static const QRegularExpression re("^<Rectangle (-?\\d+) (-?\\d+) (-?\\d+) (-?\\d+) "
                                                   "(#[0-9a-f]{6}) (\\d+) (\\d+) "
                                                   "(#[0-9a-f]{6}) (\\d+) ([01])>$",
                                                   QRegularExpression::OptimizeOnFirstUsageOption);
                Q_ASSERT(re.isValid());
                const auto match = re.match(line);
                if (!match.hasMatch()) {
                    raiseError(QString("Malformed rectangle entry: '%1'").arg(line));
                    return false;
                }
                auto rect = new Rectangle();
                int index = 1;
                const auto x = match.captured(index++).toInt();
                const auto y = match.captured(index++).toInt();
                rect->topLeft = QPoint(x, y);
                const auto w = match.captured(index++).toInt();
                const auto h = match.captured(index++).toInt();
                rect->size = QSize(w, h);
                rect->pen.color = match.captured(index++);
                rect->pen.width = match.captured(index++).toInt();
                rect->pen.style = match.captured(index++).toInt();
                rect->brush.color = match.captured(index++);
                rect->brush.style = match.captured(index++).toInt();
                if (match.captured(index++).toInt() == 0)
                    rect->brush.style = 0;
                Q_ASSERT(index=10);
                m_schematic->drawings.rectangles.append(rect);
            }
            else if (line.startsWith("<Arrow ")) {
                // x y width height headHeight headWidth strokeColor strokeWidth strokeStyle (headStyle?)
                static const QRegularExpression re("^<Arrow (-?\\d+) (-?\\d+) (-?\\d+) (-?\\d+) "
                                                   "(\\d+) (\\d+) "
                                                   "(#[0-9a-f]{6}) (\\d+) (\\d+)( [01])?>$",
                                                   QRegularExpression::OptimizeOnFirstUsageOption);
                // Full: <Arrow 390 200 -20 -60 20 8 #ff0000 3 1>
                // Line: <Arrow 200  60 -30 100 20 8 #000000 0 1>
                Q_ASSERT(re.isValid());
                const auto match = re.match(line);
                if (!match.hasMatch()) {
                    raiseError(QString("Malformed arrow entry: '%1'").arg(line));
                    return false;
                }
                auto arrow = new Arrow();
                int index = 1;
                const auto x = match.captured(index++).toInt();
                const auto y = match.captured(index++).toInt();
                arrow->tailPos = QPoint(x, y);
                const auto w = match.captured(index++).toInt();
                const auto h = match.captured(index++).toInt();
                arrow->headPos = arrow->tailPos + QPoint(w, h);
                const auto hh = match.captured(index++).toInt();
                const auto hw = match.captured(index++).toInt();
                arrow->headSize = QSize(hw, hh);
                arrow->pen.color = match.captured(index++);
                arrow->pen.width = match.captured(index++).toInt();
                arrow->pen.style = match.captured(index++).toInt();
                const auto style = match.captured(index++);
                arrow->headStyle = style.isEmpty() ? 1 : style.toInt();
                Q_ASSERT(index=10);
                m_schematic->drawings.arrows.append(arrow);
            }
            else if (line.startsWith("<Line ")) {
                // x1 y1 x2 y2 strokeColor strokeWidth strokeStyle
                static const QRegularExpression re("^<Line (-?\\d+) (-?\\d+) (-?\\d+) (-?\\d+) "
                                                   "(#[0-9a-f]{6}) (\\d+) (\\d+)>$",
                                                   QRegularExpression::OptimizeOnFirstUsageOption);
                Q_ASSERT(re.isValid());
                const auto match = re.match(line);
                if (!match.hasMatch()) {
                    raiseError(QString("Malformed arrow entry: '%1'").arg(line));
                    return false;
                }
                auto line = new Line();
                int index = 1;
                const auto x1 = match.captured(index++).toInt();
                const auto y1 = match.captured(index++).toInt();
                line->p1 = QPoint(x1, y1);
                const auto x2 = match.captured(index++).toInt();
                const auto y2 = match.captured(index++).toInt();
                line->p2 = QPoint(x2, y2);
                line->pen.color = match.captured(index++);
                line->pen.width = match.captured(index++).toInt();
                line->pen.style = match.captured(index++).toInt();
                Q_ASSERT(index=7);
                m_schematic->drawings.lines.append(line);
            }
            else {
                raiseError(QString("Unknown drawing entry: '%1'").arg(line));
                return false;
            }
        }
        return true;
    }

}
