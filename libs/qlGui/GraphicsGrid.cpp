/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsGrid.h"

#include <QtCore/QtMath>
#include <QtCore/QRectF>
#include <QtGui/QPainter>
#include <QtWidgets/QStyleOptionGraphicsItem>

GraphicsGrid::GraphicsGrid(QGraphicsItem *parent)
    : QGraphicsObject(parent)
    , m_rect(QRectF())
    , m_majorPen(QPen(Qt::black, 0.0))
    , m_minorPen(QPen(Qt::black, 0.0))
    , m_originPen(QPen(Qt::black, 0.0, Qt::DashDotLine))
    , m_step(10.0)
    , m_minimumGridSize(20)
{

}

GraphicsGrid::~GraphicsGrid()
{

}

void GraphicsGrid::setRect(const QRectF &rect)
{
    if (m_rect == rect)
        return;
    m_rect = rect;
    update();
    emit rectChanged(m_rect);
}

QRectF GraphicsGrid::rect() const
{
    return m_rect;
}

void GraphicsGrid::setMajorColor(const QColor &color)
{
    if (m_majorPen.color() == color)
        return;
    m_majorPen.setColor(color);
    update();
    emit majorColorChanged(m_majorPen.color());
}

QColor GraphicsGrid::majorColor() const
{
    return m_majorPen.color();
}

QColor GraphicsGrid::minorColor() const
{
    return m_minorPen.color();
}

QColor GraphicsGrid::originColor() const
{
    return m_originPen.color();
}

void GraphicsGrid::setMinorColor(const QColor &color)
{
    if (m_minorPen.color() == color)
        return;
    m_minorPen.setColor(color);
    update();
    emit minorColorChanged(m_majorPen.color());
}

void GraphicsGrid::setOriginColor(const QColor &color)
{
    if (m_originPen.color() == color)
        return;
    m_originPen.setColor(color);
    update();
    emit originColorChanged(m_majorPen.color());
}

void GraphicsGrid::setStep(qreal step)
{
    if (qFuzzyCompare(m_step, step))
        return;
    m_step = step;
    update();
    emit stepChanged(m_step);
}

qreal GraphicsGrid::step() const
{
    return m_step;
}

void GraphicsGrid::setMinimumGridSize(int pixels)
{
    if (m_minimumGridSize == pixels)
        return;
    m_minimumGridSize = pixels;
    update();
    emit minimumGridSizeChanged(m_minimumGridSize);
}

int GraphicsGrid::minimumGridSize() const
{
    return m_minimumGridSize;
}

void GraphicsGrid::setOriginVisible(bool visible)
{
    if (m_originVisible == visible)
        return;
    prepareGeometryChange();
    m_originVisible = visible;
    emit originVisibleChanged(m_originVisible);
}

bool GraphicsGrid::isOriginVisible() const
{
    return m_originVisible;
}

void GraphicsGrid::paintMajorGrid(QPainter *painter, const QRectF &exposedRect, qreal step)
{
    const int hCount = qFloor(exposedRect.height()/step) + 1;
    const int vCount = qFloor(exposedRect.width()/step) + 1;
    const QPointF topLeft = exposedRect.topLeft();
    const QPointF bottomRight = exposedRect.bottomRight();
    qreal left = topLeft.x() - std::fmod(topLeft.x(), step);
    qreal top = topLeft.y() - std::fmod(topLeft.y(), step);

    QVector<QPointF> lines(hCount*2+vCount*2);
    QPointF *data = lines.data();

    const qreal gridLeft = topLeft.x();
    const qreal gridRight = bottomRight.x();
    for (int i=0; i<hCount; i++) {
        (*data).setX(gridLeft);
        (*data++).setY(top);
        (*data).setX(gridRight);
        (*data++).setY(top);
        top += step;
    }

    const qreal gridTop = topLeft.y();
    const qreal gridBottom = bottomRight.y();
    for (int i=0; i<vCount; i++) {
        (*data).setX(left);
        (*data++).setY(gridTop);
        (*data).setX(left);
        (*data++).setY(gridBottom);
        left += step;
    }

    painter->setPen(m_majorPen);
    painter->drawLines(lines);
}

void GraphicsGrid::paintMinorGrid(QPainter *painter, const QRectF &exposedRect, qreal step)
{
    const int hCount = qFloor(exposedRect.height()/step);
    const int vCount = qFloor(exposedRect.width()/step);

    QVector<QPointF> points(hCount*vCount);
    QPointF *data = points.data();

    const QPointF topLeft = exposedRect.topLeft();
    const qreal gridLeft = topLeft.x() - std::fmod(topLeft.x(), step);
    qreal top = topLeft.y() - std::fmod(topLeft.y(), step);
    for (int i=0; i<hCount; i++) {
        qreal left = gridLeft;
        for (int i=0; i<vCount; i++) {
            (*data).setX(left);
            (*data++).setY(top);
            left += step;
        }
        top += step;
    }

    painter->setPen(m_minorPen);
    painter->drawPoints(points);
}

void GraphicsGrid::paintOrigin(QPainter *painter, const QRectF &exposedRect, qreal zoom)
{
    QLineF l1(exposedRect.left(), 0, exposedRect.right(), 0);
    QLineF l2(0, exposedRect.top(), 0, exposedRect.bottom());
    m_originPen.setWidthF(1.5/zoom);
    painter->setPen(m_originPen);
    painter->drawLine(l1);
    painter->drawLine(l2);
}

QRectF GraphicsGrid::boundingRect() const
{
    return m_rect;
}

void GraphicsGrid::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);

    qreal step = m_step;
    const qreal zoom = option->levelOfDetailFromTransform(painter->worldTransform());
    const qreal gridSize = zoom * step;
    if (gridSize < m_minimumGridSize)
        step = m_minimumGridSize / zoom;
    paintMajorGrid(painter, option->exposedRect, step);
    paintMinorGrid(painter, option->exposedRect, step/10.0);
    if (m_originVisible)
        paintOrigin(painter, option->exposedRect, zoom);
}


int GraphicsGrid::type() const
{
    return Type;
}
