/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsEventHandler.h"

#include <QtCore/QtGlobal>

GraphicsViewEventHandler::GraphicsViewEventHandler()
{

}

GraphicsViewEventHandler::~GraphicsViewEventHandler()
{

}

bool GraphicsViewEventHandler::graphicsViewContextMenuEvent(GraphicsView *view, QContextMenuEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}

bool GraphicsViewEventHandler::graphicsViewDragEnterEvent(GraphicsView *view, QDragEnterEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}

bool GraphicsViewEventHandler::graphicsViewDragLeaveEvent(GraphicsView *view, QDragLeaveEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}


bool GraphicsViewEventHandler::graphicsViewDragMoveEvent(GraphicsView *view, QDragMoveEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}


bool GraphicsViewEventHandler::graphicsViewDropEvent(GraphicsView *view, QDropEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}


bool GraphicsViewEventHandler::graphicsViewFocusInEvent(GraphicsView *view, QFocusEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}


bool GraphicsViewEventHandler::graphicsViewFocusOutEvent(GraphicsView *view, QFocusEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}


bool GraphicsViewEventHandler::graphicsViewKeyPressEvent(GraphicsView *view, QKeyEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}


bool GraphicsViewEventHandler::graphicsViewKeyReleaseEvent(GraphicsView *view, QKeyEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}


bool GraphicsViewEventHandler::graphicsViewMouseDoubleClickEvent(GraphicsView *view, QMouseEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}


bool GraphicsViewEventHandler::graphicsViewMousePressEvent(GraphicsView *view, QMouseEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}


bool GraphicsViewEventHandler::graphicsViewMouseMoveEvent(GraphicsView *view, QMouseEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}


bool GraphicsViewEventHandler::graphicsViewMouseReleaseEvent(GraphicsView *view, QMouseEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}


bool GraphicsViewEventHandler::graphicsViewWheelEvent(GraphicsView *view, QWheelEvent *event)
{
    Q_UNUSED(view);
    Q_UNUSED(event);
    return false;
}



GraphicsSceneEventHandler::GraphicsSceneEventHandler()
{

}

GraphicsSceneEventHandler::~GraphicsSceneEventHandler()
{

}

bool GraphicsSceneEventHandler::graphicsSceneContextMenuEvent(GraphicsScene *scene, QGraphicsSceneContextMenuEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}

bool GraphicsSceneEventHandler::graphicsSceneDragEnterEvent(GraphicsScene *scene, QGraphicsSceneDragDropEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}


bool GraphicsSceneEventHandler::graphicsSceneDragLeaveEvent(GraphicsScene *scene, QGraphicsSceneDragDropEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}


bool GraphicsSceneEventHandler::graphicsSceneDragMoveEvent(GraphicsScene *scene, QGraphicsSceneDragDropEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}


bool GraphicsSceneEventHandler::graphicsSceneDropEvent(GraphicsScene *scene, QGraphicsSceneDragDropEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}


bool GraphicsSceneEventHandler::graphicsSceneFocusInEvent(GraphicsScene *scene, QFocusEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}


bool GraphicsSceneEventHandler::graphicsSceneFocusOutEvent(GraphicsScene *scene, QFocusEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}


bool GraphicsSceneEventHandler::graphicsSceneKeyPressEvent(GraphicsScene *scene, QKeyEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}


bool GraphicsSceneEventHandler::graphicsSceneKeyReleaseEvent(GraphicsScene *scene, QKeyEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}


bool GraphicsSceneEventHandler::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    Q_UNUSED(sourceEvent);
    return false;
}


bool GraphicsSceneEventHandler::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    Q_UNUSED(sourceEvent);
    return false;
}


bool GraphicsSceneEventHandler::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    Q_UNUSED(sourceEvent);
    return false;
}


bool GraphicsSceneEventHandler::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    Q_UNUSED(sourceEvent);
    return false;
}


bool GraphicsSceneEventHandler::graphicsSceneWheelEvent(GraphicsScene *scene, QGraphicsSceneWheelEvent *event)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);
    return false;
}

GraphicsItemEventHandler::GraphicsItemEventHandler()
{

}

GraphicsItemEventHandler::~GraphicsItemEventHandler()
{

}

bool GraphicsItemEventHandler::graphicsItemContextMenuEvent(GraphicsItem *item, QGraphicsSceneContextMenuEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}


bool GraphicsItemEventHandler::graphicsItemDragEnterEvent(GraphicsItem *item, QGraphicsSceneDragDropEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}


bool GraphicsItemEventHandler::graphicsItemDragLeaveEvent(GraphicsItem *item, QGraphicsSceneDragDropEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}


bool GraphicsItemEventHandler::graphicsItemDragMoveEvent(GraphicsItem *item, QGraphicsSceneDragDropEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}


bool GraphicsItemEventHandler::graphicsItemDropEvent(GraphicsItem *item, QGraphicsSceneDragDropEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}


bool GraphicsItemEventHandler::graphicsItemFocusInEvent(GraphicsItem *item, QFocusEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}


bool GraphicsItemEventHandler::graphicsItemFocusOutEvent(GraphicsItem *item, QFocusEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}


bool GraphicsItemEventHandler::graphicsItemKeyPressEvent(GraphicsItem *item, QKeyEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}


bool GraphicsItemEventHandler::graphicsItemKeyReleaseEvent(GraphicsItem *item, QKeyEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}


bool GraphicsItemEventHandler::graphicsItemMouseDoubleClickEvent(GraphicsItem *item, QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}


bool GraphicsItemEventHandler::graphicsItemMousePressEvent(GraphicsItem *item, QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}


bool GraphicsItemEventHandler::graphicsItemMouseMoveEvent(GraphicsItem *item, QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}


bool GraphicsItemEventHandler::graphicsItemMouseReleaseEvent(GraphicsItem *item, QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}


bool GraphicsItemEventHandler::graphicsItemWheelEvent(GraphicsItem *item, QGraphicsSceneWheelEvent *event)
{
    Q_UNUSED(item);
    Q_UNUSED(event);
    return false;
}

