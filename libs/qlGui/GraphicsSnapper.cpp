/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsScene.h"
#include "GraphicsSnapper.h"
#include "GraphicsView.h"

#include <QtCore/QDebug>
#include <QtCore/QEvent>
#include <QtWidgets/QGraphicsSceneMouseEvent>

GraphicsSnapper::GraphicsSnapper(QObject *parent)
    : QObject(parent)
    , GraphicsSceneEventHandler()
{

}

GraphicsSnapper::~GraphicsSnapper()
{

}

// FIXME: Do we need to snap pos() and lastPos(), which are in item coordinate?
// FIXME: Do we need to snap view event?
void GraphicsSnapper::setView(GraphicsView *view)
{
    if (m_view != nullptr && m_view->graphicsScene() != nullptr)
        m_view->graphicsScene()->uninstallEventHandler(this);

    m_view = view;

    if (m_view != nullptr && m_view->graphicsScene() != nullptr)
        m_view->graphicsScene()->installEventHandler(this);
}

GraphicsView *GraphicsSnapper::view() const
{
    return m_view;
}

void GraphicsSnapper::setStep(qreal step)
{
    if (qFuzzyCompare(m_step, step))
        return;
    m_step = step;
    emit stepChanged(m_step);
}

qreal GraphicsSnapper::step() const
{
    return m_step;
}

void GraphicsSnapper::snapEvent(const QGraphicsSceneMouseEvent *sourceEvent, QGraphicsSceneMouseEvent *event)
{
    // TODO: item pos?
    event->setScenePos(snapScenePos(sourceEvent->scenePos()));
    event->setLastScenePos(snapScenePos(sourceEvent->lastScenePos()));
    event->setScreenPos(snapScreenPos(sourceEvent->screenPos()));
    event->setLastScreenPos(snapScreenPos(sourceEvent->lastScreenPos()));
    for (int button = 0x01; button != Qt::MaxMouseButton; button <<= 1) {
        const auto qtButton = static_cast<Qt::MouseButton>(button);
        event->setButtonDownScenePos(qtButton, snapScenePos(sourceEvent->buttonDownScenePos(qtButton)));
        event->setButtonDownScreenPos(qtButton, snapScreenPos(sourceEvent->buttonDownScreenPos(qtButton)));
    }
}

QPoint GraphicsSnapper::snapScreenPos(const QPoint &pos)
{
    auto newPos = m_view->mapToScene(m_view->mapFromGlobal(pos));
    newPos = snapScenePos(newPos);
    return m_view->mapToGlobal(m_view->mapFromScene(newPos));
}

QPointF GraphicsSnapper::snapScenePos(const QPointF &pos)
{
    return QPointF(m_step*qRound(pos.x()/m_step),
                   m_step*qRound(pos.y()/m_step));
}


bool GraphicsSnapper::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);

    snapEvent(sourceEvent, event);
    return false;
}


bool GraphicsSnapper::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);

    snapEvent(sourceEvent, event);
    return false;
}

bool GraphicsSnapper::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);

    snapEvent(sourceEvent, event);
    return false;
}

bool GraphicsSnapper::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);

    snapEvent(sourceEvent, event);
    return false;
}
