/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtGui/QPen>
#include <QtWidgets/QGraphicsObject>

// FIXME: Add accessor
class GraphicsGrid : public QGraphicsObject
{
    Q_OBJECT
    Q_PROPERTY(QRectF rect READ rect WRITE setRect NOTIFY rectChanged)
    Q_PROPERTY(qreal step READ step WRITE setStep NOTIFY stepChanged)
    Q_PROPERTY(bool originVisible READ isOriginVisible WRITE setOriginVisible NOTIFY originVisibleChanged)
    Q_PROPERTY(int minimumGridSize READ minimumGridSize WRITE setMinimumGridSize NOTIFY minimumGridSizeChanged)
    Q_PROPERTY(QColor originColor READ originColor WRITE setOriginColor NOTIFY originColorChanged)
    Q_PROPERTY(QColor majorColor READ majorColor WRITE setMajorColor NOTIFY majorColorChanged)
    Q_PROPERTY(QColor minorColor READ minorColor WRITE setMinorColor NOTIFY minorColorChanged)

public:
    enum {
        Type = UserType + 0x200 + 0x01
    };

    GraphicsGrid(QGraphicsItem *parent = nullptr);
    ~GraphicsGrid();

    QRectF rect() const;
    QColor majorColor() const;
    QColor minorColor() const;
    QColor originColor() const;
    qreal step() const;
    int minimumGridSize() const;

    bool isOriginVisible() const;

public slots:
    void setRect(const QRectF &rect);
    void setStep(qreal step);
    void setOriginVisible(bool originVisible);
    void setMinimumGridSize(int pixels);
    void setOriginColor(const QColor &color);
    void setMajorColor(const QColor &color);
    void setMinorColor(const QColor &color);

signals:
    void rectChanged(QRectF rect);
    void stepChanged(qreal step);
    void originVisibleChanged(bool originVisible);
    void minimumGridSizeChanged(int minimumGridSize);
    void originColorChanged(QColor originColor);
    void majorColorChanged(QColor majorColor);
    void minorColorChanged(QColor minorColor);

private:
    void paintMajorGrid(QPainter *painter, const QRectF &exposedRect, qreal step);
    void paintMinorGrid(QPainter *painter, const QRectF &exposedRect, qreal step);
    void paintOrigin(QPainter *painter, const QRectF &exposedRect, qreal zoom);

    QRectF m_rect;
    QPen m_majorPen;
    QPen m_minorPen;
    QPen m_originPen;
    qreal m_step;
    bool m_originVisible = true;
    int m_minimumGridSize = 0;

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    // QGraphicsItem interface
public:
    virtual int type() const override;
};
