#-------------------------------------------------
#
# Project created by QtCreator 2017-05-11T20:10:16
#
#-------------------------------------------------

QT += widgets

TARGET = qlGui
TEMPLATE = lib
CONFIG += staticlib

DEFINES += QTPROPERTYBROWSER_LIBRARY

SOURCES += QlGui.cpp \
    GraphicsEventHandler.cpp \
    GraphicsGrid.cpp \
    GraphicsHandle.cpp \
    GraphicsItem.cpp \
    GraphicsScene.cpp \
    GraphicsSheet.cpp \
    GraphicsSnapper.cpp \
    GraphicsView.cpp \
    GraphicsZoomHandler.cpp \
    ObjectPropertyBrowser.cpp

HEADERS += QlGui.h \
    GraphicsEventHandler.h \
    GraphicsGrid.h \
    GraphicsHandle.h \
    GraphicsItem.h \
    GraphicsScene.h \
    GraphicsSheet.h \
    GraphicsSnapper.h \
    GraphicsView.h \
    GraphicsZoomHandler.h \
    ObjectPropertyBrowser.h

DISTFILES += qlGui.pri

include($$top_srcdir/QucsLab.pri)
include($$top_srcdir/libs/qlCore/qlCore.pri)
include($$top_srcdir/3rdparty/qtpropertybrowser/qtpropertybrowser.pri)
