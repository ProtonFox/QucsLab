/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "GraphicsEventHandler.h"

#include <QtCore/QObject>
#include <QtCore/QPointF>

class GraphicsView;

// TODO: minimum and maximum LOD (level of details)

class GraphicsZoomHandler : public QObject, public GraphicsViewEventHandler
{
  Q_OBJECT

public:
  GraphicsZoomHandler(QObject *parent);
  ~GraphicsZoomHandler();

  void setView(GraphicsView *view);
  GraphicsView *view() const;

  void zoom(double factor);
  void setMaximumZoomFactor(qreal max);
  void setMinimumZoomFactor(qreal min);
  void setModifiers(Qt::KeyboardModifiers modifiers);
  void setBaseZoomFactor(double value);

private:
  GraphicsView* m_view;
  Qt::KeyboardModifiers m_modifiers;
  double m_baseZoomFactor;
  QPointF m_targetScenePos;
  QPointF m_targetViewportPos;
  qreal m_maxZoom = 50;
  qreal m_minZoom = 0.02;
  qreal m_zoomDelta = 1.0; // TODO: one should be able to set the zoom step/sensitivity if needed

  // GraphicsViewEventHandler interface
public:
  virtual bool graphicsViewMouseMoveEvent(GraphicsView *view, QMouseEvent *event) override;
  virtual bool graphicsViewWheelEvent(GraphicsView *view, QWheelEvent *event) override;
};
