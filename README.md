# Qucs Lab

This project is a fork of the old, unmaintained, [Qucs Lab](https://gitlab.com/chgans/QucsLab). The name of this project will be changed in the future, as this one has actually a whole different philosophy.

This project aims at helping users to create new symbols for [Qucs-S](https://github.com/ra3xdh/qucs_s) and manage its components libraries.

## Building

```
$ git clone https://gitlab.com/chgans/QucsLab.git
$ cd QucsLab
$ qmake
$ make -j10
$ ./apps/symbol-editor/symbol-editor

```

## User manual

To be done.
