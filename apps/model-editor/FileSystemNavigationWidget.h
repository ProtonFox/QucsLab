/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtWidgets/QWidget>

class QListView;
class QFileSystemModel;

class FileSystemNavigationWidget : public QWidget
{
    Q_OBJECT
public:
    explicit FileSystemNavigationWidget(QWidget *parent = 0);
    ~FileSystemNavigationWidget();

    void setInitialPath(const QString &path);
    QString initialPath() const;

signals:
    void fileActivated(const QString &filePath);

private slots:
    void openItem(const QModelIndex &index);

private:
    void setCurrentDirectory(const QString &path);
    QListView *m_view;
    QFileSystemModel *m_model;
    QString m_initialPath;
};
