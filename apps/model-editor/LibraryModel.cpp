/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "LibraryModel.h"

#include "qlCore/LibraryObjects.h"

LibraryModel::LibraryModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

void LibraryModel::setLibrary(lib::Library *library)
{
    beginResetModel();
    m_library = library;
    endResetModel();
}

lib::Library *LibraryModel::library() const
{
    return m_library;
}

QVariant LibraryModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (section != 0)
        return QVariant();
    if (orientation != Qt::Horizontal)
        return QVariant();
    if (role != Qt::DisplayRole)
        return QVariant();
    return "Component";
}

int LibraryModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    if (m_library == nullptr)
        return 0;

    return m_library->cells.count();
}

QVariant LibraryModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const auto cell = m_library->cells.value(index.row());
    switch (role) {
        case Qt::DisplayRole:
            return cell->identification.caption;
        case Qt::ToolTipRole:
            return QString("UID: %1\n%2").arg(cell->identification.uid)
                    .arg(cell->identification.description);
        default:
            break;
    }
    return QVariant();
}
