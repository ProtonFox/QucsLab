/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtWidgets/QWidget>

namespace lib
{
    class Port;
}

class EnumItemDelegate;
class PortTableModel;
class StringItemDelegate;

class QCompleter;
class QLineEdit;
class QRegularExpressionValidator;
class QSortFilterProxyModel;
class QStringListModel;
class QTableView;

class PortTableWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PortTableWidget(QWidget *parent = 0);

    void setPorts(const QList<lib::Port *> &ports);
    QList<lib::Port *> ports() const;

signals:

private:
    QList<lib::Port *> m_parameters;
    QSortFilterProxyModel *m_sortFilterModel;
    PortTableModel *m_model = nullptr;
    QTableView *m_view = nullptr;
    EnumItemDelegate *m_directionDelegate = nullptr;
    StringItemDelegate *m_nameDelegate = nullptr;
    QStringListModel *m_nameListModel = nullptr;
    QCompleter *m_nameCompleter = nullptr;
    QRegularExpressionValidator *m_nameValidator = nullptr;
    QLineEdit *m_filterLineEdit;
};
