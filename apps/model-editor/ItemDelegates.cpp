/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "ItemDelegates.h"

#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QCompleter>
#include <QtWidgets/QLineEdit>

#include <QtGui/QValidator>

EnumItemDelegate::EnumItemDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{
}

void EnumItemDelegate::addEnum(int value, const QString &caption)
{
    const int index = m_values.count();
    m_values.append(value);
    m_valueToIndex.insert(value, index);
    m_valueToCaption.insert(value, caption);
}

QWidget *EnumItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    QComboBox* editor = new QComboBox(parent);
    for (int index=0; index<m_values.count(); ++index)
        editor->addItem(m_valueToCaption.value(m_values.value(index)));

    return editor;
}

void EnumItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    const auto value = index.model()->data(index, Qt::EditRole).toUInt();
    comboBox->setCurrentIndex(m_valueToIndex.value(value));
}

void EnumItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    model->setData(index, m_values.value(comboBox->currentIndex()), Qt::EditRole);
}

void EnumItemDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);
    editor->setGeometry(option.rect);
}

void EnumItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionViewItem myOption = option;
    const auto value = index.model()->data(index, Qt::EditRole).toUInt();
    QString text = m_valueToCaption.value(value);
    myOption.text = text;
    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &myOption, painter);
}

StringItemDelegate::StringItemDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{

}

void StringItemDelegate::setCompleter(QCompleter *completer)
{
    m_completer = completer;
}

QCompleter *StringItemDelegate::comleter() const
{
    return m_completer;
}

void StringItemDelegate::setValidator(QValidator *validator)
{
    m_validator = validator;
}

QValidator *StringItemDelegate::validator() const
{
    return m_validator;
}

QWidget *StringItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    auto userData = index.model()->data(index, Qt::UserRole);
    if (userData.canConvert<QStringList>()) {
        QComboBox *comboBox = new QComboBox(parent);
        comboBox->addItems(userData.toStringList());
        return comboBox;
    }
    else {
        QLineEdit* lineEdit = new QLineEdit(parent);
        lineEdit->setCompleter(m_completer);
        lineEdit->setValidator(m_validator);
        return lineEdit;
    }
}

void StringItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    auto userData = index.model()->data(index, Qt::UserRole);
    if (userData.canConvert<QStringList>()) {
        auto *comboBox = static_cast<QComboBox*>(editor);
        const auto value = index.model()->data(index, Qt::EditRole).toString();
        comboBox->setCurrentText(value);
    }
    else {
        auto *lineEdit = static_cast<QLineEdit*>(editor);
        const auto value = index.model()->data(index, Qt::EditRole).toString();
        lineEdit->setText(value);
    }
}

void StringItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    auto userData = index.model()->data(index, Qt::UserRole); // FIXME: Don't use UserRole, define a EnumItemRole
    if (userData.canConvert<QStringList>()) {
        auto *comboBox = static_cast<QComboBox*>(editor);
        model->setData(index, comboBox->currentText());
    }
    else {
        auto *lineEdit = static_cast<QLineEdit*>(editor);
        model->setData(index, lineEdit->text());
    }
}

void StringItemDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);
    editor->setGeometry(option.rect);
}

void StringItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QStyleOptionViewItem myOption = option;
    myOption.text = index.model()->data(index, Qt::EditRole).toString();;
    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &myOption, painter);
}
