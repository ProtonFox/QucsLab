/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

class QContextMenuEvent;
class QKeyEvent;
class QMouseEvent;
class QWheelEvent;
class QDragEnterEvent;
class QDragLeaveEvent;
class QDragMoveEvent;
class QDropEvent;
class QFocusEvent;

class QGraphicsSceneContextMenuEvent;
class QGraphicsSceneDragDropEvent;
class QGraphicsSceneMouseEvent;
class QGraphicsSceneWheelEvent;

class GraphicsView;
class GraphicsScene;
class GraphicsItem;

// TBD: base class w/ isActive, isEnabled, activate and desactivete
// TBD: Tool vs handler
// TBD: Pass both original and snapped events

class GraphicsViewEventHandler
{
public:
    GraphicsViewEventHandler();
    virtual ~GraphicsViewEventHandler();

    virtual bool graphicsViewContextMenuEvent(GraphicsView *view, QContextMenuEvent *event);
    virtual bool graphicsViewDragEnterEvent(GraphicsView *view, QDragEnterEvent *event);
    virtual bool graphicsViewDragLeaveEvent(GraphicsView *view, QDragLeaveEvent *event);
    virtual bool graphicsViewDragMoveEvent(GraphicsView *view, QDragMoveEvent *event);
    virtual bool graphicsViewDropEvent(GraphicsView *view, QDropEvent *event);
    virtual bool graphicsViewFocusInEvent(GraphicsView *view, QFocusEvent *event);
    virtual bool graphicsViewFocusOutEvent(GraphicsView *view, QFocusEvent *event);
    virtual bool graphicsViewKeyPressEvent(GraphicsView *view, QKeyEvent *event);
    virtual bool graphicsViewKeyReleaseEvent(GraphicsView *view, QKeyEvent *event);
    virtual bool graphicsViewMouseDoubleClickEvent(GraphicsView *view, QMouseEvent *event);
    virtual bool graphicsViewMousePressEvent(GraphicsView *view, QMouseEvent *event);
    virtual bool graphicsViewMouseMoveEvent(GraphicsView *view, QMouseEvent *event);
    virtual bool graphicsViewMouseReleaseEvent(GraphicsView *view, QMouseEvent *event);
    virtual bool graphicsViewWheelEvent(GraphicsView *view, QWheelEvent *event);
};

class GraphicsSceneEventHandler
{
public:
    GraphicsSceneEventHandler();
    virtual ~GraphicsSceneEventHandler();

    virtual bool graphicsSceneContextMenuEvent(GraphicsScene *scene, QGraphicsSceneContextMenuEvent *event);
    virtual bool graphicsSceneDragEnterEvent(GraphicsScene *scene, QGraphicsSceneDragDropEvent *event);
    virtual bool graphicsSceneDragLeaveEvent(GraphicsScene *scene, QGraphicsSceneDragDropEvent *event);
    virtual bool graphicsSceneDragMoveEvent(GraphicsScene *scene, QGraphicsSceneDragDropEvent *event);
    virtual bool graphicsSceneDropEvent(GraphicsScene *scene, QGraphicsSceneDragDropEvent *event);
    virtual bool graphicsSceneFocusInEvent(GraphicsScene *scene, QFocusEvent *event);
    virtual bool graphicsSceneFocusOutEvent(GraphicsScene *scene, QFocusEvent *event);
    virtual bool graphicsSceneKeyPressEvent(GraphicsScene *scene, QKeyEvent *event);
    virtual bool graphicsSceneKeyReleaseEvent(GraphicsScene *scene, QKeyEvent *event);
    virtual bool graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent);
    virtual bool graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent);
    virtual bool graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent);
    virtual bool graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent);
    virtual bool graphicsSceneWheelEvent(GraphicsScene *scene, QGraphicsSceneWheelEvent *event);
};

class GraphicsItemEventHandler
{
public:
    GraphicsItemEventHandler();
    virtual ~GraphicsItemEventHandler();

    virtual bool graphicsItemContextMenuEvent(GraphicsItem *item, QGraphicsSceneContextMenuEvent *event);
    virtual bool graphicsItemDragEnterEvent(GraphicsItem *item, QGraphicsSceneDragDropEvent *event);
    virtual bool graphicsItemDragLeaveEvent(GraphicsItem *item, QGraphicsSceneDragDropEvent *event);
    virtual bool graphicsItemDragMoveEvent(GraphicsItem *item, QGraphicsSceneDragDropEvent *event);
    virtual bool graphicsItemDropEvent(GraphicsItem *item, QGraphicsSceneDragDropEvent *event);
    virtual bool graphicsItemFocusInEvent(GraphicsItem *item, QFocusEvent *event);
    virtual bool graphicsItemFocusOutEvent(GraphicsItem *item, QFocusEvent *event);
    virtual bool graphicsItemKeyPressEvent(GraphicsItem *item, QKeyEvent *event);
    virtual bool graphicsItemKeyReleaseEvent(GraphicsItem *item, QKeyEvent *event);
    virtual bool graphicsItemMouseDoubleClickEvent(GraphicsItem *item, QGraphicsSceneMouseEvent *event);
    virtual bool graphicsItemMousePressEvent(GraphicsItem *item, QGraphicsSceneMouseEvent *event);
    virtual bool graphicsItemMouseMoveEvent(GraphicsItem *item, QGraphicsSceneMouseEvent *event);
    virtual bool graphicsItemMouseReleaseEvent(GraphicsItem *item, QGraphicsSceneMouseEvent *event);
    virtual bool graphicsItemWheelEvent(GraphicsItem *item, QGraphicsSceneWheelEvent *event);
};

