/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsEventHandler.h"
#include "GraphicsItem.h"
#include "GraphicsScene.h"

#include <QtCore/QVariant>
#include <QtWidgets/QGraphicsSceneMouseEvent>

GraphicsScene::GraphicsScene(QObject *parent)
    : QGraphicsScene(parent)
    , m_clonedMouseDoubleClickEvent(new QGraphicsSceneMouseEvent(QEvent::GraphicsSceneMouseDoubleClick))
    , m_clonedMouseMoveEvent(new QGraphicsSceneMouseEvent(QEvent::GraphicsSceneMouseMove))
    , m_clonedMousePressEvent(new QGraphicsSceneMouseEvent(QEvent::GraphicsSceneMousePress))
    , m_clonedMouseReleaseEvent(new QGraphicsSceneMouseEvent(QEvent::GraphicsSceneMouseRelease))
{

}

GraphicsScene::~GraphicsScene()
{

}

// FIXME: Move the lookup table into GraphicsWidget
// Keep GraphicsFooBar simple to ease reuse (symbol editor, schematic editor, ...)
void GraphicsScene::addGraphicsItem(QGraphicsItem *item, ObjectUid uid)
{
    Q_ASSERT(!m_itemLookupTable.contains(uid));

    m_itemLookupTable.insert(uid, item);
    item->setData(DocumentIdProperty, QVariant::fromValue(uid));

    // Clear QGraphicsItem flags that we dont want, this tells the QGraphicsScene/View/Item
    // to stop trying to be clever and do stuff on our back
    item->setFlag(QGraphicsItem::ItemIsMovable, false);

    addItem(item);
}

ObjectUid GraphicsScene::uid(const QGraphicsItem *graphicsItem) const
{
    return graphicsItem->data(DocumentIdProperty).value<ObjectUid>();
}

QGraphicsItem *GraphicsScene::graphicsItem(ObjectUid uid) const
{
    //Q_ASSERT(m_itemLookupTable.contains(uid));
    if (uid.isNull()) {
        qWarning() << "GraphicsScene::graphicsItem" << uid << "is null";
        return nullptr;
    }
    if (!m_itemLookupTable.contains(uid)) {
        qWarning() << "GraphicsScene::graphicsItem" << uid << "is unknown";
        return nullptr;
    }
    return m_itemLookupTable.value(uid);
}

QGraphicsItem *GraphicsScene::takeGraphicsItem(ObjectUid uid)
{
    //Q_ASSERT(m_itemLookupTable.contains(uid));
    if (!m_itemLookupTable.contains(uid)) {
        qWarning() << "GraphicsScene::takeGraphicsItem" << uid << "is unknown";
        return nullptr;
    }
    auto item = m_itemLookupTable.take(uid);
    removeItem(item);
    return item;
}

void GraphicsScene::installEventHandler(GraphicsSceneEventHandler *handler)
{
    m_eventHandlers.append(handler);
}

void GraphicsScene::uninstallEventHandler(GraphicsSceneEventHandler *handler)
{
    m_eventHandlers.removeOne(handler);
}

QPen GraphicsScene::defaultPen()
{
    return g_defaultPen;
}

QBrush GraphicsScene::defaultBrush()
{
    return g_defaultBrush;
}

QGraphicsSceneMouseEvent *GraphicsScene::cloneMouseEvent(const QGraphicsSceneMouseEvent *event) const
{
    Q_ASSERT(event->type() == QEvent::GraphicsSceneMouseDoubleClick ||
             event->type() == QEvent::GraphicsSceneMouseMove ||
             event->type() == QEvent::GraphicsSceneMousePress ||
             event->type() == QEvent::GraphicsSceneMouseRelease);

    QGraphicsSceneMouseEvent *clone;
    switch (event->type()) {
        case QEvent::GraphicsSceneMouseDoubleClick:
            clone = m_clonedMouseDoubleClickEvent;
            break;
        case QEvent::GraphicsSceneMouseMove:
            clone = m_clonedMouseMoveEvent;
            break;
        case QEvent::GraphicsSceneMousePress:
            clone = m_clonedMousePressEvent;
            break;
        case QEvent::GraphicsSceneMouseRelease:
            clone = m_clonedMouseReleaseEvent;
            break;
        default:
            clone = nullptr;
            Q_UNREACHABLE();
            break;
    }
    clone->setWidget(event->widget());
    clone->setPos(event->pos());
    clone->setScenePos(event->scenePos());
    clone->setScreenPos(event->screenPos());
    for (std::underlying_type_t<Qt::MouseButton> button = 0x01; button != Qt::MaxMouseButton; button <<= 1) {
        const auto qtButton = static_cast<Qt::MouseButton>(button);
        clone->setButtonDownPos(qtButton, event->buttonDownPos(qtButton));
        clone->setButtonDownScenePos(qtButton, event->buttonDownScenePos(qtButton));
        clone->setButtonDownScreenPos(qtButton, event->buttonDownScreenPos(qtButton));
    }
    clone->setLastPos(event->lastPos());
    clone->setLastScenePos(event->lastScenePos());
    clone->setLastScreenPos(event->lastScreenPos());
    clone->setButtons(event->buttons());
    clone->setButton(event->button());
    clone->setModifiers(event->modifiers());
    clone->setSource(event->source());
    clone->setFlags(event->flags());
    return clone;
}

void GraphicsScene::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsSceneContextMenuEvent(this, event))
            return;
    QGraphicsScene::contextMenuEvent(event);
}

void GraphicsScene::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsSceneDragEnterEvent(this, event))
            return;
    QGraphicsScene::dragEnterEvent(event);
}

void GraphicsScene::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsSceneDragLeaveEvent(this, event))
            return;
    QGraphicsScene::dragLeaveEvent(event);
}

void GraphicsScene::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsSceneDragMoveEvent(this, event))
            return;
    QGraphicsScene::dragMoveEvent(event);
}

void GraphicsScene::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsSceneDropEvent(this, event))
            return;
    QGraphicsScene::dropEvent(event);
}

void GraphicsScene::keyPressEvent(QKeyEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsSceneKeyPressEvent(this, event))
            return;
    QGraphicsScene::keyPressEvent(event);
}

void GraphicsScene::keyReleaseEvent(QKeyEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsSceneKeyReleaseEvent(this, event))
            return;
    QGraphicsScene::keyReleaseEvent(event);
}

void GraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    auto clone = cloneMouseEvent(event);
    for (auto handler: m_eventHandlers)
        if (handler->graphicsSceneMousePressEvent(this, clone, event))
            return;
    QGraphicsScene::mousePressEvent(clone);
}

void GraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    auto clone = cloneMouseEvent(event);
    for (auto handler: m_eventHandlers)
        if (handler->graphicsSceneMouseMoveEvent(this, clone, event))
            return;
    QGraphicsScene::mouseMoveEvent(clone);
}

void GraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    auto clone = cloneMouseEvent(event);
    for (auto handler: m_eventHandlers)
        if (handler->graphicsSceneMouseReleaseEvent(this, clone, event))
            return;
    QGraphicsScene::mouseReleaseEvent(clone);
}

void GraphicsScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    auto clone = cloneMouseEvent(event);
    for (auto handler: m_eventHandlers)
        if (handler->graphicsSceneMouseDoubleClickEvent(this, clone, event))
            return;
    QGraphicsScene::mouseDoubleClickEvent(clone);
}

void GraphicsScene::wheelEvent(QGraphicsSceneWheelEvent *event)
{
    for (auto handler: m_eventHandlers)
        if (handler->graphicsSceneWheelEvent(this, event))
            return;
    QGraphicsScene::wheelEvent(event);
}


const QPen GraphicsScene::g_defaultPen = QPen(Qt::black, 0.25, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
const QBrush GraphicsScene::g_defaultBrush = QBrush(Qt::white, Qt::NoBrush);
