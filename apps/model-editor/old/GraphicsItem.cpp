/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsItem.h"

#include <QtCore/QDebug>
#include <QtGui/QFont>
#include <QtGui/QPainter>
#include <QtGui/QPainterPath>


GraphicsEllipseItem::GraphicsEllipseItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
{

}

GraphicsEllipseItem::~GraphicsEllipseItem()
{

}

void GraphicsEllipseItem::setSize(const QSizeF &size)
{
    if (m_size == size)
        return;

    prepareGeometryChange();
    m_size = size;
}

QSizeF GraphicsEllipseItem::size() const
{
    return m_size;
}

void GraphicsEllipseItem::setStyle(Qs::EllipseStyle style)
{
    if (m_style == style)
        return;
    m_style = style;
    update();
}

Qs::EllipseStyle GraphicsEllipseItem::style() const
{
    return m_style;
}

void GraphicsEllipseItem::setStartAngle(qreal angle)
{
    if (qFuzzyCompare(m_startAngle, angle))
        return;

    prepareGeometryChange();
    m_startAngle = angle;
}

qreal GraphicsEllipseItem::startAngle() const
{
    return m_startAngle;
}

void GraphicsEllipseItem::setSpanAngle(qreal angle)
{
    if (qFuzzyCompare(m_spanAngle, angle))
        return;

    prepareGeometryChange();
    m_spanAngle = angle;
}

qreal GraphicsEllipseItem::spanAngle() const
{
    return m_spanAngle;
}

void GraphicsEllipseItem::setPen(const QPen &pen)
{
    if (m_pen == pen)
        return;

    prepareGeometryChange();
    m_pen = pen;
}

QPen GraphicsEllipseItem::pen() const
{
    return m_pen;
}

void GraphicsEllipseItem::setBrush(const QBrush &brush)
{
    if (m_brush == brush)
        return;

    m_brush = brush;
    update();
}

QBrush GraphicsEllipseItem::brush() const
{
    return m_brush;
}

QPainterPath GraphicsEllipseItem::createPath() const
{
    QPainterPath path;
    const QRectF rect(QPointF(-m_size.width()/2.0, -m_size.height()/2.0), m_size);
    switch (m_style) {
        case Qs::EllipsoidalArc:
            path.arcMoveTo(rect, m_startAngle);
            path.arcTo(rect, m_startAngle, m_spanAngle);
            break;
        case Qs::EllipsoidalPie:
            path.moveTo(QPointF(0, 0));
            path.arcTo(rect, m_startAngle, m_spanAngle);
            path.closeSubpath();
            break;
        case Qs::EllipsoidalChord:
            path.arcMoveTo(rect, m_startAngle);
            path.arcTo(rect, m_startAngle, m_spanAngle);
            path.closeSubpath();
            break;
        case Qs::FullEllipsoid:
            path.addEllipse(rect);
            break;
    }
    return path;
}

int GraphicsEllipseItem::type() const {
    return Type;
}

QRectF GraphicsEllipseItem::boundingRect() const
{
    const QSizeF size = m_size + QSizeF(m_pen.widthF(), m_pen.widthF());
    return QRectF(QPointF(-size.width()/2.0, -size.height()/2.0), size);
}

QPainterPath GraphicsEllipseItem::shape() const
{
    QPainterPath path = createPath();
    QPainterPathStroker stroker;
    stroker.setCurveThreshold(0.1);
    stroker.setWidth(m_pen.widthF());
    stroker.setCapStyle(m_pen.capStyle());
    stroker.setJoinStyle(m_pen.joinStyle());
    stroker.setDashPattern(Qt::SolidLine);
    QPainterPath strokedPath = stroker.createStroke(path);

    if (m_style == Qs::EllipsoidalArc || m_brush.style() == Qt::NoBrush)
        return strokedPath;

    path.setFillRule(Qt::WindingFill);
    path.addPath(strokedPath);
    return path;
}

void GraphicsEllipseItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QPainterPath path = createPath();
    painter->setPen(m_pen);
    QBrush brush = m_brush;
    brush.setTransform(painter->worldTransform().inverted());
    painter->setBrush(brush);
    painter->drawPath(path);
    if (isSelected()) {
        painter->setPen(QPen(Qt::green, 0.0, Qt::DashLine));
        painter->setBrush(Qt::NoBrush);
        painter->drawPath(path);
        //painter->drawPath(shape());
    }
}



GraphicsRectangleItem::GraphicsRectangleItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
{

}

GraphicsRectangleItem::~GraphicsRectangleItem()
{

}

void GraphicsRectangleItem::setSize(const QSizeF &size)
{
    if (m_size == size)
        return;

    prepareGeometryChange();
    m_size = size;
}

QSizeF GraphicsRectangleItem::size() const
{
    return m_size;
}

void GraphicsRectangleItem::setPen(const QPen &pen)
{
    if (m_pen == pen)
        return;

    prepareGeometryChange();
    m_pen = pen;
}

QPen GraphicsRectangleItem::pen() const
{
    return m_pen;
}

void GraphicsRectangleItem::setBrush(const QBrush &brush)
{
    if (m_brush == brush)
        return;

    m_brush = brush;
    update();
}

QBrush GraphicsRectangleItem::brush() const
{
    return m_brush;
}

int GraphicsRectangleItem::type() const
{
    return Type;
}


QRectF GraphicsRectangleItem::boundingRect() const
{
    const QSizeF size = m_size + QSizeF(m_pen.widthF(), m_pen.widthF());
    return QRectF(QPointF(-size.width()/2.0, -size.height()/2.0), size);
}

QPainterPath GraphicsRectangleItem::shape() const
{
    QPainterPath path;
    path.addRect(boundingRect());
    return path;
}

void GraphicsRectangleItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setPen(m_pen);
    QBrush brush = m_brush;
    brush.setTransform(painter->worldTransform().inverted());
    painter->setBrush(brush);
    painter->drawRect(QRectF(QPointF(-m_size.width()/2.0, -m_size.height()/2.0), m_size));
    if (isSelected()) {
        painter->setPen(QPen(Qt::green, 0.0, Qt::DashLine));
        painter->setBrush(Qt::NoBrush);
        painter->drawRect(QRectF(QPointF(-m_size.width()/2.0, -m_size.height()/2.0), m_size));
        //painter->drawPath(shape());
    }
}

// TODO: For readability, Symbol's ports need to be drawn last (on top of Symbol's drawings)
// => Need to be managed at the scene level, either with layers or simply
// by fiddling the Z-index
GraphicsPortItem::GraphicsPortItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , m_size(1.5, 1.5) // FIXME Allow to change default size
    , m_textItem(new QGraphicsSimpleTextItem(this))
{
    // FIXME: For hit test of the port over it's text
    m_textItem->setFlag(QGraphicsItem::ItemStacksBehindParent);
}

GraphicsPortItem::~GraphicsPortItem()
{

}

void GraphicsPortItem::setName(const QString &name)
{
    m_textItem->setText(name);

    // Auto adjust text eight to tightly fit in the circle
    // Use scaling instead of font size to avoid platfrom dependent behaviour
    m_textItem->setScale(m_size.height()/m_textItem->boundingRect().height());

    // Keep text centered
    m_textItem->setPos(QPointF());
    m_textItem->setPos(m_textItem->mapToParent(-m_textItem->boundingRect().center()));
}

QString GraphicsPortItem::name() const
{
    return m_textItem->text();
}

void GraphicsPortItem::setPen(const QPen &pen)
{
    if (m_pen == pen)
        return;

    prepareGeometryChange();
    m_pen = pen;
    m_textItem->setBrush(m_pen.color());
}

QPen GraphicsPortItem::pen() const
{
    return m_pen;
}

int GraphicsPortItem::type() const
{
    return Type;
}

QRectF GraphicsPortItem::boundingRect() const
{
    const QSizeF size = m_size + QSizeF(m_pen.widthF(), m_pen.widthF());
    return QRectF(QPointF(-size.width()/2.0, -size.height()/2.0), size);
}

QPainterPath GraphicsPortItem::shape() const
{
    QPainterPath path;
    path.addRect(boundingRect());
    return path;
}

void GraphicsPortItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    const QRectF rect = QRectF(QPointF(-m_size.width()/2.0, -m_size.height()/2.0), m_size);
    painter->setPen(m_pen);
    painter->setBrush(m_brush);
    painter->drawEllipse(rect);
    if (isSelected()) {
        painter->setPen(QPen(Qt::green, 0.0, Qt::DashLine));
        painter->setBrush(Qt::NoBrush);
        painter->drawEllipse(rect);
        //painter->drawPath(shape());
    }
}

GraphicsLineItem::GraphicsLineItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
{

}

GraphicsLineItem::~GraphicsLineItem()
{

}

void GraphicsLineItem::setP1(const QPointF &point)
{
    if (m_p1 == point)
        return;

    prepareGeometryChange();
    m_p1 = point;
}

QPointF GraphicsLineItem::p1() const
{
    return m_p1;
}

void GraphicsLineItem::setP2(const QPointF &point)
{
    if (m_p2 == point)
        return;

    prepareGeometryChange();
    m_p2 = point;
}

QPointF GraphicsLineItem::p2() const
{
    return m_p2;
}

void GraphicsLineItem::setPen(const QPen &pen)
{
    if (m_pen == pen)
        return;

    prepareGeometryChange();
    m_pen = pen;
}

QPen GraphicsLineItem::pen() const
{
    return m_pen;
}

int GraphicsLineItem::type() const
{
    return Type;
}

QRectF GraphicsLineItem::boundingRect() const
{
    return QRectF(m_p1, m_p2).normalized().adjusted(-m_pen.widthF()/2.0, -m_pen.widthF()/2.0,
                                                    m_pen.widthF()/2.0, m_pen.widthF()/2.0);
}

QPainterPath GraphicsLineItem::shape() const
{
    QPainterPath path;
    path.moveTo(m_p1);
    path.lineTo(m_p2);
    QPainterPathStroker stroker;
    stroker.setWidth(m_pen.isCosmetic() ? 0.1 : m_pen.widthF());
    stroker.setCapStyle(m_pen.capStyle());
    stroker.setJoinStyle(m_pen.joinStyle());
    stroker.setDashPattern(Qt::SolidLine);
    return stroker.createStroke(path);
}

void GraphicsLineItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    painter->setPen(m_pen);
    painter->drawLine(m_p1, m_p2);
    if (isSelected()) {
        painter->setPen(QPen(Qt::green, 0.0, Qt::DashLine));
        painter->setBrush(Qt::NoBrush);
        painter->drawLine(m_p1, m_p2);
        //painter->drawPath(shape());
    }
}

GraphicsLabelItem::GraphicsLabelItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
    , m_textItem(new QGraphicsSimpleTextItem())
{
    // FIXME: text size in mm, pixel or point?
    // In the mean time, this scaling gives good results when using size as font point-size
    setScale(0.1);
}

GraphicsLabelItem::~GraphicsLabelItem()
{

}

void GraphicsLabelItem::setText(const QString &text)
{
    m_textItem->setText(text);
}

QString GraphicsLabelItem::text() const
{
    return m_textItem->text();
}

void GraphicsLabelItem::setFont(const QFont &font)
{
    m_textItem->setFont(font);
}

QFont GraphicsLabelItem::font() const
{
    return m_textItem->font();
}

void GraphicsLabelItem::setBrush(const QBrush &brush)
{
    m_textItem->setBrush(brush);
}

QBrush GraphicsLabelItem::brush() const
{
    return m_textItem->brush();
}

int GraphicsLabelItem::type() const
{
    return Type;
}

QRectF GraphicsLabelItem::boundingRect() const
{
    return m_textItem->boundingRect();
}

QPainterPath GraphicsLabelItem::shape() const
{
    // For hit test, better use bounding rect as shape than
    // m_text->shape() which returns the detailed shape of all
    // the individual letters
    QPainterPath path;
    path.addRect(m_textItem->boundingRect());
    return path;
}

void GraphicsLabelItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    // Delegate painting to the text item (which is not part of our scene)
    m_textItem->setPos(pos());
    m_textItem->setRotation(rotation());
    m_textItem->setScale(scale());
    m_textItem->setTransform(transform());
    m_textItem->paint(painter, option, widget);
}
