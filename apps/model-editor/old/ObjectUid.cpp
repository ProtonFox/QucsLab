/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "ObjectUid.h"

#include <QtCore/QVariant>
#include <QtCore/QDataStream>

ObjectUid ObjectUid::createId() noexcept
{
#if !defined(Q_ATOMIC_INT64_IS_SUPPORTED)
#error "No atomic support for 64 bits unsigned int"
#endif
    static QBasicAtomicInteger<quint64> next = Q_BASIC_ATOMIC_INITIALIZER(0);
    return ObjectUid(next.fetchAndAddRelaxed(1) + 1);
}

QDebug operator<<(QDebug debug, ObjectUid uid)
{
    debug << uid.value();
    return debug;
}

QDataStream &operator<<(QDataStream &out, const ObjectUid &uid)
{
    out << uid.m_value;
    return out;
}

QDataStream &operator>>(QDataStream &in, ObjectUid &uid)
{
    in >> uid.m_value;
    return in;
}
