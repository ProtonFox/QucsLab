/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtCore/QtGlobal>

//
// All this "dirty" stuff is to allow introspection of enums and flags decalred in Qs namespace
// - The moc will see a QObject with enums and flags
// - The doc will see a namespace
// - The user will see a namespace
//
// Based on Qt's qnamespace.h
//

#if !defined(Q_QDOC) && !defined(Q_MOC_RUN)
struct QMetaObject;
const QMetaObject *qt_getQtMetaObject() Q_DECL_NOEXCEPT; // defined in qobject.h (which can't be included here)
#define QS_ENUM(ENUM) \
    inline const QMetaObject *qt_getEnumMetaObject(ENUM) Q_DECL_NOEXCEPT { return qt_getQtMetaObject(); } \
    inline Q_DECL_CONSTEXPR const char *qt_getEnumName(ENUM) Q_DECL_NOEXCEPT { return #ENUM; }
#define QS_FLAG(ENUM) QS_ENUM(ENUM)
#else
#define QS_ENUM Q_ENUM
#define QS_FLAG Q_FLAG
#endif

#ifndef Q_MOC_RUN
namespace Qs {
#else
class Q_CORE_EXPORT Qs {
#endif

#if defined(Q_MOC_RUN)
    Q_GADGET
public:
#endif
    extern const QMetaObject staticMetaObject;

    enum StrokeStyle {
        SolidStroke = 0,
        DashStroke,
        DotStroke,
        DashDotStroke,
        DasDotDotStroke,
        NoStroke
    };

    enum StrokeWidth {
        SmallestStroke = 0,
        SmallStroke,
        MediumStroke,
        LargeStroke,
        LargestStroke
    };

    enum FillStyle {
        SolidFill = 0,
        HorLineFill,
        VerLineFill,
        FwLineFill,
        BwLineFill,
        HorVerLineFill,
        FwBwLineFill,
        NoFill
    };

    enum EllipseStyle {
        EllipsoidalArc = 0,
        EllipsoidalPie,
        EllipsoidalChord,
        FullEllipsoid
    };

#ifndef Q_QDOC
    QS_ENUM(StrokeStyle)
    QS_ENUM(StrokeWidth)
    QS_ENUM(FillStyle)
    QS_ENUM(EllipseStyle)
    // Flags, eg:
    // QS_FLAG(SomeFlag)
#endif
}
#ifdef Q_MOC_RUN
;
#endif

#undef QS_ENUM
#undef QS_FLAG

// Flags, eg:
// Q_DECLARE_OPERATORS_FOR_FLAGS(Qs::SomeFlags)
