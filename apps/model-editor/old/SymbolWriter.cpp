/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "Symbol.h"
#include "SymbolWriter.h"

#include <QtCore/QDebug>
#include <QtCore/QXmlStreamWriter>

class SymbolWriterPrivate
{
public:
    SymbolWriterPrivate()
        : writer(new QXmlStreamWriter())
    {}

    void writeSymbol(const Symbol *object);
    void writeEllipse(const Ellipse *object);
    void writeLine(const Line *object);
    void writeRectangle(const Rectangle *object);
    void writeLabel(const Label *object);
    void writePort(const Port *object);
    void writeDrawing(const Object *object);
    void writeStroke(const Object *object);
    void writeStrokeStyle(Qs::StrokeStyle style);
    void writeStrokeWidth(Qs::StrokeWidth width);
    void writeFill(const Object *object);
    void writeFillStyle(Qs::FillStyle style);
    void writeEllipseStyle(Qs::EllipseStyle style);
    void writeBoolean(const char *tag, bool value);
    void writeDouble(const char *tag, qreal value);
    void writeColor(const char *tag, const QColor &color);

    QScopedPointer<QXmlStreamWriter> writer;
};

SymbolWriter::SymbolWriter():
    d_ptr(new SymbolWriterPrivate)
{
}

SymbolWriter::~SymbolWriter()
{

}

// FIXME: Error handling?
bool SymbolWriter::writeDocument(QIODevice *device, const Symbol *symbol)
{
    Q_D(SymbolWriter);

    d->writer->setDevice(device);
    d->writer->setAutoFormatting(true);
    d->writer->writeStartDocument("1.0");
    d->writer->writeDefaultNamespace("http://www.leda.org/xdl");
    d->writer->writeStartElement("symbol");
    d->writer->writeAttribute("version", "1.0");
    d->writer->writeTextElement("caption", symbol->caption());
    d->writer->writeTextElement("description", symbol->description());
    for (const auto object: symbol->childObjects()) {
        auto line = qobject_cast<const Line*>(object);
        auto ellipse = qobject_cast<const Ellipse*>(object);
        auto rectangle = qobject_cast<const Rectangle*>(object);
        auto label = qobject_cast<const Label*>(object);
        auto port = qobject_cast<const Port*>(object);
        if (line != nullptr)
            d->writeLine(line);
        else if (ellipse != nullptr)
            d->writeEllipse(ellipse);
        else if (ellipse != nullptr)
            d->writeEllipse(ellipse);
        else if (rectangle != nullptr)
            d->writeRectangle(rectangle);
        else if (label != nullptr)
            d->writeLabel(label);
        else if (port != nullptr)
            d->writePort(port);
        else
            Q_UNREACHABLE();
    }
    d->writer->writeEndElement(); // symbol
    d->writer->writeEndDocument();
    return !d->writer->hasError();
}

// FIXME: Duplicated code with write()
bool SymbolWriter::writeObjects(QIODevice *device, const QList<const QObject *> &objects)
{
    Q_D(SymbolWriter);

    d->writer->setDevice(device);
    d->writer->setAutoFormatting(true);
    for (const auto object: objects) {
        auto line = qobject_cast<const Line*>(object);
        auto ellipse = qobject_cast<const Ellipse*>(object);
        auto rectangle = qobject_cast<const Rectangle*>(object);
        auto label = qobject_cast<const Label*>(object);
        auto port = qobject_cast<const Port*>(object);
        if (line != nullptr)
            d->writeLine(line);
        else if (ellipse != nullptr)
            d->writeEllipse(ellipse);
        else if (ellipse != nullptr)
            d->writeEllipse(ellipse);
        else if (rectangle != nullptr)
            d->writeRectangle(rectangle);
        else if (label != nullptr)
            d->writeLabel(label);
        else if (port != nullptr)
            d->writePort(port);
        else
            Q_UNREACHABLE();
    }
    return true;
}

QString SymbolWriter::errorString() const
{
    Q_D(const SymbolWriter);

    return d->writer->device()->errorString();
}

void SymbolWriterPrivate::writeEllipse(const Ellipse *object)
{
    writer->writeStartElement("ellipse");
    writeDrawing(object);
    writeDouble("width", object->size().width());
    writeDouble("height", object->size().height());
    writeEllipseStyle(object->style());
    writeDouble("startAngle", object->startAngle());
    writeDouble("spanAngle", object->spanAngle());
    writeStroke(object);
    writeFill(object);
    writer->writeEndElement();
}

void SymbolWriterPrivate::writeLine(const Line *object)
{
    writer->writeStartElement("line");
    writeDrawing(object);
    writeDouble("x1", object->p1().x() + object->location().x());
    writeDouble("y1", object->p1().y() + object->location().y());
    writeDouble("x2", object->p2().x() + object->location().x());
    writeDouble("y2", object->p2().y() + object->location().y());
    writeStroke(object);
    writer->writeEndElement();
}

void SymbolWriterPrivate::writeRectangle(const Rectangle *object)
{
    writer->writeStartElement("rectangle");
    writeDrawing(object);
    writeDouble("width", object->size().width());
    writeDouble("height", object->size().height());
    writeStroke(object);
    writeFill(object);
    writer->writeEndElement();
}

void SymbolWriterPrivate::writeLabel(const Label *object)
{
    writer->writeStartElement("label");
    writeDrawing(object);
    writer->writeAttribute("text", object->text());
    writeDouble("size", object->size());
    writer->writeAttribute("color", object->color().name());
    writer->writeEndElement();
}

void SymbolWriterPrivate::writePort(const Port *object)
{
    writer->writeStartElement("port");
    writeDrawing(object);
    writer->writeAttribute("name", object->name());
    writer->writeEndElement();
}

void SymbolWriterPrivate::writeDrawing(const Object *object)
{
    writeDouble("x", object->location().x());
    writeDouble("y", object->location().y());
    if (object->rotation() != 0.0)
        writeDouble("rotation", object->rotation());
    if (object->isLocked())
        writeBoolean("locked", object->isLocked());
    if (object->isMirrored())
        writeBoolean("mirrored", object->isMirrored());
    if (!object->isVisible())
        writeBoolean("visible", object->isVisible());
}

void SymbolWriterPrivate::writeStroke(const Object *object)
{
    writer->writeStartElement("stroke");
    writeStrokeStyle(object->property("strokeStyle").value<Qs::StrokeStyle>());
    writeStrokeWidth(object->property("strokeWidth").value<Qs::StrokeWidth>());
    writeColor("color", object->property("strokeColor").value<QColor>());
    writer->writeEndElement();
}

void SymbolWriterPrivate::writeFill(const Object *object)
{
    writer->writeStartElement("fill");
    writeFillStyle(object->property("fillStyle").value<Qs::FillStyle>());
    writeColor("color", object->property("fillColor").value<QColor>());
    writer->writeEndElement();
}

void SymbolWriterPrivate::writeFillStyle(Qs::FillStyle style)
{
    const char *value;
    switch (style) {
        case Qs::SolidFill:
            value = "solid";
            break;
        case Qs::HorLineFill:
            value = "hor";
            break;
        case Qs::VerLineFill:
            value = "ver";
            break;
        case Qs::HorVerLineFill:
            value = "horver";
            break;
        case Qs::FwLineFill:
            value = "fw";
            break;
        case Qs::BwLineFill:
            value = "bw";
            break;
        case Qs::FwBwLineFill:
            value = "fwbw";
            break;
        case Qs::NoFill:
        default:
            value = "none";
            break;
    }
    writer->writeAttribute("style", value);
}

void SymbolWriterPrivate::writeStrokeStyle(Qs::StrokeStyle style)

{
    const char *value;
    switch (style) {
        case Qs::DashStroke:
            value = "dash";
            break;
        case Qs::DotStroke:
            value = "dot";
            break;
        case Qs::DashDotStroke:
            value = "dashdot";
            break;
        case Qs::DasDotDotStroke:
            value = "dashdotdot";
            break;
        case Qs::NoStroke:
            value = "none";
            break;
        case Qs::SolidStroke:
        default:
            value = "solid";
            break;
    }
    writer->writeAttribute("style", value);
}

void SymbolWriterPrivate::writeStrokeWidth(Qs::StrokeWidth width)
{
    const char *value;
    switch (width) {
        case Qs::SmallestStroke:
            value = "smallest";
            break;
        case Qs::SmallStroke:
            value = "small";
            break;
        case Qs::LargeStroke:
            value = "large";
            break;
        case Qs::LargestStroke:
            value = "largest";
            break;
        case Qs::MediumStroke:
        default:
            value = "medium";
            break;
    }
    writer->writeAttribute("width", value);
}

void SymbolWriterPrivate::writeEllipseStyle(Qs::EllipseStyle style)
{
    static const char *name = "style";
    switch (style) {
        case Qs::EllipsoidalArc:
            writer->writeAttribute(name, "arc");
            break;
        case Qs::EllipsoidalPie:
            writer->writeAttribute(name, "pie");
            break;
        case Qs::EllipsoidalChord:
            writer->writeAttribute(name, "chord");
            break;
        case Qs::FullEllipsoid:
            writer->writeAttribute(name, "full");
            break;
    }
}

void SymbolWriterPrivate::writeBoolean(const char *tag, bool value)
{
    writer->writeAttribute(tag, value ? "true" : "false");
}

void SymbolWriterPrivate::writeDouble(const char *tag, qreal value)
{
    writer->writeAttribute(tag, QString::number(value));
}

void SymbolWriterPrivate::writeColor(const char *tag, const QColor &color)
{
    writer->writeAttribute(tag, color.name());
}
