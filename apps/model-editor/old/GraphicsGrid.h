/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtGui/QPen>
#include <QtWidgets/QGraphicsItem>

// FIXME: Add accessor
class GraphicsGrid : public QGraphicsItem
{
public:
    GraphicsGrid(QGraphicsItem *parent = nullptr);
    ~GraphicsGrid();

    void setRect(const QRectF &rect);
    QRectF rect() const;
    void setColor(const QColor &color);
    QColor color() const;
    void setStep(qreal step);
    qreal step() const;
    void setMinimumGridSize(int pixels);
    int minimumGridSize() const;

private:
    QRectF m_rect;
    QPen m_pen;
    qreal m_step;
    int m_minimumGridSize;

    // QGraphicsItem interface
public:
    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
};
