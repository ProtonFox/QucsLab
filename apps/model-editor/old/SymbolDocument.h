/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtCore/QObject>

#include "ObjectUid.h"

class Symbol;
class Object;

// TODO: Compose:
//  - a document (load, save)
//  - a top level object: Symbol
//  - an undo stack
//  - a selection model

class SymbolDocument : public QObject
{
    Q_OBJECT
public:
    explicit SymbolDocument(QObject *parent = 0);
    explicit SymbolDocument(Symbol *symbol, QObject *parent = 0);
    ~SymbolDocument();

    // CRUD
    ObjectUid createObject(const QString &typeName, ObjectUid uid = ObjectUid());
    QMap<QString, QVariant> readObject(ObjectUid uid) const;
    void updateObject(ObjectUid uid, const QMap<QString, QVariant> &properties);
    void deleteObject(ObjectUid uid);

    // Introspection
    const Symbol *symbol() const;
    const Object *object(ObjectUid uid) const;
    QList<ObjectUid> objectUids() const;
    int objectCount() const;

    // Selection FIXME: marked as const b/c of RO access to document
    // and we still want to allow to change selection
    // Selection should not be part of document (see QAbstractView selection model)
    void select(const QSet<ObjectUid> &objects) const;
    QList<const QObject *> selectedObjects() const;
    void selectAll() const;
    void clearSelection() const;
    void invertSelection() const;
    QSet<ObjectUid> selection() const;

    // Observer
signals:
    void aboutToAddObject();
    void objectAdded(ObjectUid uid);
    void aboutToRemoveObject(ObjectUid uid);
    void objectRemoved();
    void aboutToUpdateObject(ObjectUid uid);
    void objectUpdated(ObjectUid uid);
    void selectionChanged(const QSet<ObjectUid> &current, const QSet<ObjectUid> &previous) const;

public slots:

private:
    QScopedPointer<Symbol> m_root;
    QHash<ObjectUid, Object *> m_objectLookupTable;
    mutable QSet<ObjectUid> m_selection;
};
