/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtWidgets/QGraphicsScene>

#include "ObjectUid.h"

class GraphicsSceneEventHandler;

class GraphicsScene : public QGraphicsScene
{
public:
    enum { // FIXME: global namespace?
        DocumentIdProperty = 0,
    };

    GraphicsScene(QObject *parent = nullptr);
    ~GraphicsScene();

    // TODO: move into GraphicsWidget
    void addGraphicsItem(QGraphicsItem *graphicsItem, ObjectUid uid);
    ObjectUid uid(const QGraphicsItem *graphicsItem) const;
    QGraphicsItem *graphicsItem(ObjectUid uid) const;
    QGraphicsItem *takeGraphicsItem(ObjectUid uid);

    void installEventHandler(GraphicsSceneEventHandler *handler);
    void uninstallEventHandler(GraphicsSceneEventHandler *handler);

    static QPen defaultPen();
    static QBrush defaultBrush();

private:
    QList<GraphicsSceneEventHandler *> m_eventHandlers;
    QHash<ObjectUid, QGraphicsItem *> m_itemLookupTable;
    QPointF m_lastMousePos;
    static const QPen g_defaultPen;
    static const QBrush g_defaultBrush;
    QGraphicsSceneMouseEvent *m_clonedMouseDoubleClickEvent;
    QGraphicsSceneMouseEvent *m_clonedMouseMoveEvent;
    QGraphicsSceneMouseEvent *m_clonedMousePressEvent;
    QGraphicsSceneMouseEvent *m_clonedMouseReleaseEvent;
    QGraphicsSceneMouseEvent *cloneMouseEvent(const QGraphicsSceneMouseEvent *event) const;

    // QGraphicsScene interface
protected:
    virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;
    virtual void dragEnterEvent(QGraphicsSceneDragDropEvent *event) override;
    virtual void dragLeaveEvent(QGraphicsSceneDragDropEvent *event) override;
    virtual void dragMoveEvent(QGraphicsSceneDragDropEvent *event) override;
    virtual void dropEvent(QGraphicsSceneDragDropEvent *event) override;
    virtual void keyPressEvent(QKeyEvent *event) override;
    virtual void keyReleaseEvent(QKeyEvent *event) override;
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
    virtual void wheelEvent(QGraphicsSceneWheelEvent *event) override;
};
