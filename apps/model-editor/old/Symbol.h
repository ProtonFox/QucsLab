/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtCore/QPointF>
#include <QtCore/QRectF>
#include <QtCore/QSizeF>
#include <QtCore/QString>

// FIXME: No QtGui dependency
#include <QtGui/QColor>

#include "ObjectUid.h"
#include "Qstudio.h"

class Object: public QObject
{
    Q_OBJECT
    Q_PROPERTY(ObjectUid uid READ uid NOTIFY uidChanged)
    Q_PROPERTY(QPointF location READ location WRITE setLocation NOTIFY locationChanged)
    Q_PROPERTY(qreal rotation READ rotation WRITE setRotation NOTIFY rotationChanged)
    Q_PROPERTY(bool mirrored READ isMirrored WRITE setMirrored NOTIFY mirroredChanged)
    Q_PROPERTY(bool visible READ isVisible WRITE setVisible NOTIFY visibleChanged)
    Q_PROPERTY(bool locked READ isLocked WRITE setLocked NOTIFY lockedChanged)

public:
    explicit Object(Object *parent = nullptr);
    ~Object();

    Object *parentObject();
    const Object *parentObject() const;

    QList<Object *> childObjects();
    QList<const Object *> childObjects() const;

    ObjectUid uid() const;

    QPointF location() const;
    qreal rotation() const;
    bool isMirrored() const;
    bool isVisible() const;
    bool isLocked() const;

public slots:
    void setLocation(const QPointF &location);
    void setRotation(qreal rotation);
    void setMirrored(bool mirrored);
    void setVisible(bool visible);
    void setLocked(bool locked);

signals:
    void locationChanged(const QPointF &location);
    void rotationChanged(qreal rotation);
    void mirroredChanged(bool mirrored);
    void visibleChanged(bool visible);
    void lockedChanged(bool locked);
    void uidChanged(ObjectUid uid);

private:
    ObjectUid m_uid;
    QPointF m_location;
    qreal m_rotation = 0.0;
    bool m_mirrored = false;
    bool m_visible = true;
    bool m_locked = false;

    // Needed to restore uid for undo/redo
    friend class SymbolDocument;
    void setUid(ObjectUid uid);
};

class Ellipse: public Object
{
    Q_OBJECT
    Q_PROPERTY(QSizeF size READ size WRITE setSize NOTIFY sizeChanged)
    Q_PROPERTY(qreal startAngle READ startAngle WRITE setStartAngle NOTIFY startAngleChanged)
    Q_PROPERTY(qreal spanAngle READ spanAngle WRITE setSpanAngle NOTIFY spanAngleChanged)
    Q_PROPERTY(Qs::EllipseStyle style READ style WRITE setStyle NOTIFY styleChanged)
    Q_PROPERTY(Qs::StrokeWidth strokeWidth READ strokeWidth WRITE setStrokeWidth NOTIFY strokeWidthChanged)
    Q_PROPERTY(Qs::StrokeStyle strokeStyle READ strokeStyle WRITE setStrokeStyle NOTIFY strokeStyleChanged)
    Q_PROPERTY(QColor strokeColor READ strokeColor WRITE setStrokeColor NOTIFY strokeColorChanged)
    Q_PROPERTY(Qs::FillStyle fillStyle READ fillStyle WRITE setFillStyle NOTIFY fillStyleChanged)
    Q_PROPERTY(QColor fillColor READ fillColor WRITE setFillColor NOTIFY fillColorChanged)

public:

    explicit Ellipse(Object *parent = nullptr);
    ~Ellipse();

    QSizeF size() const;
    qreal startAngle() const;
    qreal spanAngle() const;
    Qs::EllipseStyle style() const;
    Qs::StrokeWidth strokeWidth() const;
    Qs::StrokeStyle strokeStyle() const;
    QColor strokeColor() const;
    Qs::FillStyle fillStyle() const;
    QColor fillColor() const;

public slots:
    void setSize(const QSizeF &size);
    void setStartAngle(qreal startAngle);
    void setSpanAngle(qreal spanAngle);
    void setStyle(Qs::EllipseStyle style);
    void setStrokeWidth(Qs::StrokeWidth strokeWidth);
    void setStrokeStyle(Qs::StrokeStyle strokeStyle);
    void setStrokeColor(const QColor &strokeColor);
    void setFillStyle(Qs::FillStyle fillStyle);
    void setFillColor(const QColor &fillColor);

signals:
    void sizeChanged(const QSizeF &size);
    void startAngleChanged(qreal startAngle);
    void spanAngleChanged(qreal spanAngle);
    void styleChanged(Qs::EllipseStyle style);
    void strokeWidthChanged(Qs::StrokeWidth strokeWidth);
    void strokeStyleChanged(Qs::StrokeStyle strokeStyle);
    void strokeColorChanged(const QColor &strokeColor);
    void fillStyleChanged(Qs::FillStyle fillStyle);
    void fillColorChanged(const QColor &fillColor);

private:
    QSizeF m_size;
    qreal m_startAngle = 0.0;
    qreal m_spanAngle = 360.0;
    Qs::EllipseStyle m_style = Qs::FullEllipsoid;
    Qs::StrokeWidth m_strokeWidth = Qs::MediumStroke;
    Qs::StrokeStyle m_strokeStyle = Qs::SolidStroke;
    QColor m_strokeColor = Qt::black;
    Qs::FillStyle m_fillStyle = Qs::NoFill;
    QColor m_fillColor = Qt::white;
};

class Rectangle: public Object
{
    Q_OBJECT
    Q_PROPERTY(QSizeF size READ size WRITE setSize NOTIFY sizeChanged)
    Q_PROPERTY(Qs::StrokeWidth strokeWidth READ strokeWidth WRITE setStrokeWidth NOTIFY strokeWidthChanged)
    Q_PROPERTY(Qs::StrokeStyle strokeStyle READ strokeStyle WRITE setStrokeStyle NOTIFY strokeStyleChanged)
    Q_PROPERTY(QColor strokeColor READ strokeColor WRITE setStrokeColor NOTIFY strokeColorChanged)
    Q_PROPERTY(Qs::FillStyle fillStyle READ fillStyle WRITE setFillStyle NOTIFY fillStyleChanged)
    Q_PROPERTY(QColor fillColor READ fillColor WRITE setFillColor NOTIFY fillColorChanged)

public:
    explicit Rectangle(Object *parent = nullptr);
    ~Rectangle();

    QSizeF size() const;
    Qs::StrokeWidth strokeWidth() const;
    Qs::StrokeStyle strokeStyle() const;
    QColor strokeColor() const;
    Qs::FillStyle fillStyle() const;
    QColor fillColor() const;

public slots:
    void setSize(QSizeF size);
    void setStrokeWidth(Qs::StrokeWidth strokeWidth);
    void setStrokeStyle(Qs::StrokeStyle strokeStyle);
    void setStrokeColor(const QColor &strokeColor);
    void setFillStyle(Qs::FillStyle fillStyle);
    void setFillColor(const QColor &fillColor);

signals:
    void sizeChanged(const QSizeF &size);
    void strokeWidthChanged(Qs::StrokeWidth strokeWidth);
    void strokeStyleChanged(Qs::StrokeStyle strokeStyle);
    void strokeColorChanged(const QColor &strokeColor);
    void fillStyleChanged(Qs::FillStyle fillStyle);
    void fillColorChanged(const QColor &fillColor);

private:
    QSizeF m_size;
    Qs::StrokeWidth m_strokeWidth = Qs::MediumStroke;
    Qs::StrokeStyle m_strokeStyle = Qs::SolidStroke;
    QColor m_strokeColor = Qt::black;
    Qs::FillStyle m_fillStyle = Qs::NoFill;
    QColor m_fillColor = Qt::white;
};

class Line: public Object
{
    Q_OBJECT
    Q_PROPERTY(QPointF p1 READ p1 WRITE setP1 NOTIFY p1Changed)
    Q_PROPERTY(QPointF p2 READ p2 WRITE setP2 NOTIFY p2Changed)
    Q_PROPERTY(Qs::StrokeWidth strokeWidth READ strokeWidth WRITE setStrokeWidth NOTIFY strokeWidthChanged)
    Q_PROPERTY(Qs::StrokeStyle strokeStyle READ strokeStyle WRITE setStrokeStyle NOTIFY strokeStyleChanged)
    Q_PROPERTY(QColor strokeColor READ strokeColor WRITE setStrokeColor NOTIFY strokeColorChanged)

public:
    explicit Line(Object *parent = nullptr);
    ~Line();

    QPointF p1() const;
    QPointF p2() const;
    Qs::StrokeWidth strokeWidth() const;
    Qs::StrokeStyle strokeStyle() const;
    QColor strokeColor() const;

public slots:
    void setP1(const QPointF &point);
    void setP2(const QPointF &point);
    void setStrokeWidth(Qs::StrokeWidth strokeWidth);
    void setStrokeStyle(Qs::StrokeStyle strokeStyle);
    void setStrokeColor(const QColor &strokeColor);

signals:
    void p1Changed(const QPointF &point);
    void p2Changed(const QPointF &point);
    void strokeWidthChanged(Qs::StrokeWidth strokeWidth);
    void strokeStyleChanged(Qs::StrokeStyle strokeStyle);
    void strokeColorChanged(const QColor &strokeColor);

private:
    QPointF m_p1;
    QPointF m_p2;
    Qs::StrokeWidth m_strokeWidth = Qs::MediumStroke;
    Qs::StrokeStyle m_strokeStyle = Qs::SolidStroke;
    QColor m_strokeColor = Qt::black;
};

class Label: public Object
{
    Q_OBJECT
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(qreal size READ size WRITE setSize NOTIFY sizeChanged)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)

public:
    explicit Label(Object *parent = nullptr);
    ~Label();

    QString text() const;
    qreal size() const;
    QColor color() const;

public slots:
    void setText(const QString &text);
    void setSize(qreal size);
    void setColor(const QColor &color);

signals:
    void textChanged(const QString &text);
    void sizeChanged(qreal size);
    void colorChanged(const QColor &color);

private:
    QString m_text;
    qreal m_size = 10.0;
    QColor m_color = Qt::black;
};

// TBD: Use sequence number instead of name, does a name makes sense for a symbol?
class Port: public Object
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)

public:
    explicit Port(Object *parent = nullptr);
    ~Port();

    QString name() const;

public slots:
    void setName(const QString &name);

signals:
    void nameChanged(const QString &name);

private:
    QString m_name;
};

// FIXME: Split b/w Object and DrawingObject
// In the symbol editor context, Symbol has no location, rotation, ...
// In the schematics editor context, a symbol *instance* has location, rotation, ...
class Symbol: public Object
{
    Q_OBJECT
    Q_PROPERTY(QString caption READ caption WRITE setCaption NOTIFY captionChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)

public:
    explicit Symbol(Object *parent = nullptr);
    ~Symbol();

    QString caption() const;
    QString description() const;

public slots:
    void setCaption(QString caption);
    void setDescription(QString description);

signals:
    void captionChanged(QString caption);
    void descriptionChanged(QString description);

private:
    QString m_caption;
    QString m_description;
};

// Proxies a read-only Object's properties, and generate
// UpdateCommand when properties are changed
// Allowing to transparently change the properties of a document object
// w/o bypassing the undo framework
class UndoCommand;
class UpdateObjectCommand;
class ObjectProxy: public QObject
{
    Q_OBJECT
public:
    ObjectProxy(QObject *parent = nullptr);
    ~ObjectProxy();

    void setObject(const Object *object);
    const Object *object() const;

    UndoCommand *takeCommand();

signals:
    void commandAvailable();

private:
    const Object *m_object = nullptr;
    QScopedPointer<UpdateObjectCommand> m_command;

    // QObject interface
public:
    virtual bool event(QEvent *event) override;
};
