#-------------------------------------------------
#
# Project created by QtCreator 2017-05-05T17:00:13
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = model-editor
TEMPLATE = app

DEFINES += DATADIR=\\\"$$PWD/../../data/components/\\\"

SOURCES += main.cpp\
        MainWindow.cpp \
    ParameterTableModel.cpp \
    ParameterTableWidget.cpp \
    PortTableModel.cpp \
    ComponentMetadataForm.cpp \
    ItemDelegates.cpp \
    PortTableWidget.cpp \
    ComponentInterfaceForm.cpp \
    ImplementationListModel.cpp \
    ComponentImplementationForm.cpp \
    LibraryModel.cpp \
    LibraryContentNavigationWidget.cpp \
    ComponentImplementationMultiForm.cpp \
    ComponentRepresentationMultiForm.cpp \
    FileSystemNavigationWidget.cpp \
    GraphicsWidget.cpp \
    GraphicsToolPropertyWidget.cpp \
    GraphicsTool.cpp \
    DocumentNavigationWidget.cpp \
    DocumentOutlineModel.cpp

HEADERS  += MainWindow.h \
    ParameterTableModel.h \
    ParameterTableWidget.h \
    PortTableModel.h \
    ComponentMetadataForm.h \
    ItemDelegates.h \
    PortTableWidget.h \
    ComponentInterfaceForm.h \
    ImplementationListModel.h \
    ComponentImplementationForm.h \
    LibraryModel.h \
    LibraryContentNavigationWidget.h \
    ComponentImplementationMultiForm.h \
    ComponentRepresentationMultiForm.h \
    FileSystemNavigationWidget.h \
    GraphicsWidget.h \
    GraphicsToolPropertyWidget.h \
    GraphicsTool.h \
    DocumentNavigationWidget.h \
    DocumentOutlineModel.h

FORMS += \
    ComponentMetadataForm.ui


include($$top_srcdir/QucsLab.pri)
include($$top_srcdir/libs/qlCore/qlCore.pri)
include($$top_srcdir/libs/qlGui/qlGui.pri)
