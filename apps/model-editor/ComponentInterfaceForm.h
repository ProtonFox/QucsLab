/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "qlCore/LibraryObjects.h"

#include <QtWidgets/QWidget>

class PortTableWidget;
class ParameterTableWidget;

class ComponentInterfaceForm : public QWidget
{
    Q_OBJECT

public:
    explicit ComponentInterfaceForm(QWidget *parent = 0);
    ~ComponentInterfaceForm();

    void setInterface(const QList<lib::Port *> &ports, const QList<lib::Parameter *> &parameters);

signals:

public slots:

private:
    QList<lib::Port *> m_ports;
    QList<lib::Parameter *> m_parameters;
    PortTableWidget *m_portWidget = nullptr;
    ParameterTableWidget *m_parameterWidget = nullptr;
};
