/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "ItemDelegates.h"
#include "PortTableModel.h"
#include "PortTableWidget.h"

#include "qlCore/LibraryObjects.h"

#include <QtCore/QStringListModel>
#include <QtCore/QSortFilterProxyModel>
#include <QtGui/QRegularExpressionValidator>
#include <QtWidgets/QCompleter>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>

PortTableWidget::PortTableWidget(QWidget *parent)
    : QWidget(parent)
    , m_sortFilterModel(new QSortFilterProxyModel(this))
    , m_model(new PortTableModel(this))
    , m_view(new QTableView())
    , m_directionDelegate(new EnumItemDelegate(this))
    , m_nameDelegate(new StringItemDelegate(this))
    , m_nameListModel(new QStringListModel(this))
    , m_nameCompleter(new QCompleter(this))
    , m_nameValidator(new QRegularExpressionValidator(this))
    , m_filterLineEdit(new QLineEdit())
{
    m_model->setInvalidItemBackgroundBrush(QBrush(QColor("#dc322f")));

    m_sortFilterModel->setSourceModel(m_model);
    m_sortFilterModel->setFilterKeyColumn(-1); // All columns

    m_nameValidator->setRegularExpression(QRegularExpression("[a-zA-Z][a-zA-Z0-9]*",
                                                             QRegularExpression::OptimizeOnFirstUsageOption));
    m_nameCompleter->setModel(m_nameListModel);
    m_nameCompleter->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
    m_nameCompleter->setFilterMode(Qt::MatchContains);
    m_nameDelegate->setCompleter(m_nameCompleter);
    m_nameDelegate->setValidator(m_nameValidator);
    m_model->setNameValidator(m_nameValidator);
    m_view->setItemDelegateForColumn(PortTableModel::NameSection, m_nameDelegate);

    m_directionDelegate->addEnum(Qs::InputPort, "Input");
    m_directionDelegate->addEnum(Qs::OutputPort, "Output");
    m_directionDelegate->addEnum(Qs::InputOutputPort, "Bidirectional");
    m_view->setItemDelegateForColumn(PortTableModel::DirectionSection, m_directionDelegate);

    m_view->setModel(m_sortFilterModel);
    m_view->setSortingEnabled(true);
    m_view->horizontalHeader()->setStretchLastSection(true);

    m_filterLineEdit->setPlaceholderText("Filter..."); // FIXME: user history file
    connect(m_filterLineEdit, &QLineEdit::textChanged,
            m_sortFilterModel, &QSortFilterProxyModel::setFilterWildcard);

    setLayout(new QVBoxLayout());
    layout()->addWidget(m_filterLineEdit);
    layout()->addWidget(m_view);

}

void PortTableWidget::setPorts(const QList<lib::Port *> &ports)
{
    m_model->setPortList(ports);
}

QList<lib::Port *> PortTableWidget::ports() const
{
    return m_model->portList();
}
