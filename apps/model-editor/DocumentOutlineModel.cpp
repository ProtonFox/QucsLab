/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "DocumentOutlineModel.h"

#include "qlCore/SymbolObjects.h"
#include "qlCore/SymbolDocument.h"

DocumentOutlineModel::DocumentOutlineModel(QObject *parent)
    : QAbstractItemModel(parent)
    , m_document(nullptr)
{
}

void DocumentOutlineModel::setDocument(const SymbolDocument *document)
{
    beginResetModel();

    if (m_document != nullptr)
        endMonitorDocument();

    m_document = document;

    if (m_document != nullptr)
        beginMonitorDocument();

    endResetModel();
}

const SymbolDocument *DocumentOutlineModel::document() const
{
    return m_document;
}

ObjectUid DocumentOutlineModel::objectUid(const QModelIndex &index) const
{
    if (isDocumentIndex(index))
        return m_document->symbol()->uid();

    // FIXME: Don't rely on QObject tree
    const Object *object = qobject_cast<const Object*>(m_document->symbol()->children().value(index.row()));
    return object->uid();
}

QModelIndex DocumentOutlineModel::objectIndex(ObjectUid uid) const
{
    if (uid == m_document->symbol()->uid())
        return documentIndex();
    int row = m_document->symbol()->children().indexOf(const_cast<Object*>(m_document->object(uid)));
    return itemIndex(row);
}

QVariant DocumentOutlineModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (section != 0)
        return QVariant();
    if (orientation != Qt::Horizontal)
        return QVariant();
    if (role != Qt::DisplayRole)
        return QVariant();
    return "Object";
}

QModelIndex DocumentOutlineModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_ASSERT(column == 0);

    if (isRootIndex(parent)) {
        Q_ASSERT(row == 0);
        return documentIndex();
    }

    return itemIndex(row);
}

QModelIndex DocumentOutlineModel::parent(const QModelIndex &index) const
{
    Q_ASSERT(index.isValid());

    if (isDocumentIndex(index))
        return rootIndex();

    return documentIndex();
}

int DocumentOutlineModel::rowCount(const QModelIndex &parent) const
{
    if (m_document == nullptr)
        return 0;

    if (isRootIndex(parent))
        return 1;

    if (isDocumentIndex(parent)) {
        return m_document->objectCount();
    }

    return 0;
}

int DocumentOutlineModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 1;
}

QVariant DocumentOutlineModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    if (isDocumentIndex(index))
        return "Symbol";

    const QObject *object = m_document->symbol()->children().value(index.row());
    return object->metaObject()->className();
}

void DocumentOutlineModel::beginMonitorDocument()
{
    connect(m_document, &SymbolDocument::aboutToAddObject,
            this, [this]() {
        this->beginInsertRows(documentIndex(),            // parent
                              m_document->objectCount(),  // first
                              m_document->objectCount()); // last
    });
    connect(m_document, &SymbolDocument::objectAdded,
            this, &DocumentOutlineModel::endInsertRows);
    connect(m_document, &SymbolDocument::aboutToRemoveObject,
            this, [this](ObjectUid documentId) {
        // FIXME: Don't rely on QObject tree
        int row = m_document->symbol()->children().indexOf(const_cast<Object*>(m_document->object(documentId)));
        this->beginRemoveRows(documentIndex(), // parent
                              row,      // first
                              row);     // last
    });
    connect(m_document, &SymbolDocument::objectRemoved,
            this, &DocumentOutlineModel::endRemoveRows);
}

void DocumentOutlineModel::endMonitorDocument()
{
    m_document->disconnect(this);
}

QModelIndex DocumentOutlineModel::rootIndex() const
{
    return QModelIndex();
}

bool DocumentOutlineModel::isRootIndex(const QModelIndex &index) const
{
    return !index.isValid();
}

bool DocumentOutlineModel::isDocumentIndex(const QModelIndex &index) const
{
    return index.internalPointer() == (void*)(-1);
}

QModelIndex DocumentOutlineModel::documentIndex() const
{
    return createIndex(0, 0, (void*)(-1));
}

QModelIndex DocumentOutlineModel::itemIndex(int documentId) const
{
    return createIndex(documentId, 0, (void*)(nullptr));
}
