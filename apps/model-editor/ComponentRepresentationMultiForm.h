/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtCore/QMap>
#include <QtWidgets/QWidget>

namespace lib
{
    class SymbolView;
}

class GraphicsWidget;

class QListView;
class QStackedWidget;
class QStringListModel;

class SymbolViewMultiForm : public QWidget
{
    Q_OBJECT
public:
    explicit SymbolViewMultiForm(QWidget *parent = 0);

    void setViews(const QList<lib::SymbolView *> &views);
    QList<lib::SymbolView *> representations() const;

signals:

public slots:

private:
    QList<lib::SymbolView *> m_symbolViewList;
    QStringListModel *m_listModel;
    QListView *m_listView = nullptr;
    QStackedWidget *m_stackedWidget = nullptr;
    QMap<QString, GraphicsWidget*> m_nameToGraphicsWidget;
};
