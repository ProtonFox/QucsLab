/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "ComponentMetadataForm.h"
#include "ComponentInterfaceForm.h"
#include "ComponentImplementationMultiForm.h"
#include "ComponentRepresentationMultiForm.h"
#include "FileSystemNavigationWidget.h"
#include "LibraryContentNavigationWidget.h"
#include "MainWindow.h"

#include "qlCore/LibraryObjects.h"
#include "qlCore/LibraryReader.h"

#include <QtCore/QFile>
#include <QtCore/QXmlStreamReader>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QMessageBox>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_editor(new QTabWidget())
    , m_metadataForm(new ComponentMetadataForm())
    , m_interfaceForm(new ComponentInterfaceForm())
    , m_sourceCodeViewMultiForm(new SourceCodeViewMultiForm())
    , m_symbolViewMultiForm(new SymbolViewMultiForm())
    , m_lcNavigationWidget(new LibraryContentNavigationWidget())
    , m_lcNavigationDockWidget(new QDockWidget())
    , m_fsNavigationWidget(new FileSystemNavigationWidget())
    , m_fsNavigationDockWidget(new QDockWidget())
{
    //    createFakeComponent();
    //    editComponent();

    m_editor->addTab(m_metadataForm, "Meta-data");
    m_editor->addTab(m_interfaceForm, "Interface");
    m_editor->addTab(m_sourceCodeViewMultiForm, "Implementations");
    m_editor->addTab(m_symbolViewMultiForm, "Representations");
    m_editor->setCurrentIndex(0);
    setCentralWidget(m_editor);

    setupFsNavigator();
    setupLcNavigator();
}

MainWindow::~MainWindow()
{
}

void MainWindow::openDocument(const QString &path)
{
    QFile file(path);
    if (!file.open(QFile::ReadOnly)) {
        QMessageBox::critical(this, "Error", file.errorString());
        return;
    }
    QXmlStreamReader xml;
    xml.setDevice(&file);
    xml.readNextStartElement(); // library
    lib::CellStreamReader reader;
    reader.setXml(&xml);
    lib::Library *library;
    if ((library = reader.readLibrary()) == nullptr) {
        QMessageBox::critical(this, "Error", xml.errorString());
        return;
    }
    file.close();

    if (xml.hasError()) {
        QMessageBox::critical(this,
                              "Error",
                              QString("File '%1'\n"
                                      "Line %2, column %3, offset %4\n"
                                      "Error: %5")
                              .arg(file.fileName()).arg(xml.lineNumber())
                              .arg(xml.columnNumber()).arg(xml.characterOffset())
                              .arg(xml.errorString()));
        return;
    }

    editLibrary(library);
}

void MainWindow::setupFsNavigator()
{
    m_fsNavigationDockWidget->setObjectName("org.qucs.symboleditor.filesystem");
    m_fsNavigationDockWidget->setWidget(m_fsNavigationWidget);
    m_fsNavigationDockWidget->setWindowTitle("File system");
    m_fsNavigationWidget->setInitialPath(DATADIR);
    addDockWidget(Qt::LeftDockWidgetArea, m_fsNavigationDockWidget);
    connect(m_fsNavigationWidget, &FileSystemNavigationWidget::fileActivated,
            this, &MainWindow::openDocument);
}

void MainWindow::setupLcNavigator()
{
    m_lcNavigationDockWidget->setObjectName("org.qucs.modeleditor.library");
    m_lcNavigationDockWidget->setWidget(m_lcNavigationWidget);
    m_lcNavigationDockWidget->setWindowTitle("Library");
    addDockWidget(Qt::RightDockWidgetArea, m_lcNavigationDockWidget);
    connect(m_lcNavigationWidget, &LibraryContentNavigationWidget::componentActivated,
            this, &MainWindow::editCell);
}


void MainWindow::editLibrary(lib::Library *library)
{
    m_currentLibrary = library;
    m_lcNavigationWidget->setLibrary(m_currentLibrary);
    if (!m_currentLibrary->cells.isEmpty())
        editCell(m_currentLibrary->cells.first());
    else
        editCell(nullptr);
}

void MainWindow::editCell(lib::Cell *cell)
{
    m_currentCell = cell;
    if (cell == nullptr) {
        m_editor->hide();
        return;
    }

    m_metadataForm->setIdentification(cell->identification);
    m_metadataForm->setAttribution(cell->attribution);
    m_metadataForm->setDocumentation(cell->documentation);
    m_interfaceForm->setInterface(cell->ports, cell->parameters);
    m_sourceCodeViewMultiForm->setCell(cell);
    m_sourceCodeViewMultiForm->setViews(cell->views<lib::SourceCodeView>());
    m_symbolViewMultiForm->setViews(cell->views<lib::SymbolView>());
}
