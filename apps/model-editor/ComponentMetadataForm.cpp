/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "ComponentMetadataForm.h"
#include "ui_ComponentMetadataForm.h"

ComponentMetadataForm::ComponentMetadataForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ComponentMetadataForm)
{
    ui->setupUi(this);
    connect(ui->captionLineEdit, &QLineEdit::textChanged,
            this, [this](const QString &text) {
        m_identification.caption = text;
        emit identificationChanged();
    });
    connect(ui->descriptionLineEdit, &QLineEdit::textChanged,
            this, [this](const QString &text) {
        m_identification.description = text;
        emit identificationChanged();
    });
    connect(ui->statementLineEdit, &QLineEdit::textChanged,
            this, [this](const QString &text) {
        m_attribution.statement = text;
        emit attributionChanged();
    });
    connect(ui->licenseLineEdit, &QLineEdit::textChanged,
            this, [this](const QString &text) {
        m_attribution.license = text;
        emit attributionChanged();
    });
    connect(ui->documentationTextEdit, &QPlainTextEdit::textChanged,
            this, [this]() {
        m_documentation.text = ui->documentationTextEdit->document()->toPlainText();
        emit documentationChanged();
    });
}

ComponentMetadataForm::~ComponentMetadataForm()
{
    delete ui;
}

lib::Identification ComponentMetadataForm::identification() const
{
    return m_identification;
}

lib::Attribution ComponentMetadataForm::attribution() const
{
    return m_attribution;
}

lib::Documentation ComponentMetadataForm::documentation() const
{
    return m_documentation;
}

void ComponentMetadataForm::setIdentification(lib::Identification identification)
{
    if (m_identification == identification)
        return;

    m_identification = identification;
    ui->uuidLineEdit->setText(m_identification.uid);
    ui->captionLineEdit->setText(m_identification.caption);
    ui->descriptionLineEdit->setText(m_identification.description);
    emit identificationChanged();
}

void ComponentMetadataForm::setAttribution(lib::Attribution attribution)
{
    if (m_attribution == attribution)
        return;

    m_attribution = attribution;
    ui->statementLineEdit->setText(m_attribution.statement);
    ui->licenseLineEdit->setText(m_attribution.license);
    emit attributionChanged();
}

void ComponentMetadataForm::setDocumentation(lib::Documentation documentation)
{
    if (m_documentation == documentation)
        return;

    m_documentation = documentation;
    ui->documentationTextEdit->document()->setPlainText(m_documentation.text);
    emit documentationChanged();
}
