/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "ComponentInterfaceForm.h"
#include "ParameterTableWidget.h"
#include "PortTableWidget.h"

#include "qlCore/LibraryObjects.h"

#include <QtWidgets/QGroupBox>
#include <QtWidgets/QVBoxLayout>

ComponentInterfaceForm::ComponentInterfaceForm(QWidget *parent)
    : QWidget(parent)
    , m_portWidget(new PortTableWidget)
    , m_parameterWidget(new ParameterTableWidget)
{
    setLayout(new QVBoxLayout());

    auto portGroupBox = new QGroupBox("Ports");
    portGroupBox->setLayout(new QVBoxLayout());
    portGroupBox->layout()->addWidget(m_portWidget);

    auto parameterGroupBox = new QGroupBox("Parameters");
    parameterGroupBox->setLayout(new QVBoxLayout());
    parameterGroupBox->layout()->addWidget(m_parameterWidget);

    layout()->addWidget(portGroupBox);
    layout()->addWidget(parameterGroupBox);
}

ComponentInterfaceForm::~ComponentInterfaceForm()
{

}

void ComponentInterfaceForm::setInterface(const QList<lib::Port *> &ports, const QList<lib::Parameter *> &parameters)
{
    m_ports = ports;
    m_parameters = parameters;
    m_portWidget->setPorts(ports);
    m_parameterWidget->setParameters(parameters);
}
