/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "PortTableModel.h"

#include <QtGui/QValidator>

#include <QtWidgets/QCompleter>
#include <QtWidgets/QComboBox>

PortTableModel::PortTableModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

PortTableModel::~PortTableModel()
{

}

void PortTableModel::setPortList(const QList<lib::Port *> &list)
{
    beginResetModel();
    m_ports = list;
    endResetModel();
}

QList<lib::Port *> PortTableModel::portList() const
{
    return m_ports;
}

void PortTableModel::setNameValidator(QValidator *validator)
{
    m_nameValidator = validator;
}

QValidator *PortTableModel::nameValidator() const
{
    return m_nameValidator;
}

void PortTableModel::setInvalidItemBackgroundBrush(const QBrush &brush)
{
    m_invalidItemBackgroundBrush = brush;
}

QBrush PortTableModel::invalidItemBackgroundBrush() const
{
    return m_invalidItemBackgroundBrush;
}

QString PortTableModel::toString(Qs::PortDirection direction)
{
    switch (direction) {
        case Qs::InputPort:
            return "Input";
        case Qs::OutputPort:
            return "Output";
        case Qs::InputOutputPort:
            return "Bidirectional";
        default:
            Q_UNREACHABLE();
            return QString();
    }
}

bool PortTableModel::isValidName(const QString &name) const
{
    if (m_nameValidator == nullptr)
        return true;
    QString data = name;
    int pos = 0;
    return m_nameValidator->validate(data, pos) == QValidator::Acceptable;
}

QVariant PortTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation != Qt::Horizontal)
        return QVariant();

    Q_ASSERT(section >= 0 && section < SectionCount);

    switch (section) {
        case NameSection:
            return "Name";
        case DirectionSection:
            return "Direction";
        case DocumentationSection:
            return "Documentation";
        default:
            Q_UNREACHABLE();
            return QVariant();
    }
}

int PortTableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return m_ports.count();
}

int PortTableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return SectionCount;
}

QVariant PortTableModel::data(const QModelIndex &modelIndex, int role) const
{
    if (!modelIndex.isValid())
        return QVariant();

    const int section = modelIndex.column();
    const int row = modelIndex.row();
    Q_ASSERT(row >= 0 && row < m_ports.count());
    Q_ASSERT(section >= 0 && section < SectionCount);

    const auto port = m_ports.value(row);
    if (role == Qt::DisplayRole) {
        switch (section) {
            case NameSection:
                return port->name();
            case DirectionSection:
                return toString(port->direction());
            case DocumentationSection:
                return port->documentation();
            default:
                Q_UNREACHABLE();
                return QVariant();
        }
    }
    if (role == Qt::EditRole) {
        switch (section) {
            case NameSection:
                return port->name();
            case DirectionSection:
                return port->direction();
            case DocumentationSection:
                return port->documentation();
            default:
                Q_UNREACHABLE();
                return QVariant();
        }
    }
    if (role == Qt::BackgroundRole) {
        bool isValid = false;
        switch (section) {
            case NameSection:
                isValid = isValidName(port->name());
                break;
            default:
                return QVariant();
        }
        return isValid ? QVariant() : m_invalidItemBackgroundBrush;
    }
    return QVariant();
}

bool PortTableModel::setData(const QModelIndex &modelIndex, const QVariant &value, int role)
{
    if (data(modelIndex, role) == value)
        return false;

    const int section = modelIndex.column();
    const int row = modelIndex.row();
    Q_ASSERT(row >= 0 && row < m_ports.count());
    Q_ASSERT(section >= 0 && section < SectionCount);

    const auto port = m_ports.value(row);

    if (role == Qt::EditRole) {
        switch (section) {
            case NameSection:
                port->setName(value.toString());
                break;
            case DirectionSection:
                port->setDirection(value.value<Qs::PortDirection>());
                break;
            case DocumentationSection:
                port->setDocumentation(value.toString());
                break;
            default:
                Q_UNREACHABLE();
                return false;
        }
    }

    // Invalidate the whole line
    emit dataChanged(index(modelIndex.row(), 0, modelIndex.parent()),
                     index(modelIndex.row(), SectionCount, modelIndex.parent()),
                     QVector<int>() << Qt::DisplayRole
                     << Qt::EditRole << Qt::BackgroundRole
                     << Qt::CheckStateRole);
    return true;
}

Qt::ItemFlags PortTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    Qt::ItemFlags flags = Qt::ItemIsEnabled | Qt::ItemIsEditable;
    return flags;
}

bool PortTableModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endInsertRows();
    return false;
}

bool PortTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endRemoveRows();
    return false;
}
