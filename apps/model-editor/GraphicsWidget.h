/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "qlCore/QlCore.h"
#include "qlCore/Object.h"

#include <QtCore/QList>
#include <QtWidgets/QWidget>

class GraphicsGrid;
class GraphicsScene;
class GraphicsSheet;
class GraphicsSnapper;
class GraphicsView;
class GraphicsZoomHandler;

class GraphicsSelectTool;
class GraphicsMoveTool;
class GraphicsTool;
class PlaceEllipseTool;
class PlaceLabelTool;
class PlaceLineTool;
class PlacePortTool;
class PlaceRectangleTool;

class QGraphicsItem;
class GraphicsEllipseItem;
class GraphicsLabelItem;
class GraphicsLineItem;
class GraphicsPortItem;
class GraphicsRectangleItem;

class DocumentNavigationWidget;
class GraphicsToolPropertyWidget;

namespace sym {
    class Port;
}

namespace draw {
    class DrawingObject;
    class Ellipse;
    class Line;
    class Label;
    class Rectangle;
}

class QUndoStack;
class SymbolDocument;
class UndoCommand;

class QAction;
class QActionGroup;
class QDockWidget;
class QToolBar;
class QUndoView;
class QPen;
class QBrush;

class GraphicsWidget : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(bool isModified READ isModified NOTIFY isModifiedChanged)

public:
    explicit GraphicsWidget(QWidget *parent = 0);
    ~GraphicsWidget();

    void saveDocument(const QString &path);
    void openDocument(const QString &path);
    void closeDocument();
    QString documentName() const;
    // FIXME
    void setDocument(SymbolDocument *document);
    // FIXME
    void setGraphicsObjects(const QList<draw::DrawingObject *> &objects);

    QList<QAction *> editActions() const;
    QList<QAction *> placementActions() const;
    QList<QAction *> toolsActions() const;
    QList<QDockWidget *> dockWidgets() const;
    QList<QToolBar *> toolBars() const;

    bool canUndo() const;
    bool canRedo() const;
    bool canCut() const;
    bool canCopy() const;
    bool canPaste() const;
    bool canMove() const;
    bool canDelete() const;
    bool canSelectAll() const;
    bool canClearSelection() const;
    bool canInvertSelection() const;

    bool isModified() const;

signals:
    void canUndoChanged(bool canUndo);
    void canRedoChanged(bool canRedo);
    void canCutChanged(bool canCut);
    void canCopyChanged(bool canCopy);
    void canPasteChanged(bool canPaste);
    void canMoveChanged(bool canMove);
    void canDeleteChanged(bool canDelete);
    void canSelectAllChanged(bool canSelectAll);
    void canClearSelectionChanged(bool canClearSelection);
    void canInvertSelectionChanged(bool canInvertSelection);

    void isModifiedChanged(bool isModified);

public slots:
    void undo();
    void redo();
    void cut(QClipboard *clipboard);
    void copy(QClipboard *clipboard);
    void paste(const QClipboard *clipboard);
    void moveIt();
    void deleteIt();
    void selectAll();
    void clearSelection();
    void invertSelection();

private slots:
    void setCurrentTool(GraphicsTool *tool);
    void addSceneItem(ObjectUid documentObjectUid);
    void removeSceneItem(ObjectUid documentObjectUid);
    void updateSceneItem(ObjectUid documentObjectUid);
    void updateSceneSelectionFromDocumentSelection(const QSet<ObjectUid> &current, const QSet<ObjectUid> &previous);
    void updateDocumentSelectionFromSceneSelection();

private:
    void loadTools();
    void createActions();
    void createToolBars();
    void createDockWidgets();
    void setupUndoStack();
    void setupClipBoard();
    void addSceneItem(const draw::DrawingObject *object);
    QList<QGraphicsItem *> addSceneItems(const QList<draw::DrawingObject *> &objects); // FIXME
    void updateGraphicsItem(QGraphicsItem *item, const draw::DrawingObject *object);
    void updateEllipseItem(GraphicsEllipseItem *item, const draw::Ellipse *object);
    void updateLabelItem(GraphicsLabelItem *item, const draw::Label *object);
    void updateLineItem(GraphicsLineItem *item, const draw::Line *object);
    void updateRectangleItem(GraphicsRectangleItem *item, const draw::Rectangle *object);
    void updatePortItem(GraphicsPortItem *item, const sym::Port *object);
    void beginEditDocument();
    void endEditDocument();
    void executeCommand(UndoCommand *command);
    static QPen createPen(Qs::StrokeWidth width, Qs::StrokeStyle style, const QColor &color);
    static QBrush createBrush(Qs::FillStyle style, const QColor &color);

private:
    SymbolDocument *m_document = nullptr;
    QUndoStack *m_undoStack = nullptr;
    GraphicsScene *m_scene = nullptr;
    GraphicsView *m_view = nullptr;
    GraphicsSheet *m_sheet = nullptr;
    GraphicsGrid *m_grid = nullptr;
    GraphicsSnapper *m_snapper = nullptr;
    GraphicsZoomHandler *m_zoomHandler = nullptr;
    GraphicsSelectTool *m_selectTool = nullptr;
    GraphicsMoveTool *m_moveTool = nullptr;
    PlaceEllipseTool *m_placeEllipseTool = nullptr;
    PlaceLabelTool *m_placeLabelTool = nullptr;
    PlaceLineTool *m_placeLineTool = nullptr;
    PlacePortTool *m_placePortTool = nullptr;
    PlaceRectangleTool *m_placeRectangleTool = nullptr;
    GraphicsTool *m_currentTool = nullptr;
    QList<GraphicsTool *> m_tools;
    QActionGroup *m_toolActionGroup = nullptr;
    QList<QDockWidget *> m_dockWidgets;
    QList<QToolBar *> m_toolBars;
    bool m_updatingSceneSelection = false;
    DocumentNavigationWidget *m_documentOulineWidget = nullptr;
    //QDockWidget *m_documentOulineDockWidget = nullptr;
    GraphicsToolPropertyWidget *m_toolPropertyWidget = nullptr;
    QDockWidget *m_toolPropertyDockWidget = nullptr;
    QUndoView *m_commandHistoryWidget = nullptr;
    QDockWidget *m_commandHistoryDockWidget = nullptr;
    static const QString g_symbolFragmentMimeType;
    bool m_pastingObjects = false;
    QSet<ObjectUid> m_pastedObjectUids;
};
