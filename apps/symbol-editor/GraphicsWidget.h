/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "qlCore/QlCore.h"
#include "qlCore/Object.h"

#include <QtCore/QList>
#include <QtWidgets/QWidget>

class GraphicsItemFactory;
class GraphicsGrid;
class GraphicsScene;
class GraphicsSheet;
class GraphicsSnapper;
class GraphicsView;
class GraphicsZoomHandler;

class GraphicsCloneTool;
class GraphicsMoveTool;
class GraphicsSelectTool;
class GraphicsTool;
class PlaceItemsTool;
class PlaceEllipseTool;
class PlaceLabelTool;
class PlaceLineTool;
class PlacePortTool;
class PlaceRectangleTool;

class DocumentNavigationWidget;
class GraphicsSelectToolOptionWidget;

class SymbolDocument;
class Object;
class UndoCommand;

class QAction;
class QActionGroup;
class QDockWidget;
class QToolBar;
class QUndoStack;
class QUndoView;

class GraphicsWidget : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(bool isModified READ isModified NOTIFY isModifiedChanged)

public:
    explicit GraphicsWidget(QWidget *parent = 0);
    ~GraphicsWidget();

    void saveDocument(const QString &path);
    void openDocument(const QString &path);
    void closeDocument();
    QString documentName() const;
    // FIXME
    void setDocument(SymbolDocument *document);
    SymbolDocument *document() const;
    GraphicsView *graphicsView() const;
    GraphicsScene *graphicsScene() const;
    GraphicsItemFactory *graphicsItemFactory() const;

    QList<QAction *> editActions() const;
    QList<QAction *> placementActions() const;
    QList<QAction *> toolsActions() const;
    QList<QAction *> viewActions() const;
    QList<QDockWidget *> dockWidgets() const;
    QList<QToolBar *> toolBars() const;

    bool canUndo() const;
    bool canRedo() const;
    bool canCut() const;
    bool canCopy() const;
    bool canPaste() const;
    bool canMove() const;
    bool canDelete() const;
    bool canSelectAll() const;
    bool canClearSelection() const;
    bool canInvertSelection() const;

    bool isModified() const;

signals:
    void canUndoChanged(bool canUndo);
    void canRedoChanged(bool canRedo);
    void canCutChanged(bool canCut);
    void canCopyChanged(bool canCopy);
    void canPasteChanged(bool canPaste);
    void canMoveChanged(bool canMove);
    void canDeleteChanged(bool canDelete);
    void canSelectAllChanged(bool canSelectAll);
    void canClearSelectionChanged(bool canClearSelection);
    void canInvertSelectionChanged(bool canInvertSelection);

    void isModifiedChanged(bool isModified);

public slots:
    void undo();
    void redo();
    void cut(QClipboard *clipboard);
    void copy(QClipboard *clipboard);
    void paste(const QClipboard *clipboard);
    void moveIt();
    void deleteIt();
    void selectAll();
    void clearSelection();
    void invertSelection();

    void zoomToDrawing();
    void zoomToSelection();
    void zoomToFit();
    void zoomToBox(const QRectF &rect, qreal marginFactor = 0.05);

private slots:
    void setCurrentTool(GraphicsTool *tool);

    void addSceneItem(ObjectUid uid);
    void removeSceneItem(ObjectUid uid);
    void updateSceneItem(ObjectUid uid);
    void updateItemOrder(ObjectUid uid, int newIndex);

    void updateSceneSelectionFromDocumentSelection(const QSet<ObjectUid> &current, const QSet<ObjectUid> &previous);
    void updateDocumentSelectionFromSceneSelection();

private:
    void loadTools();
    void createActions();
    void createToolBars();
    void createDockWidgets();
    void setupGraphicsView();
    void setupUndoStack();
    void setupClipBoard();
    void beginEditDocument();
    void endEditDocument();
    void executeCommand(UndoCommand *command);
    void addSceneItem(const Object *object);

private:
    SymbolDocument *m_document = nullptr;
    QUndoStack *m_undoStack = nullptr;
    GraphicsScene *m_scene = nullptr;
    GraphicsView *m_view = nullptr;
    GraphicsSheet *m_sheet = nullptr;
    GraphicsGrid *m_grid = nullptr;
    GraphicsSnapper *m_snapper = nullptr;
    GraphicsZoomHandler *m_zoomHandler = nullptr;
    GraphicsItemFactory *m_itemFactory = nullptr;
    GraphicsSelectTool *m_selectTool = nullptr;
    GraphicsMoveTool *m_moveTool = nullptr;
    GraphicsCloneTool *m_cloneTool = nullptr;
    PlaceItemsTool *m_placeItemsTool = nullptr;
    PlaceEllipseTool *m_placeEllipseTool = nullptr;
    PlaceLabelTool *m_placeLabelTool = nullptr;
    PlaceLineTool *m_placeLineTool = nullptr;
    PlacePortTool *m_placePortTool = nullptr;
    PlaceRectangleTool *m_placeRectangleTool = nullptr;
    GraphicsTool *m_currentTool = nullptr;
    QList<GraphicsTool *> m_tools;
    QActionGroup *m_toolActionGroup = nullptr;
    QActionGroup *m_editActionGroup = nullptr;
    QList<QAction *> m_viewActions;
    QList<QDockWidget *> m_dockWidgets;
    QList<QToolBar *> m_toolBars;
    bool m_updatingSceneSelection = false;
    QDockWidget *m_toolOptionDockWidget = nullptr;
    QUndoView *m_commandHistoryWidget = nullptr;
    QDockWidget *m_commandHistoryDockWidget = nullptr;
    static const QString g_symbolFragmentMimeType;
    bool m_pastingObjects = false;
    QSet<ObjectUid> m_pastedObjectUids;
};
