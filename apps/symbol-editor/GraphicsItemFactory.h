/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "qlCore/QlCore.h"

#include <QtCore/QObject>

namespace sym {
    class Port;
}

namespace draw {
    class DrawingObject;
    class Arrow;
    class Ellipse;
    class Line;
    class Label;
    class Object;
    class Rectangle;
}

class Object;
class UndoCommand;

class GraphicsEllipseItem;
class GraphicsLabelItem;
class GraphicsLineItem;
class GraphicsArrowItem;
class GraphicsPortItem;
class GraphicsRectangleItem;


class QBrush;
class QGraphicsItem;
class QPen;

class GraphicsItemFactory: public QObject
{
    Q_OBJECT

public:
    GraphicsItemFactory(QObject *parent = nullptr);
    ~GraphicsItemFactory();

    QList<QGraphicsItem *> createItems(const QList<Object *> &objects);
    QGraphicsItem *createItem(const Object *object);
    void updateItem(QGraphicsItem *item, const Object *object);
    QGraphicsItem *cloneItem(QGraphicsItem *item);
    static QPen createPen(Qs::StrokeWidth width, Qs::StrokeStyle style, const QColor &color);
    static QBrush createBrush(Qs::FillStyle style, const QColor &color);

    UndoCommand *createCreateObjectCommand(const QGraphicsItem *item, UndoCommand *parent = nullptr);
    UndoCommand *createUpdateObjectCommand(const QGraphicsItem *item, const Object *object, UndoCommand *parent = nullptr);

private:
    // Object -> Item: Update item
    void updateGraphicsItem(QGraphicsItem *item, const draw::DrawingObject *object);
    void updateEllipseItem(GraphicsEllipseItem *item, const draw::Ellipse *object);
    void updateLabelItem(GraphicsLabelItem *item, const draw::Label *object);
    void updateLineItem(GraphicsLineItem *item, const draw::Line *object);
    void updateArrowItem(GraphicsArrowItem *item, const draw::Arrow *object);
    void updateRectangleItem(GraphicsRectangleItem *item, const draw::Rectangle *object);
    void updatePortItem(GraphicsPortItem *item, const sym::Port *object);
    // Item -> Object: Create update command
    UndoCommand *createUpdateEllipseCommand(const GraphicsEllipseItem *item, const draw::Ellipse *object, UndoCommand *parent = nullptr);
    UndoCommand *createUpdateLabelCommand(const GraphicsLabelItem *item, const draw::Label *object, UndoCommand *parent = nullptr);
    UndoCommand *createUpdateLineCommand(const GraphicsLineItem *item, const draw::Line *object, UndoCommand *parent = nullptr);
    UndoCommand *createUpdateArrowCommand(const GraphicsArrowItem *item, const draw::Arrow *object, UndoCommand *parent = nullptr);
    UndoCommand *createUpdateRectangleCommand(const GraphicsRectangleItem *item, const draw::Rectangle *object, UndoCommand *parent = nullptr);
    UndoCommand *createUpdatePortCommand(const GraphicsPortItem *item, const sym::Port *object, UndoCommand *parent = nullptr);

    QMap<QString, QVariant> objectProperties(const QGraphicsItem *item);
    QMap<QString, QVariant> strokeProperties(const QPen &pen);
    QMap<QString, QVariant> fillProperties(const QBrush &brush);
};
