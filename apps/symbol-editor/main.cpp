/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/


#include "MainWindow.h"

#include "qlCore/Object.h"

#include <QtCore/QDebug>
#include <QtWidgets/QApplication>

#include <QFile>
int main(int argc, char *argv[])
{
    QApplication application(argc, argv);
    application.setApplicationName("QucsLab Symbol editor");
    application.setApplicationVersion("0.3");
    application.setOrganizationName("QucsLab");
    application.setOrganizationDomain("qucs.org");

    qRegisterMetaTypeStreamOperators<ObjectUid>("ObjectUid");

    MainWindow mainWindow;

    if (mainWindow.palette().color(QPalette::Base).lightness() < 128)
        QIcon::setThemeName("QucsLabDark");
    else
        QIcon::setThemeName("QucsLabLight");

    mainWindow.showMaximized();
    return application.exec();
}
