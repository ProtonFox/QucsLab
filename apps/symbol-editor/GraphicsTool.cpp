/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsItemFactory.h"
#include "GraphicsSelectToolOptionWidget.h"
#include "GraphicsTool.h"
#include "GraphicsWidget.h"

#include "qlGui/GraphicsItem.h"
#include "qlGui/GraphicsScene.h"
#include "qlGui/GraphicsView.h"
#include "qlGui/ObjectPropertyBrowser.h"
#include "qlCore/SymbolCommand.h"

#include <QtCore/QDebug>
#include <QtCore/QEvent>
#include <QtGui/QIcon>
#include <QtGui/QKeySequence>
#include <QtWidgets/QActionGroup>
#include <QtWidgets/QGraphicsItemGroup>
#include <QtWidgets/QGraphicsSceneMouseEvent>
#include <QtWidgets/QGraphicsOpacityEffect>
#include <QtWidgets/QGraphicsPathItem>
#include <QtWidgets/QRubberBand>

/******************************************************************************
 * GraphicsTool
 *****************************************************************************/

GraphicsTool::GraphicsTool(QObject *parent)
    : QObject(parent)
    , m_command(nullptr)
{

}

GraphicsTool::~GraphicsTool()
{
    delete m_command;
}

bool GraphicsTool::hasUndoCommand() const
{
    return m_command != nullptr;
}

UndoCommand *GraphicsTool::takeUndoCommand()
{
    Q_ASSERT(m_command != nullptr);
    UndoCommand *result = m_command;
    m_command = nullptr;
    return result;
}

QWidget *GraphicsTool::optionWidget() const
{
    return m_optionWidget;
}

void GraphicsTool::setUndoCommand(UndoCommand *command)
{
    if (m_command != nullptr)
        qWarning() << "GraphicsTool::setUndoCommand():" << "pushing a new command while the previous has not been consumed";
    delete m_command;
    m_command = command;
    emit undoCommandAvailable();
}

void GraphicsTool::setOptionWidget(QWidget *widget)
{
    delete m_optionWidget;
    m_optionWidget = widget;
}

/******************************************************************************
 * GraphicsSelectTool
 *****************************************************************************/

// Rubber band selection
// Click selection (Shift add to selection, ctrl remove from selection)
// Drag selection or item under cursor
// FIXME: Need to enable multiple selection at document level
// TBD: Don't handle move here. To move selection or item under cursor, use 'm' (move tool)

GraphicsSelectTool::GraphicsSelectTool(QObject *parent)
    : GraphicsTool(parent)
    , m_rubberBand(new QRubberBand(QRubberBand::Rectangle))
    , m_quickActionGroup(new QActionGroup(this))
    , m_toolWidget(new GraphicsSelectToolOptionWidget())
{
    setupRubberBand();
    setupQuickActions();
    setupOptionWidget();
}

GraphicsSelectTool::~GraphicsSelectTool()
{

}

QList<QAction *> GraphicsSelectTool::quickActions() const
{
    return m_quickActionGroup->actions();
}

void GraphicsSelectTool::setupQuickActions()
{
    auto action = new QAction(QIcon::fromTheme("object-order-front"), "Bring to front");
    m_quickActionGroup->addAction(action);
    connect(action, &QAction::triggered,
            this, &GraphicsSelectTool::bringSelectionToFront);

    action = new QAction(QIcon::fromTheme("object-order-raise"), "Raise");
    m_quickActionGroup->addAction(action);
    connect(action, &QAction::triggered,
            this, &GraphicsSelectTool::raiseSelection);

    action = new QAction(QIcon::fromTheme("object-order-lower"), "Lower");
    m_quickActionGroup->addAction(action);
    connect(action, &QAction::triggered,
            this, &GraphicsSelectTool::lowerSelection);

    action = new QAction(QIcon::fromTheme("object-order-back"), "Send to back");
    m_quickActionGroup->addAction(action);
    connect(action, &QAction::triggered,
            this, &GraphicsSelectTool::sendSelectionToBack);

    m_quickActionGroup->setEnabled(false);
}

void GraphicsSelectTool::setupOptionWidget()
{
    m_toolWidget->setQuickActions(m_quickActionGroup->actions());
    setOptionWidget(m_toolWidget);
    connect(m_toolWidget, &GraphicsSelectToolOptionWidget::commandAvailable,
            this, [this]() {
        setUndoCommand(m_toolWidget->takeCommand());
    });
}

void GraphicsSelectTool::setupRubberBand()
{
    m_rubberBand->setWindowOpacity(0.3);
    // FIXME: KDE/Plasma want's to animate the resizing which gives bery poor UX (sluggish)
}

bool GraphicsSelectTool::canSelect(const QPointF &pos)
{
    auto item = m_scene->itemAt(pos, QTransform());
    // A graphics item that is a document object surrogate and which is not locked
    return item != nullptr &&
            item->data(GraphicsScene::DocumentIdProperty).canConvert<ObjectUid>() &&
            item->isEnabled();
}

void GraphicsSelectTool::maybeAddToOrClearSelection(const QPointF &pos)
{
    if (!canSelect(pos))
        m_scene->clearSelection();
    else
        m_scene->itemAt(pos, QTransform())->setSelected(true);
}

void GraphicsSelectTool::maybeRemoveFromOrClearSelection(const QPointF &pos)
{
    if (!canSelect(pos))
        m_scene->clearSelection();
    else
        m_scene->itemAt(pos, QTransform())->setSelected(false);
}

void GraphicsSelectTool::maybeSetOrClearSelection(const QPointF &pos)
{
    m_scene->clearSelection();
    if (canSelect(pos))
        m_scene->itemAt(pos, QTransform())->setSelected(true);
}

bool GraphicsSelectTool::canStartDrag(const QPointF &pos)
{
    return m_buttonState == ButtonDown &&
            QLineF(m_view->mapFromScene(m_buttonDownPos),
                   m_view->mapFromScene(pos)).length() > m_dragThreshold;
}

void GraphicsSelectTool::startRubberBandDrag(const QPointF &pos)
{
    setState(RubberBandSelect);
    m_rubberBand->setGeometry(QRect(m_view->mapToGlobal(m_view->mapFromScene(m_buttonDownPos)),
                                     m_view->mapToGlobal(m_view->mapFromScene(pos))));
    m_rubberBand->show();
}

void GraphicsSelectTool::updateRubberBand(const QPointF &pos)
{
    QPainterPath path;
    path.addRect(QRectF(m_buttonDownPos, pos).normalized());
    m_scene->setSelectionArea(path);
    m_rubberBand->setGeometry(QRect(m_view->mapToGlobal(m_view->mapFromScene(m_buttonDownPos)),
                                     m_view->mapToGlobal(m_view->mapFromScene(pos))).normalized());
}

void GraphicsSelectTool::endRubberBand(const QPointF &pos)
{
    Q_UNUSED(pos);
    setState(ClickSelect);
    m_rubberBand->hide();
}

// TBD: Do we want cursor-based move operation?
// TBD: Or we only support move through the 'm' command?
// If we manage moving handles, then we can surely move items too.
void GraphicsSelectTool::startSelectionDrag(const QPointF &pos)
{
    Q_UNUSED(pos);
    setState(DragSelect);
}

void GraphicsSelectTool::updateDragSelect(const QPointF &pos)
{
    Q_UNUSED(pos);
}

void GraphicsSelectTool::endDragSelect(const QPointF &pos)
{
    Q_UNUSED(pos);
    setState(ClickSelect);
}

bool GraphicsSelectTool::canDragHandle(const QPointF &pos)
{
    auto item = m_scene->itemAt(pos, m_view->transform());
    // A graphics item that is a handle
    return item != nullptr && item->type() == GraphicsHandle::Type;
}

void GraphicsSelectTool::startHandleDrag(const QPointF &pos)
{
    auto item = m_scene->itemAt(pos, m_view->transform());
    Q_ASSERT(item != nullptr);
    Q_ASSERT(item->type() == GraphicsHandle::Type);
    m_handle = qgraphicsitem_cast<GraphicsHandle*>(item);
    setState(DragHandle);
    m_originalHandlePos = m_handle->pos();
}

void GraphicsSelectTool::updateHandle(const QPointF &pos)
{
    m_handle->setPos(pos);
}

void GraphicsSelectTool::endDragHandle(const QPointF &pos)
{
    Q_UNUSED(pos);
    const auto item = m_handle->target();
    Q_ASSERT(item != nullptr);
    const auto uid = item->data(GraphicsScene::DocumentIdProperty).value<ObjectUid>();
    Q_ASSERT(!uid.isNull());
    const auto object = m_document->object(uid);
    Q_ASSERT(object != nullptr);
    auto command = m_factory->createUpdateObjectCommand(item, object);
    setUndoCommand(command);
    m_originalHandlePos = m_handle->pos();
    setState(ClickSelect);
}

void GraphicsSelectTool::setState(GraphicsSelectTool::State state)
{
    m_state = state;
}

void GraphicsSelectTool::sendSelectionToBack()
{
    if (m_scene->selectedItems().isEmpty())
        return;
    auto item = m_scene->selectedItems().first();
    auto uid = item->data(GraphicsScene::DocumentIdProperty).value<ObjectUid>();
    auto command = new MoveObjectCommand();
    command->setUid(uid);
    command->setOperation(MoveObjectCommand::MoveToBottom);
    setUndoCommand(command);
}

void GraphicsSelectTool::lowerSelection()
{
    if (m_scene->selectedItems().isEmpty())
        return;
    auto item = m_scene->selectedItems().first();
    auto uid = item->data(GraphicsScene::DocumentIdProperty).value<ObjectUid>();
    auto command = new MoveObjectCommand();
    command->setUid(uid);
    command->setOperation(MoveObjectCommand::MoveDown);
    setUndoCommand(command);
}

void GraphicsSelectTool::raiseSelection()
{
    if (m_scene->selectedItems().isEmpty())
        return;
    auto item = m_scene->selectedItems().first();
    auto uid = item->data(GraphicsScene::DocumentIdProperty).value<ObjectUid>();
    auto command = new MoveObjectCommand();
    command->setUid(uid);
    command->setOperation(MoveObjectCommand::MoveUp);
    setUndoCommand(command);
}

void GraphicsSelectTool::bringSelectionToFront()
{
    if (m_scene->selectedItems().isEmpty())
        return;
    auto item = m_scene->selectedItems().first();
    auto uid = item->data(GraphicsScene::DocumentIdProperty).value<ObjectUid>();
    auto command = new MoveObjectCommand();
    command->setUid(uid);
    command->setOperation(MoveObjectCommand::MoveToTop);
    setUndoCommand(command);
}

QString GraphicsSelectTool::caption() const
{
    return QStringLiteral("Selection tool");
}

QString GraphicsSelectTool::description() const
{
    return QStringLiteral("Select and manipulate objects");
}

QKeySequence GraphicsSelectTool::shortcut() const
{
    return QKeySequence(Qt::Key_Escape);
}

QIcon GraphicsSelectTool::icon() const
{
    return QIcon::fromTheme("edit-select");
}

bool GraphicsSelectTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event)

    if (sourceEvent->button() != Qt::LeftButton)
        return false;

    return true;
}

bool GraphicsSelectTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event)

    if (sourceEvent->button() != Qt::LeftButton)
        return false;

    m_buttonState = ButtonDown;
    m_buttonDownPos = sourceEvent->scenePos();
    return true;
}

bool GraphicsSelectTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event)

    if (m_buttonState != ButtonDown)
        return true;

    const auto unfilteredPos = sourceEvent->scenePos();
    const auto filteredPos = event->scenePos();

    if (m_state == ClickSelect) {
        if (!canStartDrag(unfilteredPos))
            return true;

        if (canDragHandle(m_buttonDownPos))
            startHandleDrag(m_buttonDownPos);
        else if (canSelect(m_buttonDownPos))
            startSelectionDrag(m_buttonDownPos);
        else
            startRubberBandDrag(m_buttonDownPos);
    }

    if (m_state == DragSelect) {
        updateDragSelect(unfilteredPos);
        return true;
    }

    if (m_state == RubberBandSelect) {
        updateRubberBand(unfilteredPos);
        return true;
    }

    if (m_state == DragHandle) { // Need to snap
        updateHandle(filteredPos);
        return true;
    }

    Q_UNREACHABLE();
    return true;
}

bool GraphicsSelectTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(event);

    if (sourceEvent->button() != Qt::LeftButton)
        return false;

    m_buttonState = ButtonUp;

    const auto pos = sourceEvent->scenePos();
    switch (m_state) {
        case GraphicsSelectTool::ClickSelect:
            if (sourceEvent->modifiers() & Qt::ShiftModifier)
                maybeAddToOrClearSelection(pos);
            else if (sourceEvent->modifiers() & Qt::ControlModifier)
                maybeRemoveFromOrClearSelection(pos);
            else
                maybeSetOrClearSelection(pos);
            break;
        case GraphicsSelectTool::DragSelect:
            endDragSelect(pos);
            break;
        case GraphicsSelectTool::RubberBandSelect:
            endRubberBand(pos);
            break;
        case GraphicsSelectTool::DragHandle:
            endDragHandle(pos);
            break;
    }
    return true;
}

void GraphicsSelectTool::activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(pos);
    Q_UNUSED(sourcePos);

    m_view = editor->graphicsView();
    m_scene = editor->graphicsScene();
    m_factory = editor->graphicsItemFactory();
    m_document = editor->document();
    m_state = ClickSelect;
    m_view->graphicsScene()->installEventHandler(this);
    //m_rubberBand->setParent();
    m_toolWidget->setDocument(editor->document());

    connect(editor->document(), &SymbolDocument::selectionChanged,
            this, [this](const QSet<ObjectUid> &current, const QSet<ObjectUid> &/*previous*/) {
        m_quickActionGroup->setEnabled(!current.isEmpty());
    });
    m_quickActionGroup->setEnabled(!editor->document()->selection().isEmpty());
}

void GraphicsSelectTool::desactivate()
{
    //m_rubberBand->setParent(nullptr);
    m_view->graphicsScene()->uninstallEventHandler(this);

    switch (m_state) {
        case GraphicsSelectTool::ClickSelect:
        case GraphicsSelectTool::DragSelect:
        case GraphicsSelectTool::RubberBandSelect:
            break;
        case GraphicsSelectTool::DragHandle:
            m_handle->setPos(m_originalHandlePos);
            break;
    }
}

/******************************************************************************
 * GraphicsMoveTool
 *****************************************************************************/

GraphicsMoveTool::GraphicsMoveTool(QObject *parent)
    : GraphicsTool(parent)
{

}

GraphicsMoveTool::~GraphicsMoveTool()
{

}

void GraphicsMoveTool::activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos)
{
    m_view = editor->graphicsView();
    editor->graphicsScene()->installEventHandler(this);

    m_state = WaitForPick;
    if (checkForPick(sourcePos)) // FIXME: Only if mouse is inside view
        pick(pos);
}

void GraphicsMoveTool::desactivate()
{
    if (hasPicked())
        unpick();
    m_view->setCursor(Qt::ArrowCursor);
    m_view->graphicsScene()->uninstallEventHandler(this);
}

QString GraphicsMoveTool::caption() const
{
    return "Move tool";
}

QString GraphicsMoveTool::description() const
{
    return "Move objects around";
}

QKeySequence GraphicsMoveTool::shortcut() const
{
    return QKeySequence("m");
}

QIcon GraphicsMoveTool::icon() const
{
    return QIcon::fromTheme("transform-move");
}

bool GraphicsMoveTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool GraphicsMoveTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool GraphicsMoveTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);

    const auto pos = sourceEvent->scenePos();
    const auto snappedPos = event->scenePos();
    switch (m_state) {
        case WaitForPick:
            checkForPick(pos);
            break;
        case WaitForPlace:
            move(snappedPos);
            break;
    }
    return true;
}

bool GraphicsMoveTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);

    const auto pos = sourceEvent->scenePos();
    const auto snappedPos = event->scenePos();
    switch (m_state) {
        case WaitForPick:
            if (checkForPick(pos))
                pick(snappedPos);
            break;
        case WaitForPlace:
            place(snappedPos);
            // TBD: auto-repeat vs single shoot
            //checkForPick(pos);
            emit finished();
            break;
    }
    return true;
}

// FIXME: simplify m_items vs m_uids
// We could create a group on the fly, apply a graphical effect
// to the group and destroy the group at the end
bool GraphicsMoveTool::checkForPick(const QPointF &pos)
{
    QList<QGraphicsItem *> items; // FIXME: m_uids vs m_items
    if (!m_view->graphicsScene()->selectedItems().isEmpty())
        items = m_view->graphicsScene()->selectedItems();
    else
        items = m_view->graphicsScene()->items(pos);

    m_uids.clear();
    m_items.clear();
    for (int i=0; i<items.count(); i++) {
        auto const uid = items.at(i)->data(GraphicsScene::DocumentIdProperty).value<ObjectUid>();
        if (!uid.isNull()) {
            m_uids.append(uid);
            m_items.append(items.at(i));
        }
    }

    if (!m_uids.isEmpty())
        m_view->setCursor(Qt::CrossCursor);
    else
        m_view->setCursor(Qt::ArrowCursor);

    return !m_uids.isEmpty();
}

// FIXME: simplify m_originalItemPositions vs m_deltaPickPositions
// Need only one or the other
void GraphicsMoveTool::pick(const QPointF &pos)
{
    m_originalItemPositions.clear();
    m_deltaPickPositions.clear();
    for (const auto item: m_items) {
        m_originalItemPositions << item->pos();
        m_deltaPickPositions << pos - item->pos();
        item->setOpacity(0.5); // FIXME
    }
    m_state = WaitForPlace;
    m_view->setCursor(Qt::SizeAllCursor);
}

bool GraphicsMoveTool::hasPicked()
{
    return !m_uids.isEmpty();
}

void GraphicsMoveTool::unpick()
{
    for (int i=0; i<m_items.count(); i++) {
        m_items.at(i)->setPos(m_originalItemPositions.at(i));
        m_items.at(i)->setOpacity(1.0); // FIXME
    }
}

void GraphicsMoveTool::place(const QPointF &pos)
{
    auto command = new UndoCommand();
    command->setText(QString("Move %1 object%2").arg(m_items.count()).arg(m_items.count() == 1 ? "" : "s"));
    for (int i=0; i<m_items.count(); i++) {
        m_items.at(i)->setPos(pos - m_deltaPickPositions.at(i));
        m_items.at(i)->setOpacity(1.0); // FIXME
        auto subCommand = new UpdateObjectCommand(command);
        subCommand->setUid(m_uids.at(i));
        subCommand->setProperty("location", m_items.at(i)->pos());
    }
    setUndoCommand(command);
    m_state = WaitForPick;
    m_view->setCursor(Qt::ArrowCursor);
    m_items.clear();
    m_uids.clear();
}

void GraphicsMoveTool::move(const QPointF &pos)
{
    for (int i=0; i<m_items.count(); i++)
        m_items.at(i)->setPos(pos - m_deltaPickPositions.at(i));
}

/******************************************************************************
 * GraphicsCloneTool
 *****************************************************************************/

// FIXME: Mainly duplicated with Move tool, the only difference is the implementation
// of pick() and place()
// Might be worth making pick() and place() virtual? And having clone tool inherit
// from move tool

GraphicsCloneTool::GraphicsCloneTool(QObject *parent)
    :GraphicsTool(parent)
{

}

GraphicsCloneTool::~GraphicsCloneTool()
{

}

void GraphicsCloneTool::activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos)
{
    m_view = editor->graphicsView();
    m_factory = editor->graphicsItemFactory();
    editor->graphicsScene()->installEventHandler(this);

    m_state = WaitForPick;
    if (checkForPick(sourcePos)) // FIXME: Only if mouse is inside view
        pick(pos);
}

void GraphicsCloneTool::desactivate()
{
    if (hasPicked())
        unpick();
    m_view->setCursor(Qt::ArrowCursor);
    m_view->graphicsScene()->uninstallEventHandler(this);
}

QString GraphicsCloneTool::caption() const
{
    return "Clone tool";
}

QString GraphicsCloneTool::description() const
{
    return "Clone objects";
}

QKeySequence GraphicsCloneTool::shortcut() const
{
    return QKeySequence("c");
}

QIcon GraphicsCloneTool::icon() const
{
    return QIcon::fromTheme("edit-clone");
}

bool GraphicsCloneTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool GraphicsCloneTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool GraphicsCloneTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);

    const auto pos = sourceEvent->scenePos();
    const auto snappedPos = event->scenePos();
    switch (m_state) {
        case WaitForPick:
            checkForPick(pos);
            break;
        case WaitForPlace:
            move(snappedPos);
            break;
    }
    return true;
}

bool GraphicsCloneTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);

    const auto pos = sourceEvent->scenePos();
    const auto snappedPos = event->scenePos();
    switch (m_state) {
        case WaitForPick:
            if (checkForPick(pos))
                pick(snappedPos);
            break;
        case WaitForPlace:
            place(snappedPos);
            // TBD: auto-repeat vs single shoot
            //checkForPick(pos);
            emit finished();
            break;
    }
    return true;
}

// FIXME: simplify m_items vs m_uids
// We could create a group on the fly, apply a graphical effect
// to the group and destroy the group at the end
bool GraphicsCloneTool::checkForPick(const QPointF &pos)
{
    QList<QGraphicsItem *> items; // FIXME: m_uids vs m_items
    if (!m_view->graphicsScene()->selectedItems().isEmpty())
        items = m_view->graphicsScene()->selectedItems();
    else
        items = m_view->graphicsScene()->items(pos);

    m_uids.clear();
    m_items.clear();
    for (int i=0; i<items.count(); i++) {
        auto const uid = items.at(i)->data(GraphicsScene::DocumentIdProperty).value<ObjectUid>();
        if (!uid.isNull()) {
            m_uids.append(uid);
            m_items.append(items.at(i));
        }
    }

    if (!m_uids.isEmpty())
        m_view->setCursor(Qt::CrossCursor);
    else
        m_view->setCursor(Qt::ArrowCursor);

    return !m_uids.isEmpty();
}

// FIXME: simplify m_originalItemPositions vs m_deltaPickPositions
// Need only one or the other
void GraphicsCloneTool::pick(const QPointF &pos)
{
    m_originalItemPositions.clear();
    m_deltaPickPositions.clear();
    for (const auto item: m_items) {
        m_originalItemPositions << item->pos();
        m_deltaPickPositions << pos - item->pos();
        m_clones.append(m_factory->cloneItem(item));
        m_clones.last()->setOpacity(0.5); // FIXME
        m_view->graphicsScene()->addItem(m_clones.last());
    }
    m_state = WaitForPlace;
    m_view->setCursor(Qt::SizeAllCursor);
}

bool GraphicsCloneTool::hasPicked()
{
    return !m_uids.isEmpty();
}

void GraphicsCloneTool::unpick()
{
    qDeleteAll(m_clones);
    m_clones.clear();
}

void GraphicsCloneTool::place(const QPointF &pos)
{
    auto command = new UndoCommand();
    command->setText(QString("Clone %1 object%2").arg(m_clones.count()).arg(m_clones.count() == 1 ? "" : "s"));
    for (int i=0; i<m_clones.count(); i++) {
        auto clone = m_clones.value(i);
        clone->setPos(pos - m_deltaPickPositions.at(i));
        clone->setOpacity(1.0); // FIXME
        m_factory->createCreateObjectCommand(clone, command);
    }
    setUndoCommand(command);
    m_state = WaitForPick;
    m_view->setCursor(Qt::ArrowCursor);
    m_items.clear();
    m_uids.clear();
    qDeleteAll(m_clones);
    m_clones.clear();
}

void GraphicsCloneTool::move(const QPointF &pos)
{
    for (int i=0; i<m_clones.count(); i++)
        m_clones.at(i)->setPos(pos - m_deltaPickPositions.at(i));
}

/******************************************************************************
 * PlaceItemTool
 *****************************************************************************/

PlaceItemsTool::PlaceItemsTool(QObject *parent)
    : GraphicsTool(parent)
    , m_itemGroup(new QGraphicsItemGroup())
{
    auto effect = new QGraphicsOpacityEffect();
    m_itemGroup->setGraphicsEffect(effect);
}

PlaceItemsTool::~PlaceItemsTool()
{

}

void PlaceItemsTool::setItems(QList<QGraphicsItem *> &items)
{
    qDeleteAll(m_itemGroup->childItems());
    m_itemGroup->setPos(QPointF());
    for (auto item: items)
        item->setParentItem(m_itemGroup.data());
}

void PlaceItemsTool::pushCommand()
{
    auto factory = m_editor->graphicsItemFactory();
    auto items = m_itemGroup->childItems();

    auto command = new UndoCommand(QString("Insert %1 object%2").arg(items.count()).arg(items.count() > 1 ? "s" : ""));
    for (auto item: items) {
        item->setPos(item->pos() + m_itemGroup->pos());
        factory->createCreateObjectCommand(item, command);
    }

    setUndoCommand(command);
}

void PlaceItemsTool::activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(sourcePos);

    m_editor = editor;
    m_editor->graphicsScene()->installEventHandler(this);
    m_itemGroup->setPos(pos);
    m_referencePos = pos;
    m_editor->graphicsScene()->addItem(m_itemGroup.data());
}

void PlaceItemsTool::desactivate()
{
    m_editor->graphicsScene()->removeItem(m_itemGroup.data());
    m_editor->graphicsScene()->uninstallEventHandler(this);
}

QString PlaceItemsTool::caption() const
{
    return "Place items";
}

QString PlaceItemsTool::description() const
{
    return "Paste/Clone items";
}

QKeySequence PlaceItemsTool::shortcut() const
{
    return QKeySequence();
}

QIcon PlaceItemsTool::icon() const
{
    return QIcon();
}

bool PlaceItemsTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceItemsTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceItemsTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    m_itemGroup->setPos(event->scenePos());
    return true;
}

bool PlaceItemsTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    pushCommand();
    emit finished();
    return true;
}

/******************************************************************************
 * PlaceEllipseTool
 *****************************************************************************/

PlaceEllipseTool::PlaceEllipseTool(QObject *parent)
    : GraphicsTool(parent)
    , m_view(nullptr)
    , m_item(new GraphicsEllipseItem())
{
    m_strokeStyle = Qs::SolidStroke;
    m_strokeColor = Qt::darkBlue;
    m_strokeWidth = Qs::MediumStroke;
    m_fillColor = Qt::lightGray;
    m_fillStyle = Qs::SolidFill;
    m_item->setStyle(Qs::FullEllipsoid);
    m_item->setPen(GraphicsItemFactory::createPen(m_strokeWidth,
                                                  m_strokeStyle,
                                                  m_strokeColor));
    m_item->setBrush(GraphicsItemFactory::createBrush(m_fillStyle,
                                                      m_fillColor));

    auto optionWidget = new ObjectPropertyBrowser();
    optionWidget->setStyle(ObjectPropertyBrowser::ListStyle);
    optionWidget->setObject(this, this);
    optionWidget->setBaseClassName("PlaceEllipseTool");
    setOptionWidget(optionWidget);
}

PlaceEllipseTool::~PlaceEllipseTool()
{
}


Qs::EllipseStyle PlaceEllipseTool::style() const
{
    return Qs::EllipseStyle(m_item->style());
}

Qs::StrokeStyle PlaceEllipseTool::strokeStyle() const
{
    return m_strokeStyle;
}

Qs::StrokeWidth PlaceEllipseTool::strokeWidth() const
{
    return m_strokeWidth;
}

QColor PlaceEllipseTool::strokeColor() const
{
    return m_strokeColor;
}

Qs::FillStyle PlaceEllipseTool::fillStyle() const
{
    return m_fillStyle;
}

QColor PlaceEllipseTool::fillColor() const
{
    return m_fillColor;
}

void PlaceEllipseTool::setStyle(Qs::EllipseStyle style)
{
    if (m_item->style() == style)
        return;

    m_item->setStyle(style);
    emit styleChanged(style);
    restart(); // Can messup the state machine
}

void PlaceEllipseTool::setStrokeStyle(Qs::StrokeStyle strokeStyle)
{
    if (m_strokeStyle == strokeStyle)
        return;

    m_strokeStyle = strokeStyle;
    m_item->setPen(GraphicsItemFactory::createPen(m_strokeWidth,
                                                  m_strokeStyle,
                                                  m_strokeColor));
    emit strokeStyleChanged(strokeStyle);
}

void PlaceEllipseTool::setStrokeWidth(Qs::StrokeWidth strokeWidth)
{
    if (m_strokeWidth == strokeWidth)
        return;

    m_strokeWidth = strokeWidth;
    m_item->setPen(GraphicsItemFactory::createPen(m_strokeWidth,
                                                  m_strokeStyle,
                                                  m_strokeColor));
    emit strokeWidthChanged(strokeWidth);
}

void PlaceEllipseTool::setStrokeColor(QColor strokeColor)
{
    if (m_strokeColor == strokeColor)
        return;

    m_strokeColor = strokeColor;
    m_item->setPen(GraphicsItemFactory::createPen(m_strokeWidth,
                                                  m_strokeStyle,
                                                  m_strokeColor));
    emit strokeColorChanged(strokeColor);
}

void PlaceEllipseTool::setFillStyle(Qs::FillStyle fillStyle)
{
    if (m_fillStyle == fillStyle)
        return;

    m_fillStyle = fillStyle;
    m_item->setBrush(GraphicsItemFactory::createBrush(m_fillStyle,
                                                      m_fillColor));
    emit fillStyleChanged(fillStyle);
}

void PlaceEllipseTool::setFillColor(QColor fillColor)
{
    if (m_fillColor == fillColor)
        return;

    m_fillColor = fillColor;
    m_item->setBrush(GraphicsItemFactory::createBrush(m_fillStyle,
                                                      m_fillColor));
    emit fillColorChanged(fillColor);
}

void PlaceEllipseTool::activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(pos);
    Q_UNUSED(sourcePos);

    m_view = editor->graphicsView();
    m_factory = editor->graphicsItemFactory();
    editor->graphicsScene()->installEventHandler(this);
    restart();
}

void PlaceEllipseTool::desactivate()
{
    if (m_item->scene() != nullptr)
        m_view->graphicsScene()->removeItem(m_item.data());
    m_view->graphicsScene()->uninstallEventHandler(this);
}

QString PlaceEllipseTool::caption() const
{
    return QStringLiteral("Ellipse tool");
}

QString PlaceEllipseTool::description() const
{
    return QStringLiteral("Place ellipses, circles and arcs");
}

QKeySequence PlaceEllipseTool::shortcut() const
{
    return QKeySequence("e");
}

QIcon PlaceEllipseTool::icon() const
{
    return QIcon::fromTheme("draw-ellipse");
}
bool PlaceEllipseTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceEllipseTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceEllipseTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    const auto pos = event->scenePos();
    switch (m_state) {
        case PlaceEllipseTool::WaitForCenter:
            m_item->setPos(pos);
            break;
        case PlaceEllipseTool::WaitForRadius: {
            const qreal length = QLineF(m_item->pos(), pos).length();
            m_item->setSize(QSizeF(2*length, 2*length));
            break;
        }
        case PlaceEllipseTool::WaitForStartAngle: {
            const QLineF l1(m_item->pos(), pos);
            const qreal angle = -l1.angleTo(QLineF(0, 0, 1, 0));
            m_item->setStartAngle(angle);
            break;
        }
        case PlaceEllipseTool::WaitForSpanAngle: {
            const QLineF l1(m_item->pos(), pos);
            QLineF l2(l1);
            l2.setAngle(m_item->startAngle());
            m_item->setSpanAngle(360 - l1.angleTo(l2));
            break;
        }
    }
    return true;
}

bool PlaceEllipseTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    switch (m_state) {
        case PlaceEllipseTool::WaitForCenter:
            m_item->setSize(QSizeF());
            m_view->scene()->addItem(m_item.data());
            m_state = PlaceEllipseTool::WaitForRadius;
            break;
        case PlaceEllipseTool::WaitForRadius:
            if (m_item->style() == Qs::FullEllipsoid) {
                pushCommand();
                restart();
                break;
            }
            if (qFuzzyCompare(m_item->spanAngle(), 360.0) ||
                    qFuzzyCompare(m_item->spanAngle(), 0.0))
                m_item->setSpanAngle(270.0);
            m_state = PlaceEllipseTool::WaitForStartAngle;
            break;
        case PlaceEllipseTool::WaitForStartAngle:
            m_state = PlaceEllipseTool::WaitForSpanAngle;
            break;
        case PlaceEllipseTool::WaitForSpanAngle: {
            pushCommand();
            restart();
            break;
        }
    }
    return true;
}

void PlaceEllipseTool::pushCommand()
{
    setUndoCommand(m_factory->createCreateObjectCommand(m_item.data()));
}

void PlaceEllipseTool::restart()
{
    m_item->setPos(QPointF());
    m_item->setRotation(0.0);
    m_item->setSize(QSizeF());
    m_item->setStartAngle(0.0);
    m_item->setSpanAngle(360.0);
    if (m_state != WaitForCenter)
        m_view->scene()->removeItem(m_item.data());
    m_state = WaitForCenter;
}

/******************************************************************************
 * PlaceRectangleTool
 *****************************************************************************/

PlaceRectangleTool::PlaceRectangleTool(QObject *parent)
    : GraphicsTool(parent)
    , m_item(new GraphicsRectangleItem)
{
    m_strokeStyle = Qs::SolidStroke;
    m_strokeColor = Qt::darkBlue;
    m_strokeWidth = Qs::MediumStroke;
    m_fillColor = Qt::lightGray;
    m_fillStyle = Qs::SolidFill;

    auto optionWidget = new ObjectPropertyBrowser();
    optionWidget->setObject(this, this);
    optionWidget->setStyle(ObjectPropertyBrowser::ListStyle);
    optionWidget->setBaseClassName("PlaceRectangleTool");
    setOptionWidget(optionWidget);
}

PlaceRectangleTool::~PlaceRectangleTool()
{

}

Qs::StrokeStyle PlaceRectangleTool::strokeStyle() const
{
    return m_strokeStyle;
}

Qs::StrokeWidth PlaceRectangleTool::strokeWidth() const
{
    return m_strokeWidth;
}

QColor PlaceRectangleTool::strokeColor() const
{
    return m_strokeColor;
}

Qs::FillStyle PlaceRectangleTool::fillStyle() const
{
    return m_fillStyle;
}

QColor PlaceRectangleTool::fillColor() const
{
    return m_fillColor;
}

void PlaceRectangleTool::setStrokeStyle(Qs::StrokeStyle strokeStyle)
{
    if (m_strokeStyle == strokeStyle)
        return;

    m_strokeStyle = strokeStyle;
    m_item->setPen(GraphicsItemFactory::createPen(m_strokeWidth,
                                                  m_strokeStyle,
                                                  m_strokeColor));
    emit strokeStyleChanged(strokeStyle);
}

void PlaceRectangleTool::setStrokeWidth(Qs::StrokeWidth strokeWidth)
{
    if (m_strokeWidth == strokeWidth)
        return;

    m_strokeWidth = strokeWidth;
    m_item->setPen(GraphicsItemFactory::createPen(m_strokeWidth,
                                                  m_strokeStyle,
                                                  m_strokeColor));
    emit strokeWidthChanged(strokeWidth);
}

void PlaceRectangleTool::setStrokeColor(QColor strokeColor)
{
    if (m_strokeColor == strokeColor)
        return;

    m_strokeColor = strokeColor;
    m_item->setPen(GraphicsItemFactory::createPen(m_strokeWidth,
                                                  m_strokeStyle,
                                                  m_strokeColor));
    emit strokeColorChanged(strokeColor);
}

void PlaceRectangleTool::setFillStyle(Qs::FillStyle fillStyle)
{
    if (m_fillStyle == fillStyle)
        return;

    m_fillStyle = fillStyle;
    m_item->setBrush(GraphicsItemFactory::createBrush(m_fillStyle,
                                                      m_fillColor));
    emit fillStyleChanged(fillStyle);
}

void PlaceRectangleTool::setFillColor(QColor fillColor)
{
    if (m_fillColor == fillColor)
        return;

    m_fillColor = fillColor;
    m_item->setBrush(GraphicsItemFactory::createBrush(m_fillStyle,
                                                      m_fillColor));
    emit fillColorChanged(fillColor);
}

void PlaceRectangleTool::pushCommand()
{
    setUndoCommand(m_factory->createCreateObjectCommand(m_item.data()));
}

void PlaceRectangleTool::activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(sourcePos);

    m_view = editor->graphicsView();
    m_factory = editor->graphicsItemFactory();
    editor->graphicsScene()->installEventHandler(this);
    m_item->setPos(pos);
    m_item->setRotation(0.0);
    m_item->setSize(QSizeF());
    m_item->setPen(GraphicsItemFactory::createPen(m_strokeWidth,
                                                  m_strokeStyle,
                                                  m_strokeColor));
    m_item->setBrush(GraphicsItemFactory::createBrush(m_fillStyle,
                                                      m_fillColor));
}

void PlaceRectangleTool::desactivate()
{
    if (m_item->scene() != nullptr)
        m_view->graphicsScene()->removeItem(m_item.data());
    m_view->graphicsScene()->uninstallEventHandler(this);
}

QString PlaceRectangleTool::caption() const
{
    return QStringLiteral("Rectangle tool");
}

QString PlaceRectangleTool::description() const
{
    return QStringLiteral("Place rectangles");
}

QKeySequence PlaceRectangleTool::shortcut() const
{
    return QKeySequence("r");
}

QIcon PlaceRectangleTool::icon() const
{
    return QIcon::fromTheme("draw-rectangle");
}

bool PlaceRectangleTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceRectangleTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceRectangleTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    const auto pos = event->scenePos();
    switch (m_state) {
        case PlaceRectangleTool::WaitForCenter:
            m_item->setPos(pos);
            break;
        case PlaceRectangleTool::WaitForCorner: {
            const QLineF line = QLineF(m_item->pos(), pos);
            m_item->setSize(QSizeF(2*qAbs(line.dx()), 2*qAbs(line.dy())));
            break;
        }
    }
    return true;
}

bool PlaceRectangleTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    switch (m_state) {
        case PlaceRectangleTool::WaitForCenter:
            m_item->setSize(QSizeF());
            m_view->scene()->addItem(m_item.data());
            m_state = PlaceRectangleTool::WaitForCorner;
            break;
        case PlaceRectangleTool::WaitForCorner:
            pushCommand();
            m_view->scene()->removeItem(m_item.data());
            m_state = PlaceRectangleTool::WaitForCenter;
            break;
    }
    return true;
}

/******************************************************************************
 * PlacePortTool
 *****************************************************************************/

PlacePortTool::PlacePortTool(QObject *parent)
    : GraphicsTool(parent)
    , m_view(nullptr)
    , m_item(new GraphicsPortItem())
{
    m_item->setPen(QPen(QColor("#fa0000"), 0.0));

    auto optionWidget = new ObjectPropertyBrowser();
    optionWidget->setObject(this, this);
    optionWidget->setStyle(ObjectPropertyBrowser::ListStyle);
    optionWidget->setBaseClassName("PlacePortTool");
    setOptionWidget(optionWidget);
}

PlacePortTool::~PlacePortTool()
{

}

bool PlacePortTool::autoIncrement() const
{
    return m_autoIncrement;
}

int PlacePortTool::sequenceNumber() const
{
    return m_sequenceNumber;
}

void PlacePortTool::setAutoIncrement(bool autoIncrement)
{
    if (m_autoIncrement == autoIncrement)
        return;

    m_autoIncrement = autoIncrement;
    emit autoIncrementChanged(autoIncrement);
}

void PlacePortTool::setSequenceNumber(int sequenceNumber)
{
    if (m_sequenceNumber == sequenceNumber)
        return;

    m_sequenceNumber = sequenceNumber;
    m_item->setName(QString::number(m_sequenceNumber));
    emit sequenceNumberChanged(sequenceNumber);
}

void PlacePortTool::pushCommand()
{
    setUndoCommand(m_factory->createCreateObjectCommand(m_item.data()));
    if (m_autoIncrement)
        setSequenceNumber(m_sequenceNumber+1);
}

void PlacePortTool::activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(sourcePos);

    m_view = editor->graphicsView();
    m_factory = editor->graphicsItemFactory();
    m_item->setPos(pos);
    m_item->setRotation(0.0);
    m_item->setName(QString::number(m_sequenceNumber));

    editor->graphicsScene()->installEventHandler(this);
    editor->graphicsScene()->addItem(m_item.data());
}

void PlacePortTool::desactivate()
{
    m_view->graphicsScene()->removeItem(m_item.data());
    m_view->graphicsScene()->uninstallEventHandler(this);
}

QString PlacePortTool::caption() const
{
    return "Port tool";
}

QString PlacePortTool::description() const
{
    return "Place ports";
}

QKeySequence PlacePortTool::shortcut() const
{
    return QKeySequence("p");
}

QIcon PlacePortTool::icon() const
{
    return QIcon::fromTheme("draw-donut");
}

bool PlacePortTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlacePortTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlacePortTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    m_item->setPos(event->scenePos());
    return true;
}

bool PlacePortTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    pushCommand();
    return true;
}

/******************************************************************************
 * PlaceLineTool
 *****************************************************************************/

PlaceLineTool::PlaceLineTool(QObject *parent)
    : GraphicsTool(parent)
    , m_view(nullptr)
    , m_item(new GraphicsLineItem())
{
    m_strokeStyle = Qs::SolidStroke;
    m_strokeColor = Qt::darkBlue;
    m_strokeWidth = Qs::MediumStroke;

    auto optionWidget = new ObjectPropertyBrowser();
    optionWidget->setObject(this, this);
    optionWidget->setStyle(ObjectPropertyBrowser::ListStyle);
    optionWidget->setBaseClassName("PlaceLineTool");
    setOptionWidget(optionWidget);
}

PlaceLineTool::~PlaceLineTool()
{

}

Qs::StrokeStyle PlaceLineTool::strokeStyle() const
{
    return m_strokeStyle;
}

Qs::StrokeWidth PlaceLineTool::strokeWidth() const
{
    return m_strokeWidth;
}

QColor PlaceLineTool::strokeColor() const
{
    return m_strokeColor;
}

void PlaceLineTool::setStrokeStyle(Qs::StrokeStyle strokeStyle)
{
    if (m_strokeStyle == strokeStyle)
        return;

    m_strokeStyle = strokeStyle;
    m_item->setPen(GraphicsItemFactory::createPen(m_strokeWidth,
                                                  m_strokeStyle,
                                                  m_strokeColor));
    emit strokeStyleChanged(strokeStyle);
}

void PlaceLineTool::setStrokeWidth(Qs::StrokeWidth strokeWidth)
{
    if (m_strokeWidth == strokeWidth)
        return;

    m_strokeWidth = strokeWidth;
    m_item->setPen(GraphicsItemFactory::createPen(m_strokeWidth,
                                                  m_strokeStyle,
                                                  m_strokeColor));
    emit strokeWidthChanged(strokeWidth);
}

void PlaceLineTool::setStrokeColor(QColor strokeColor)
{
    if (m_strokeColor == strokeColor)
        return;

    m_strokeColor = strokeColor;
    m_item->setPen(GraphicsItemFactory::createPen(m_strokeWidth,
                                                  m_strokeStyle,
                                                  m_strokeColor));
    emit strokeColorChanged(strokeColor);
}

void PlaceLineTool::pushCommand()
{
    setUndoCommand(m_factory->createCreateObjectCommand(m_item.data()));
}

void PlaceLineTool::activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(sourcePos);

    m_view = editor->graphicsView();
    m_factory = editor->graphicsItemFactory();
    m_state = WaitForP1;
    m_item->setPos(QPointF());
    m_item->setP1(pos);
    m_item->setP2(QPointF());
    m_item->setRotation(0.0);
    m_item->setPen(GraphicsItemFactory::createPen(m_strokeWidth,
                                                  m_strokeStyle,
                                                  m_strokeColor));

    m_view->graphicsScene()->installEventHandler(this);
}

void PlaceLineTool::desactivate()
{
    if (m_item->scene() != nullptr)
        m_view->graphicsScene()->removeItem(m_item.data());
    m_view->graphicsScene()->uninstallEventHandler(this);
}

QString PlaceLineTool::caption() const
{
    return "Line tool";
}

QString PlaceLineTool::description() const
{
    return "Place lines";
}

QKeySequence PlaceLineTool::shortcut() const
{
    return QKeySequence("l");
}

QIcon PlaceLineTool::icon() const
{
    return QIcon::fromTheme("draw-line");
}

bool PlaceLineTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceLineTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceLineTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    const auto pos = event->scenePos();
    switch (m_state) {
        case PlaceLineTool::WaitForP1:
            m_item->setP1(pos);
            break;
        case PlaceLineTool::WaitForP2:
            m_item->setP2(pos);
            break;
    }
    return true;
}

bool PlaceLineTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;


    switch (m_state) {
        case PlaceLineTool::WaitForP1:
            m_item->setP2(m_item->p1());
            m_state = PlaceLineTool::WaitForP2;
            m_view->scene()->addItem(m_item.data());
            break;
        case PlaceLineTool::WaitForP2:
            m_state = PlaceLineTool::WaitForP1;
            m_view->scene()->removeItem(m_item.data());
            pushCommand();
            break;
    }
    return true;
}

/******************************************************************************
 * PlaceLabelTool
 *****************************************************************************/

PlaceLabelTool::PlaceLabelTool(QObject *parent)
    : GraphicsTool(parent)
    , m_view(nullptr)
    , m_item(new GraphicsLabelItem())
{
    m_color = Qt::black;
    m_fontSize = 12;
    auto optionWidget = new ObjectPropertyBrowser();
    optionWidget->setObject(this, this);
    optionWidget->setStyle(ObjectPropertyBrowser::ListStyle);
    optionWidget->setBaseClassName("PlaceLabelTool");
    setOptionWidget(optionWidget);
}

PlaceLabelTool::~PlaceLabelTool()
{
}

QColor PlaceLabelTool::color() const
{
    return m_color;
}

int PlaceLabelTool::fontSize() const
{
    return m_fontSize;
}

void PlaceLabelTool::setColor(QColor color)
{
    if (m_color == color)
        return;

    m_color = color;
    m_item->setBrush(m_color);
    emit colorChanged(color);
}

void PlaceLabelTool::setFontSize(int fontSize)
{
    if (m_fontSize == fontSize)
        return;

    m_fontSize = fontSize;
    auto font = m_item->font();
    font.setPointSize(m_fontSize);
    m_item->setFont(font);
    emit fontSizeChanged(fontSize);
}

void PlaceLabelTool::activate(GraphicsWidget *editor, const QPointF &pos, const QPointF &sourcePos)
{
    Q_UNUSED(sourcePos);

    m_view = editor->graphicsView();
    m_factory = editor->graphicsItemFactory();
    m_item->setPos(pos);
    m_item->setRotation(0.0);
    m_item->setFont(QFont());
    m_item->setText(QString("Text"));
    auto font = m_item->font();
    font.setPointSize(m_fontSize);
    m_item->setFont(font);

    m_view->graphicsScene()->addItem(m_item.data());
    m_view->graphicsScene()->installEventHandler(this);
}

void PlaceLabelTool::desactivate()
{
    m_view->graphicsScene()->uninstallEventHandler(this);
    m_view->graphicsScene()->removeItem(m_item.data());
}

QString PlaceLabelTool::caption() const
{
    return QStringLiteral("Label tool");
}

QString PlaceLabelTool::description() const
{
    return QStringLiteral("Place text labels");
}

QKeySequence PlaceLabelTool::shortcut() const
{
    return QKeySequence("t");
}

QIcon PlaceLabelTool::icon() const
{
    return QIcon::fromTheme("draw-text");
}

bool PlaceLabelTool::graphicsSceneMouseDoubleClickEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceLabelTool::graphicsSceneMousePressEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    return true;
}

bool PlaceLabelTool::graphicsSceneMouseMoveEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    m_item->setPos(event->scenePos());
    return true;
}

bool PlaceLabelTool::graphicsSceneMouseReleaseEvent(GraphicsScene *scene, QGraphicsSceneMouseEvent *event, const QGraphicsSceneMouseEvent *sourceEvent)
{
    Q_UNUSED(scene);
    Q_UNUSED(sourceEvent);

    if (event->button() != Qt::LeftButton)
        return false;

    pushCommand();
    return true;
}

void PlaceLabelTool::pushCommand()
{
    setUndoCommand(m_factory->createCreateObjectCommand(m_item.data()));
}
