/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "DocumentNavigationWidget.h"
#include "DocumentOutlineModel.h"

#include "qlGui/ObjectPropertyBrowser.h"
#include "qlCore/SymbolObjects.h"
#include "qlCore/SymbolDocument.h"

#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>

DocumentNavigationWidget::DocumentNavigationWidget(QWidget *parent)
    : QWidget(parent)
    , m_outlineModel(new DocumentOutlineModel(this))
    , m_outlineView(new QTreeView())
    , m_propertyBrowser(new ObjectPropertyBrowser())
    , m_propertyObjectProxy(new ObjectProxy(this))
{
    setLayout(new QVBoxLayout());
    layout()->addWidget(m_outlineView);
    layout()->addWidget(m_propertyBrowser);

    m_outlineView->setModel(m_outlineModel);
    m_outlineView->setSelectionMode(QTreeView::ExtendedSelection);

    m_propertyBrowser->setStyle(ObjectPropertyBrowser::TreeStyle);
    m_propertyBrowser->setBaseClassName("Object");
}

DocumentNavigationWidget::~DocumentNavigationWidget()
{

}

void DocumentNavigationWidget::setDocument(const SymbolDocument *document)
{
    if (m_outlineModel->document() == document)
        return;

    if (m_outlineModel->document() != nullptr) {
        endMonitorDocument();
    }

    m_outlineModel->setDocument(document);

    if (m_outlineModel->document() != nullptr) {
        beginMonitorDocument();
    }
}

const SymbolDocument *DocumentNavigationWidget::document() const
{
    return m_outlineModel->document();
}

UndoCommand *DocumentNavigationWidget::takeCommand()
{
    return m_propertyObjectProxy->takeCommand();
}

void DocumentNavigationWidget::onViewSelectionChanged()
{
    const auto selection = m_outlineView->selectionModel()->selection();   
    const auto selectedIndexes = selection.indexes();
    bool isEmptySelection = selectedIndexes.isEmpty();
    bool isSingleSelection = selection.indexes().count() == 1;

    if (isEmptySelection) {
        m_outlineModel->document()->clearSelection();
        m_propertyBrowser->setObject(nullptr, nullptr);
        return;
    }

    // Update Document selection
    QSet<ObjectUid> selectedUids;
    for (auto const &selectedIndex: selectedIndexes)
        selectedUids.insert(m_outlineModel->objectUid(selectedIndex));
    m_outlineModel->document()->select(selectedUids);

    // Update property browser
    if (isSingleSelection) {
        const auto uid = m_outlineModel->objectUid(selectedIndexes.first());
        const auto object = m_outlineModel->document()->object(uid);
        m_propertyObjectProxy->setObject(object);
        m_propertyBrowser->setObject(object, m_propertyObjectProxy);
    }
    else
        m_propertyBrowser->setObject(nullptr, nullptr);
}

void DocumentNavigationWidget::onDocumentSelectionChanged(const QSet<ObjectUid> &current, const QSet<ObjectUid> &previous)
{
    Q_UNUSED(previous);

    // Update Outline view selection
    QItemSelection itemSelection;
    for (ObjectUid uid: current) {
        QModelIndex index = m_outlineModel->objectIndex(uid);
        QItemSelectionRange itemSelectionRange(index, index);
        itemSelection.append(itemSelectionRange);
    }
    m_outlineView->selectionModel()->select(itemSelection,
                                            QItemSelectionModel::Clear|
                                            QItemSelectionModel::Select |
                                            QItemSelectionModel::Rows);
}

void DocumentNavigationWidget::beginMonitorDocument()
{
    connect(m_outlineModel->document(), &SymbolDocument::selectionChanged,
            this, &DocumentNavigationWidget::onDocumentSelectionChanged);
    connect(m_outlineView->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, &DocumentNavigationWidget::onViewSelectionChanged);
    connect(m_propertyObjectProxy, &ObjectProxy::commandAvailable,
            this, &DocumentNavigationWidget::commandAvailable);
    m_outlineView->setExpanded(m_outlineModel->documentIndex(), true);
}

void DocumentNavigationWidget::endMonitorDocument()
{
    m_outlineView->disconnect(this);
    m_propertyObjectProxy->disconnect(this);
    m_outlineModel->document()->disconnect(this);
    m_outlineModel->setDocument(nullptr);
    m_propertyObjectProxy->setObject(nullptr);
    m_propertyBrowser->setObject(nullptr, nullptr);
}
