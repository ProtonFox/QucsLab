/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "DocumentNavigationWidget.h"
#include "GraphicsItemFactory.h"
#include "GraphicsSelectToolOptionWidget.h"
#include "GraphicsTool.h"
#include "GraphicsWidget.h"

#include "qlGui/GraphicsGrid.h"
#include "qlGui/GraphicsItem.h"
#include "qlGui/GraphicsScene.h"
#include "qlGui/GraphicsSheet.h"
#include "qlGui/GraphicsSnapper.h"
#include "qlGui/GraphicsView.h"
#include "qlGui/GraphicsZoomHandler.h"
#include "qlCore/SymbolObjects.h"
#include "qlCore/SymbolDocument.h"
#include "qlCore/SymbolReader.h"
#include "qlCore/SymbolWriter.h"
#include "qlCore/SymbolCommand.h"

#include <QtCore/QBuffer>
#include <QtCore/QFile>
#include <QtCore/QMimeData>
#include <QtCore/QStack>
#include <QtGui/QClipboard>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QUndoStack>
#include <QtWidgets/QUndoView>
#include <QtWidgets/QVBoxLayout>

const QString GraphicsWidget::g_symbolFragmentMimeType("application/vnd+qucs.gui.symbol.fragment+xml");

GraphicsWidget::GraphicsWidget(QWidget *parent)
    : QWidget(parent)
    , m_undoStack(new QUndoStack(this))
    , m_scene(new GraphicsScene(this))
    , m_view(new GraphicsView())
    , m_sheet(new GraphicsSheet())
    , m_grid(new GraphicsGrid())
    , m_snapper(new GraphicsSnapper(this))
    , m_zoomHandler(new GraphicsZoomHandler(this))
    , m_itemFactory(new GraphicsItemFactory(this))
    , m_selectTool(new GraphicsSelectTool(this))
    , m_moveTool(new GraphicsMoveTool(this))
    , m_cloneTool(new GraphicsCloneTool(this))
    , m_placeItemsTool(new PlaceItemsTool(this))
    , m_placeEllipseTool(new PlaceEllipseTool(this))
    , m_placeLabelTool(new PlaceLabelTool(this))
    , m_placeLineTool(new PlaceLineTool(this))
    , m_placePortTool(new PlacePortTool(this))
    , m_placeRectangleTool(new PlaceRectangleTool(this))
    , m_toolActionGroup(new QActionGroup(this))
    , m_editActionGroup(new QActionGroup(this))
    , m_toolOptionDockWidget(new QDockWidget())
    , m_commandHistoryWidget(new QUndoView())
    , m_commandHistoryDockWidget(new QDockWidget())
{
    loadTools();
    createActions();
    createToolBars();
    createDockWidgets();
    setupGraphicsView();
    setupUndoStack();
    setupClipBoard();

    setLayout(new QVBoxLayout());
    layout()->addWidget(m_view);
}

GraphicsWidget::~GraphicsWidget()
{

}

void GraphicsWidget::setDocument(SymbolDocument *document)
{
    if (m_document != nullptr)
        endEditDocument();

    m_document = document;

    if (m_document != nullptr)
        beginEditDocument();
}

SymbolDocument *GraphicsWidget::document() const
{
    return m_document;
}

GraphicsView *GraphicsWidget::graphicsView() const
{
    return m_view;
}

GraphicsScene *GraphicsWidget::graphicsScene() const
{
    return m_scene;
}

GraphicsItemFactory *GraphicsWidget::graphicsItemFactory() const
{
    return m_itemFactory;
}

void GraphicsWidget::saveDocument(const QString &path)
{
    SymbolWriter writer;
    QFile file(path);
    if (!file.open(QFile::WriteOnly)) {
        QMessageBox::critical(this, "Error", file.errorString());
        return;
    }
    if (!writer.writeSymbol(&file, m_document->symbol())) {
        QMessageBox::critical(this, "Error", writer.errorString());
        return;
    }
    if (file.error() != QFile::NoError) {
        QMessageBox::critical(this, "Error", file.errorString());
        return;
    }
    file.close();
    m_undoStack->setClean();
}

void GraphicsWidget::openDocument(const QString &path)
{
    SymbolReader reader;
    QFile file(path);
    if (!file.open(QFile::ReadOnly)) {
        QMessageBox::critical(this, "Error", file.errorString());
        return;
    }
    if (!reader.readSymbol(&file)) {
        QMessageBox::critical(this, "Error", reader.errorString());
        return;
    }
    file.close();

    setDocument(new SymbolDocument(reader.takeSymbol()));
}

void GraphicsWidget::closeDocument()
{
    setDocument(nullptr);
}

QList<QAction *> GraphicsWidget::editActions() const
{
    QList<QAction *> result;
    for (auto action: m_toolActionGroup->actions()) {
        if (action->text() == m_selectTool->caption())
            result.append(action);
        else if (action->text() == m_moveTool->caption())
            result.append(action);
        else if (action->text() == m_cloneTool->caption())
            result.append(action);
    }
    auto separator = new QAction();
    separator->setSeparator(true);
    result.append(separator);
    result.append(m_selectTool->quickActions());
    separator = new QAction();
    separator->setSeparator(true);
    result.append(separator);
    result.append(m_editActionGroup->actions());
    return result;
}

QList<QAction *> GraphicsWidget::placementActions() const
{
    QList<QAction *> result;
    for (auto action: m_toolActionGroup->actions()) {
        if (action->text() == m_selectTool->caption())
            continue;
        if (action->text() == m_moveTool->caption())
            continue;
        if (action->text() == m_cloneTool->caption())
            continue;
        result.append(action);
    }
    return result;
}

QList<QAction *> GraphicsWidget::toolsActions() const
{
    return QList<QAction *>();
}

QList<QAction *> GraphicsWidget::viewActions() const
{
    return m_viewActions;
}

QList<QDockWidget *> GraphicsWidget::dockWidgets() const
{
    return m_dockWidgets;
}

QList<QToolBar *> GraphicsWidget::toolBars() const
{
    return m_toolBars;
}

bool GraphicsWidget::canUndo() const
{
    return m_undoStack->canUndo();
}

bool GraphicsWidget::canRedo() const
{
    return m_undoStack->canRedo();
}

bool GraphicsWidget::canCut() const
{
    return m_document && !m_document->selection().isEmpty();
}

bool GraphicsWidget::canCopy() const
{
    return m_document && !m_document->selection().isEmpty();
}

// FIXME:
bool GraphicsWidget::canPaste() const
{
    return true;
}

bool GraphicsWidget::canMove() const
{
    return m_document && !m_document->selection().isEmpty();
}

bool GraphicsWidget::canDelete() const
{
    return m_document && !m_document->selection().isEmpty();
}

bool GraphicsWidget::canSelectAll() const
{
    return true;
}

bool GraphicsWidget::canClearSelection() const
{
    return m_document && !m_document->selection().isEmpty();
}

bool GraphicsWidget::canInvertSelection() const
{
    return m_document && !m_document->selection().isEmpty();
}

bool GraphicsWidget::isModified() const
{
    return !m_undoStack->isClean();
}

void GraphicsWidget::undo()
{
    m_undoStack->undo();
}

void GraphicsWidget::redo()
{
    m_undoStack->redo();
}

void GraphicsWidget::cut(QClipboard *clipboard)
{
    copy(clipboard);
    deleteIt();
}

// TODO: move mime data format definition to global QucsLab namespace
void GraphicsWidget::copy(QClipboard *clipboard)
{
    QBuffer buffer;
    SymbolWriter writer;
    buffer.open(QBuffer::WriteOnly);
    auto objects = m_document->selectedObjects();
    if (!writer.writeObjects(&buffer, objects)) {
        qWarning() << "Failed to serialise selected objects:" << writer.errorString();
        return;
    }
    auto mimeData = new QMimeData();
    mimeData->setData(g_symbolFragmentMimeType, buffer.data());
    clipboard->setMimeData(mimeData);
}

void GraphicsWidget::paste(const QClipboard *clipboard)
{
    if (!clipboard->mimeData()->hasFormat(g_symbolFragmentMimeType)) {
        qWarning() << "Unable to read pasted data of unkown MIME type";
        return;
    }

    // FIXME: Hack to make the reader happy: make the data looks like a symbol document
    // This relies on another hack in SymbolReader!
    QByteArray data;
    data.append("<symbol xmlns=\"\" version=\"1.0\">\n");
    data.append(clipboard->mimeData()->data(g_symbolFragmentMimeType));
    data.append("</symbol>");
    QBuffer buffer;
    buffer.setData(data);
    buffer.open(QBuffer::ReadOnly);
    SymbolReader reader;
    if (!reader.readObjects(&buffer)) {
        qWarning() << "Unable to read pasted data, althout it claimed to have the right MIME type";
        return;
    }
    auto objects = reader.objects();
    auto items = m_itemFactory->createItems(objects);
    m_placeItemsTool->setItems(items);
    setCurrentTool(m_placeItemsTool);
}

void GraphicsWidget::moveIt()
{
    setCurrentTool(m_moveTool);
}

void GraphicsWidget::deleteIt()
{
    auto uids = m_document->selection();
    auto *command = new DeleteObjectCommand();
    command->setText(QString("Delete %1 object%2").arg(uids.count()).arg(uids.count() == 1 ? "" : "s"));
    command->setUids(uids);
    executeCommand(command);
}

void GraphicsWidget::selectAll()
{
    m_document->selectAll();
}

void GraphicsWidget::clearSelection()
{
    m_document->clearSelection();
}

void GraphicsWidget::invertSelection()
{
    m_document->invertSelection();
}

void GraphicsWidget::createActions()
{
    for (auto tool: m_tools) {
        auto action = new QAction(tool->icon(), tool->caption());
        action->setShortcut(tool->shortcut());
        action->setToolTip(QString("%1: %2 <i>%3</i>").arg(tool->caption(),
                                                           tool->description(),
                                                           tool->shortcut().toString()));
        action->setCheckable(true);
        action->setChecked(false);
        action->setData(QVariant::fromValue(tool));
        m_toolActionGroup->addAction(action);

        connect(tool, &GraphicsTool::undoCommandAvailable,
                this, [this, tool]() {
            executeCommand(tool->takeUndoCommand());
        });
        connect(tool, &GraphicsTool::finished,
                this, [this]() {
            m_toolActionGroup->actions().first()->trigger();
        });
    }

    m_toolActionGroup->actions().first()->setChecked(true);
    m_toolActionGroup->setExclusive(true);
    connect(m_toolActionGroup, &QActionGroup::triggered,
            this, [this](QAction *triggered) {
        auto tool = triggered->data().value<GraphicsTool*>();
        setCurrentTool(tool);
    });

    auto zoomDrawAction = new QAction(QIcon::fromTheme("zoom-original"), "Zoom to drawing");
    connect(zoomDrawAction, &QAction::triggered,
            this, &GraphicsWidget::zoomToDrawing);
    auto zoomSelectAction = new QAction(QIcon::fromTheme("zoom-draw"), "Zoom to selection");
    connect(zoomSelectAction, &QAction::triggered,
            this, &GraphicsWidget::zoomToSelection);
    auto zoomFitAction = new QAction(QIcon::fromTheme("zoom"), "Zoom to fit");
    connect(zoomFitAction, &QAction::triggered,
            this, &GraphicsWidget::zoomToFit);

    m_viewActions << zoomDrawAction
                  << zoomSelectAction
                  << zoomFitAction;
}

void GraphicsWidget::createToolBars()
{
    auto bar = new QToolBar("Edit");
    bar->addActions(m_toolActionGroup->actions());
    m_toolBars.append(bar);
    bar = new QToolBar("View");
    bar->addActions(m_viewActions);
    m_toolBars.append(bar);
}

void GraphicsWidget::createDockWidgets()
{
    m_toolOptionDockWidget->setObjectName("org.qucs.symboleditor.toolproperties");
    m_toolOptionDockWidget->setWindowTitle("Tool");
    m_dockWidgets.append(m_toolOptionDockWidget);

    m_commandHistoryWidget->setStack(m_undoStack);
    m_commandHistoryDockWidget->setObjectName("org.qucs.symboleditor.commandhistory");
    m_commandHistoryDockWidget->setWidget(m_commandHistoryWidget);
    m_commandHistoryDockWidget->setWindowTitle("Command history");
    m_commandHistoryDockWidget->hide(); // FIXME: Should be in user settings
    m_dockWidgets.append(m_commandHistoryDockWidget);
}

void GraphicsWidget::setupGraphicsView()
{
    m_view->setAcceptDrops(true);
    m_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_view->setBackgroundBrush(QColor("#eee8d5"));
    m_view->setInteractive(true);
    m_view->setDragMode(QGraphicsView::NoDrag);

    m_sheet->setRect(QRectF(-297/2.0, -210/2.0, 297.0, 210.0));
    m_sheet->setPen(QPen(QColor("#93a1a1"), 0.0));
    m_sheet->setBrush(QColor("#fdf6e3"));

    // TODO: connect to sheet's sceneRectChanged()
    m_grid->setRect(m_sheet->rect());
    m_grid->setPos(m_sheet->pos());
    m_grid->setMajorColor(QColor("#93a1a1"));
    m_grid->setMinorColor(QColor("#657b83"));
    m_grid->setOriginColor(QColor("#859900"));
    m_grid->setStep(2.0); // mm
    m_grid->setMinimumGridSize(50); // pixels

    // TODO: connect to grid's stepChanged()
    m_snapper->setStep(m_grid->step()/10.0); // minor grid

    m_scene->addItem(m_sheet);
    m_scene->addItem(m_grid);
    m_scene->setSceneRect(m_scene->itemsBoundingRect());
    m_view->setScene(m_scene);

    // FIXME: Handlers need to be attached after m_view->setScene(m_scene)
    m_zoomHandler->setView(m_view);
    m_zoomHandler->setMaximumZoomFactor(50);
    m_zoomHandler->setMinimumZoomFactor(1);
    m_snapper->setView(m_view);
}

void GraphicsWidget::setupUndoStack()
{
    connect(m_undoStack, &QUndoStack::canUndoChanged,
            this, &GraphicsWidget::canUndoChanged);
    connect(m_undoStack, &QUndoStack::canRedoChanged,
            this, &GraphicsWidget::canRedoChanged);
    connect(m_undoStack, &QUndoStack::cleanChanged,
            this, [this](bool clean) {
        emit isModifiedChanged(!clean);
    });
}

void GraphicsWidget::setupClipBoard()
{

}
void GraphicsWidget::setCurrentTool(GraphicsTool *tool)
{
    if (m_currentTool != nullptr) {
        m_currentTool->desactivate();
        m_toolOptionDockWidget->setWindowTitle("No tool");
        m_toolOptionDockWidget->setWidget(nullptr);
    }

    m_currentTool = tool;

    if (m_currentTool != nullptr) {
        const auto pos = m_view->mapToScene(m_view->mapFromGlobal(QCursor::pos()));
        const auto snappedPos = m_snapper->snapScenePos(pos);
        m_toolOptionDockWidget->setWindowTitle(m_currentTool->caption());
        m_toolOptionDockWidget->setWidget(m_currentTool->optionWidget());
        m_currentTool->activate(this, snappedPos, pos);
    }
}

void GraphicsWidget::beginEditDocument()
{
    connect(m_document, &SymbolDocument::selectionChanged,
            this, [this]() {
        // TODO: Have a dedicated onDocumentationSelectionChanged()
        emit canMoveChanged(canMove());
        emit canDeleteChanged(canDelete());
        emit canCopyChanged(canCopy());
        emit canCutChanged(canCut());
        emit canSelectAllChanged(canSelectAll());
        emit canClearSelectionChanged(canClearSelection());
        emit canInvertSelectionChanged(canInvertSelection());
    });
    connect(m_document, &SymbolDocument::objectAdded,
            this, QOverload<ObjectUid>::of(&GraphicsWidget::addSceneItem));
    connect(m_document, &SymbolDocument::aboutToRemoveObject,
            this, &GraphicsWidget::removeSceneItem);
    connect(m_document, &SymbolDocument::objectUpdated,
            this, &GraphicsWidget::updateSceneItem);
    connect(m_document, &SymbolDocument::aboutToMoveObject,
            this, &GraphicsWidget::updateItemOrder);
    connect(m_document, &SymbolDocument::selectionChanged,
            this, &GraphicsWidget::updateSceneSelectionFromDocumentSelection);
    connect(m_scene, &GraphicsScene::selectionChanged,
            this, &GraphicsWidget::updateDocumentSelectionFromSceneSelection);

    for (int i=0; i<m_document->objectCount(); ++i)
        addSceneItem(m_document->object(i));

    setCurrentTool(m_tools.first()); // select
    zoomToFit();
}

void GraphicsWidget::endEditDocument()
{
    m_scene->disconnect(this);
    m_document->disconnect(this);
    for (auto uid: m_document->objectUids())
        if (uid != m_document->symbol()->uid())
            removeSceneItem(uid);
    m_undoStack->clear();
    setCurrentTool(nullptr);
    // TBD: might need to notify the tools too?
    // Should they even have access to the document? Ideally no
    // Currently select need to b/c the selection is part of the document
    // which is wrong in the first place
}

void GraphicsWidget::executeCommand(UndoCommand *command)
{
    command->setDocument(m_document);
    m_undoStack->push(command);
}

void GraphicsWidget::addSceneItem(const Object *object)
{
    auto item = m_itemFactory->createItem(object);
    m_scene->addGraphicsItem(item, object->uid());
}

void GraphicsWidget::addSceneItem(ObjectUid uid)
{
    addSceneItem(m_document->object(uid));
}

void GraphicsWidget::removeSceneItem(ObjectUid uid)
{
    delete m_scene->takeGraphicsItem(uid);
}

void GraphicsWidget::updateSceneItem(ObjectUid uid)
{
    if (uid == m_document->symbol()->uid())
        return;
    auto object = m_document->object(uid);
    Q_ASSERT(object != nullptr);
    auto item = m_scene->graphicsItem(uid);
    Q_ASSERT(item != nullptr);
    m_itemFactory->updateItem(item, object);
}

void GraphicsWidget::updateSceneSelectionFromDocumentSelection(const QSet<ObjectUid> &current, const QSet<ObjectUid> &previous)
{
    m_updatingSceneSelection = true;
    auto unchanged = current & previous;
    auto deselected = previous - current;
    auto selected = current - unchanged;
    for (const auto &uid: deselected)
        if (uid != m_document->symbol()->uid())
            m_scene->graphicsItem(uid)->setSelected(false);
    for (const auto &uid: selected)
        if (uid != m_document->symbol()->uid())
            m_scene->graphicsItem(uid)->setSelected(true);
    m_updatingSceneSelection = false;
}

void GraphicsWidget::updateDocumentSelectionFromSceneSelection()
{
    if (m_updatingSceneSelection)
        return;

    // FIXME: Need an ordered selection when copy/pasting, cloning, ...
    // So: Don't use QSet and implement our own selectedItems as
    // QGS doesn't guarantee any order
    QSet<ObjectUid> selection;
    for (const auto item: m_scene->selectedItems()) {
        selection.insert(m_scene->uid(item));
    }
    m_document->select(selection);
}

void GraphicsWidget::zoomToDrawing()
{
    zoomToBox(m_sheet->boundingRect(), 0.05);
}

void GraphicsWidget::zoomToSelection()
{
    auto items = m_scene->selectedItems();
    if (items.isEmpty())
        return;

    QRectF rect;
    for (auto item: items)
        rect |= item->mapToScene(item->boundingRect()).boundingRect();

    zoomToBox(rect, 0.1);
}

void GraphicsWidget::zoomToFit()
{
    auto items = m_scene->items();
    if (items.isEmpty())
        return;

    QRectF rect;
    for (auto item: items) {
        if (item != m_sheet && item != m_grid) {
            rect |= item->mapToScene(item->boundingRect()).boundingRect();
        }
    }

    zoomToBox(rect, 0.1);
}

void GraphicsWidget::zoomToBox(const QRectF &rect, qreal marginFactor)
{
    const auto margin = qMax(rect.width(), rect.height())*marginFactor/2.0;
    const QRectF box = rect.adjusted(-margin, -margin, margin, margin);
    m_view->fitInView(box, Qt::KeepAspectRatio);
    m_view->centerOn(box.center());
}

void GraphicsWidget::updateItemOrder(ObjectUid uid, int newIndex)
{
    auto itemToMove = m_scene->graphicsItem(uid);
    const int oldIndex = m_document->objectIndex(uid);
    const int lowestIndex = qMin(oldIndex, newIndex);
    const int highestIndex = m_document->objectCount()-1;

    if (oldIndex == newIndex)
        return;

    m_updatingSceneSelection = true;

    QList<QGraphicsItem *> items;
    for (int index=lowestIndex; index<=highestIndex; index++) {
        auto object = m_document->object(index);
        auto item = m_scene->takeGraphicsItem(object->uid());
        items.append(item);
    }
    items.takeAt(oldIndex-lowestIndex);
    items.insert(newIndex-lowestIndex, itemToMove);
    for (auto item: items)
        m_scene->addGraphicsItem(item, item->data(GraphicsScene::DocumentIdProperty).value<ObjectUid>());

    m_updatingSceneSelection = false;
}

void GraphicsWidget::loadTools()
{
    // FIXME: Place items is special used for Paste and Clone
    connect(m_placeItemsTool, &GraphicsTool::undoCommandAvailable,
            this, [this]() {
        executeCommand(m_placeItemsTool->takeUndoCommand());
    });
    connect(m_placeItemsTool, &GraphicsTool::finished,
            this, [this]() {
        m_toolActionGroup->actions().first()->trigger();
    });

    m_tools << m_selectTool
            << m_moveTool
            << m_cloneTool
            << m_placeLineTool
            << m_placeEllipseTool
            << m_placeRectangleTool
            << m_placeLabelTool
            << m_placePortTool;
}
