#-------------------------------------------------
#
# Project created by QtCreator 2017-04-23T14:14:18
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = symbol-editor
TEMPLATE = app

DEFINES += DATADIR=\\\"$$PWD/../../data/symbols/\\\"


SOURCES += main.cpp\
        MainWindow.cpp \
    GraphicsTool.cpp \
    DocumentOutlineModel.cpp \
    DocumentNavigationWidget.cpp \
    GraphicsWidget.cpp \
    FileSystemNavigationWidget.cpp \
    GraphicsSelectToolOptionWidget.cpp \
    GraphicsItemFactory.cpp \

HEADERS  += MainWindow.h \
    GraphicsTool.h \
    DocumentOutlineModel.h \
    DocumentNavigationWidget.h \
    GraphicsWidget.h \
    FileSystemNavigationWidget.h \
    GraphicsSelectToolOptionWidget.h \
    GraphicsItemFactory.h \

RESOURCES += \
    assets/QucsLabLightIconTheme.qrc \
    assets/QucsLabDarkIconTheme.qrc

include($$top_srcdir/QucsLab.pri)
include($$top_srcdir/libs/qlCore/qlCore.pri)
include($$top_srcdir/libs/qlGui/qlGui.pri)
