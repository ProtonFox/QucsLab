/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsItemFactory.h"

#include "qlCore/SymbolObjects.h"
#include "qlCore/SymbolCommand.h"
#include "qlGui/GraphicsItem.h"
#include "qlGui/GraphicsScene.h"

#include <QtGui/QBrush>
#include <QtGui/QFont>
#include <QtGui/QPen>
#include <QtWidgets/QGraphicsItem>

GraphicsItemFactory::GraphicsItemFactory(QObject *parent)
    : QObject(parent)
{

}

GraphicsItemFactory::~GraphicsItemFactory()
{

}

QList<QGraphicsItem *> GraphicsItemFactory::createItems(const QList<Object *> &objects)
{
    QList<QGraphicsItem *> items;
    for (auto object: objects)
        items.append(createItem(object));
    return items;
}

QGraphicsItem *GraphicsItemFactory::createItem(const Object *object)
{
    QGraphicsItem *result;
    const auto ellipse = qobject_cast<const draw::Ellipse*>(object);
    const auto rectangle = qobject_cast<const draw::Rectangle*>(object);
    const auto port = qobject_cast<const sym::Port*>(object);
    const auto label = qobject_cast<const draw::Label*>(object);
    const auto line = qobject_cast<const draw::Line*>(object);
    const auto arrow = qobject_cast<const draw::Arrow*>(object);
    if (ellipse != nullptr) {
        auto item = new GraphicsEllipseItem();
        updateEllipseItem(item, ellipse);
        result = item;
    }
    else if (rectangle != nullptr) {
        auto item = new GraphicsRectangleItem();
        updateRectangleItem(item, rectangle);
        result = item;
    }
    else if (port != nullptr) {
        auto item = new GraphicsPortItem();
        updatePortItem(item, port);
        result = item;
    }
    else if (line != nullptr) {
        auto item = new GraphicsLineItem();
        updateLineItem(item, line);
        result = item;
    }
    else if (arrow != nullptr) {
        auto item = new GraphicsArrowItem();
        updateArrowItem(item, arrow);
        result = item;
    }
    else if (label != nullptr) {
        auto item = new GraphicsLabelItem();
        updateLabelItem(item, label);
        result = item;
    }
    else {
        qWarning() << QString("Unsupported document object type: %1").arg(object->metaObject()->className());
        result = nullptr;
    }
    return result;
}

void GraphicsItemFactory::updateItem(QGraphicsItem *item, const Object *object)
{
    auto ellipseObject = qobject_cast<const draw::Ellipse*>(object);
    auto labelObject = qobject_cast<const draw::Label*>(object);
    auto lineObject = qobject_cast<const draw::Line*>(object);
    auto arrowObject = qobject_cast<const draw::Arrow*>(object);
    auto rectangleObject = qobject_cast<const draw::Rectangle*>(object);
    auto portObject = qobject_cast<const sym::Port*>(object);
    if (ellipseObject  != nullptr) {
        auto ellipseItem = qgraphicsitem_cast<GraphicsEllipseItem *>(item);
        Q_ASSERT(ellipseItem != nullptr);
        updateEllipseItem(ellipseItem, ellipseObject);
    }
    else if (rectangleObject  != nullptr) {
        auto rectangleItem = qgraphicsitem_cast<GraphicsRectangleItem *>(item);
        Q_ASSERT(rectangleItem != nullptr);
        updateRectangleItem(rectangleItem, rectangleObject);
    }
    else if (portObject  != nullptr) {
        auto portItem = qgraphicsitem_cast<GraphicsPortItem *>(item);
        Q_ASSERT(portItem != nullptr);
        updatePortItem(portItem, portObject);
    }
    else if (lineObject  != nullptr) {
        auto lineItem = qgraphicsitem_cast<GraphicsLineItem *>(item);
        Q_ASSERT(lineItem != nullptr);
        updateLineItem(lineItem, lineObject);
    }
    else if (arrowObject  != nullptr) {
        auto arrowItem = qgraphicsitem_cast<GraphicsArrowItem *>(item);
        Q_ASSERT(arrowItem != nullptr);
        updateArrowItem(arrowItem, arrowObject);
    }
    else if (labelObject  != nullptr) {
        auto labelItem = qgraphicsitem_cast<GraphicsLabelItem *>(item);
        Q_ASSERT(labelItem != nullptr);
        updateLabelItem(labelItem, labelObject);
    }
    else
        qWarning() << QString("Unsupported document object type: %1").arg(object->metaObject()->className());
}

QGraphicsItem *GraphicsItemFactory::cloneItem(QGraphicsItem *item)
{

    auto ellipseItem = qgraphicsitem_cast<GraphicsEllipseItem *>(item);
    auto rectangleItem = qgraphicsitem_cast<GraphicsRectangleItem *>(item);
    auto portItem = qgraphicsitem_cast<GraphicsPortItem *>(item);
    auto lineItem = qgraphicsitem_cast<GraphicsLineItem *>(item);
    auto arrowItem = qgraphicsitem_cast<GraphicsArrowItem *>(item);
    auto labelItem = qgraphicsitem_cast<GraphicsLabelItem *>(item);

    if (ellipseItem != nullptr) {
        auto item = ellipseItem;
        auto clone = new GraphicsEllipseItem();
        clone->setPos(item->pos());
        clone->setRotation(item->rotation());
        clone->setSize(item->size());
        clone->setStartAngle(item->startAngle());
        clone->setSpanAngle(item->spanAngle());
        clone->setStyle(item->style());
        clone->setPen(item->pen());
        clone->setBrush(item->brush());
        return clone;
    }
    else if (rectangleItem != nullptr) {
        auto item = rectangleItem;
        auto clone = new GraphicsRectangleItem();
        clone->setPos(item->pos());
        clone->setRotation(item->rotation());
        clone->setSize(item->size());
        clone->setPen(item->pen());
        clone->setBrush(item->brush());
        return clone;
    }
    else if (portItem != nullptr){
        auto item = portItem;
        auto clone = new GraphicsPortItem();
        clone->setPos(item->pos());
        clone->setRotation(item->rotation());
        clone->setName(item->name());
        clone->setPen(item->pen());
        return clone;
    }
    else if (lineItem != nullptr){
        auto item = lineItem;
        auto clone = new GraphicsLineItem();
        clone->setPos(item->pos());
        clone->setRotation(item->rotation());
        clone->setP1(item->p1());
        clone->setP2(item->p2());
        clone->setPen(item->pen());
        return clone;
    }
    else if (arrowItem != nullptr){
        auto item = arrowItem;
        auto clone = new GraphicsArrowItem();
        clone->setPos(item->pos());
        clone->setRotation(item->rotation());
        clone->setStart(item->start());
        clone->setEnd(item->end());
        clone->setStyle(item->style());
        clone->setPen(item->pen());
        return clone;
    }
    else if (labelItem != nullptr) {
        auto item = labelItem;
        auto clone = new GraphicsLabelItem();
        clone->setPos(item->pos());
        clone->setRotation(item->rotation());
        clone->setText(item->text());
        clone->setFont(item->font());
        clone->setBrush(item->brush());
        return clone;
    }
    else
        qWarning() << QString("Unsupported GraphicsItem type");

    return nullptr;

}

UndoCommand *GraphicsItemFactory::createCreateObjectCommand(const QGraphicsItem *item, UndoCommand *parent)
{
    CreateObjectCommand *command = nullptr;

    auto ellipseItem = qgraphicsitem_cast<const GraphicsEllipseItem *>(item);
    auto rectangleItem = qgraphicsitem_cast<const GraphicsRectangleItem *>(item);
    auto portItem = qgraphicsitem_cast<const GraphicsPortItem *>(item);
    auto lineItem = qgraphicsitem_cast<const GraphicsLineItem *>(item);
    auto arrowItem = qgraphicsitem_cast<const GraphicsArrowItem *>(item);
    auto labelItem = qgraphicsitem_cast<const GraphicsLabelItem *>(item);

    if (ellipseItem != nullptr) {
        command = new CreateObjectCommand(parent);
        command->setTypeName("Ellipse");
        command->setProperties(objectProperties(ellipseItem));
        command->setProperties(strokeProperties(ellipseItem->pen()));
        command->setProperties(fillProperties(ellipseItem->brush()));
        command->setProperty("size", ellipseItem->size());
        command->setProperty("startAngle", ellipseItem->startAngle());
        command->setProperty("spanAngle", ellipseItem->spanAngle());
        command->setProperty("style", ellipseItem->style());
    }
    else if (rectangleItem != nullptr) {
        command = new CreateObjectCommand(parent);
        command->setTypeName("Rectangle");
        command->setProperties(objectProperties(rectangleItem));
        command->setProperties(strokeProperties(rectangleItem->pen()));
        command->setProperties(fillProperties(rectangleItem->brush()));
        command->setProperty("size", rectangleItem->size());
    }
    else if (portItem != nullptr){
        command = new CreateObjectCommand(parent);
        command->setTypeName("Port");
        command->setProperties(objectProperties(portItem));
        command->setProperty("name", portItem->name());
    }
    else if (lineItem != nullptr){
        command = new CreateObjectCommand(parent);
        command->setTypeName("Line");
        command->setProperties(objectProperties(lineItem));
        command->setProperties(strokeProperties(lineItem->pen()));
        command->setProperty("p1", lineItem->p1());
        command->setProperty("p2", lineItem->p2());
    }
    else if (arrowItem != nullptr){
        command = new CreateObjectCommand(parent);
        command->setTypeName("Arrow");
        command->setProperties(objectProperties(arrowItem));
        command->setProperties(strokeProperties(arrowItem->pen()));
        command->setProperty("end", arrowItem->start());
        command->setProperty("start", arrowItem->end());
        command->setProperty("style", arrowItem->style());
    }
    else if (labelItem != nullptr) {
        command = new CreateObjectCommand(parent);
        command->setTypeName("Label");
        command->setProperties(objectProperties(labelItem));
        command->setProperty("size", labelItem->font().pointSizeF());
        command->setProperty("color", labelItem->brush().color());
        command->setProperty("text", labelItem->text());
    }
    else
        qWarning() << QString("Unsupported GraphicsItem type");

    return command;
}

UndoCommand *GraphicsItemFactory::createUpdateObjectCommand(const QGraphicsItem *item, const Object *object, UndoCommand *parent)
{
    auto ellipseObject = qobject_cast<const draw::Ellipse*>(object);
    auto labelObject = qobject_cast<const draw::Label*>(object);
    auto lineObject = qobject_cast<const draw::Line*>(object);
    auto arrowObject = qobject_cast<const draw::Arrow*>(object);
    auto rectangleObject = qobject_cast<const draw::Rectangle*>(object);
    auto portObject = qobject_cast<const sym::Port*>(object);

    if (ellipseObject  != nullptr) {
        auto ellipseItem = qgraphicsitem_cast<const GraphicsEllipseItem *>(item);
        Q_ASSERT(ellipseItem != nullptr);
        return createUpdateEllipseCommand(ellipseItem, ellipseObject, parent);
    }
    else if (rectangleObject  != nullptr) {
        auto rectangleItem = qgraphicsitem_cast<const GraphicsRectangleItem *>(item);
        Q_ASSERT(rectangleItem != nullptr);
        return createUpdateRectangleCommand(rectangleItem, rectangleObject, parent);
    }
    else if (portObject  != nullptr) {
        auto portItem = qgraphicsitem_cast<const GraphicsPortItem *>(item);
        Q_ASSERT(portItem != nullptr);
        return createUpdatePortCommand(portItem, portObject, parent);
    }
    else if (lineObject  != nullptr) {
        auto lineItem = qgraphicsitem_cast<const GraphicsLineItem *>(item);
        Q_ASSERT(lineItem != nullptr);
        return createUpdateLineCommand(lineItem, lineObject, parent);
    }
    else if (arrowObject  != nullptr) {
        auto arrowItem = qgraphicsitem_cast<const GraphicsArrowItem *>(item);
        Q_ASSERT(arrowItem != nullptr);
        return createUpdateArrowCommand(arrowItem, arrowObject, parent);
    }
    else if (labelObject  != nullptr) {
        auto labelItem = qgraphicsitem_cast<const GraphicsLabelItem *>(item);
        Q_ASSERT(labelItem != nullptr);
        return createUpdateLabelCommand(labelItem, labelObject, parent);
    }
    else
        qWarning() << QString("Unsupported document object type: %1").arg(object->metaObject()->className());
    return nullptr;
}

void GraphicsItemFactory::updateGraphicsItem(QGraphicsItem *item, const draw::DrawingObject *object)
{
    item->setPos(object->location());
    item->setRotation(object->rotation());
    // object->isMirrored(); // FIXME: use setTransform
    item->setVisible(object->isVisible());
    item->setEnabled(!object->isLocked());
    item->setFlag(QGraphicsItem::ItemIsSelectable);
}

void GraphicsItemFactory::updateEllipseItem(GraphicsEllipseItem *item, const draw::Ellipse *object)
{
    updateGraphicsItem(item, object);
    item->setSize(object->size());
    item->setStartAngle(object->startAngle());
    item->setSpanAngle(object->spanAngle());
    item->setStyle(object->style());
    item->setPen(createPen(object->strokeWidth(), object->strokeStyle(), object->strokeColor()));
    item->setBrush(createBrush(object->fillStyle(), object->fillColor()));
}

void GraphicsItemFactory::updateLineItem(GraphicsLineItem *item, const draw::Line *object)
{
    updateGraphicsItem(item, object);
    item->setP1(object->p1());
    item->setP2(object->p2());
    item->setPen(createPen(object->strokeWidth(), object->strokeStyle(), object->strokeColor()));
}

void GraphicsItemFactory::updateArrowItem(GraphicsArrowItem *item, const draw::Arrow *object)
{
    updateGraphicsItem(item, object);
    item->setStart(object->p1());
    item->setEnd(object->p2());
    item->setStyle(object->style());
    item->setPen(createPen(object->strokeWidth(), object->strokeStyle(), object->strokeColor()));
}

void GraphicsItemFactory::updateLabelItem(GraphicsLabelItem *item, const draw::Label *object)
{
    updateGraphicsItem(item, object);
    item->setText(object->text());
    auto font = item->font();
    font.setPointSizeF(object->size()); // Point size, pixel size, scene size?
    item->setFont(font);
    item->setBrush(object->color());
}

void GraphicsItemFactory::updateRectangleItem(GraphicsRectangleItem *item, const draw::Rectangle *object)
{
    updateGraphicsItem(item, object);
    item->setSize(object->size());
    item->setPen(createPen(object->strokeWidth(), object->strokeStyle(), object->strokeColor()));
    item->setBrush(createBrush(object->fillStyle(), object->fillColor()));
}

// FIXME: Move magic value out
void GraphicsItemFactory::updatePortItem(GraphicsPortItem *item, const sym::Port *object)
{
    updateGraphicsItem(item, object);
    item->setName(object->name());
    item->setPen(QPen(QColor("#fa0000"), 0.0)); // Original Qucs Color
    item->setZValue(100); // Ports are always on top
}

UndoCommand *GraphicsItemFactory::createUpdateEllipseCommand(const GraphicsEllipseItem *item, const draw::Ellipse *object, UndoCommand *parent)
{
    Q_UNUSED(object); // FIXME: Need to check which properties need update
    auto command = new UpdateObjectCommand(parent);
    command->setUid(object->uid());
    command->setProperties(objectProperties(item));
    command->setProperties(strokeProperties(item->pen()));
    command->setProperties(fillProperties(item->brush()));
    command->setProperty("size", item->size());
    command->setProperty("startAngle", item->startAngle());
    command->setProperty("spanAngle", item->spanAngle());
    command->setProperty("style", item->style());
    return command;
}

UndoCommand *GraphicsItemFactory::createUpdateLabelCommand(const GraphicsLabelItem *item, const draw::Label *object, UndoCommand *parent)
{
    Q_UNUSED(object); // FIXME: Need to check which properties need update
    auto command = new UpdateObjectCommand(parent);
    command->setUid(object->uid());
    command->setProperties(objectProperties(item));
    command->setProperty("size", item->font().pointSizeF());
    command->setProperty("color", item->brush().color());
    command->setProperty("text", item->text());
    return command;
}

UndoCommand *GraphicsItemFactory::createUpdateLineCommand(const GraphicsLineItem *item, const draw::Line *object, UndoCommand *parent)
{
    Q_UNUSED(object); // FIXME: Need to check which properties need update
    auto command = new UpdateObjectCommand(parent);
    command->setUid(object->uid());
    command->setProperties(objectProperties(item));
    command->setProperties(strokeProperties(item->pen()));
    command->setProperty("p1", item->p1());
    command->setProperty("p2", item->p2());
    return command;
}

UndoCommand *GraphicsItemFactory::createUpdateArrowCommand(const GraphicsArrowItem *item, const draw::Arrow *object, UndoCommand *parent)
{
    Q_UNUSED(object); // FIXME: Need to check which properties need update
    auto command = new UpdateObjectCommand(parent);
    command->setUid(object->uid());
    command->setProperties(objectProperties(item));
    command->setProperties(strokeProperties(item->pen()));
    command->setProperty("p1", item->start());
    command->setProperty("p2", item->end()); // FIXME: p1/start and p2/end
    command->setProperty("style", item->style());
    return command;
}

UndoCommand *GraphicsItemFactory::createUpdateRectangleCommand(const GraphicsRectangleItem *item, const draw::Rectangle *object, UndoCommand *parent)
{
    Q_UNUSED(object); // FIXME: Need to check which properties need update
    auto command = new UpdateObjectCommand(parent);
    command->setUid(object->uid());
    command->setProperties(objectProperties(item));
    command->setProperties(strokeProperties(item->pen()));
    command->setProperties(fillProperties(item->brush()));
    command->setProperty("size", item->size());
    return command;
}

UndoCommand *GraphicsItemFactory::createUpdatePortCommand(const GraphicsPortItem *item, const sym::Port *object, UndoCommand *parent)
{
    Q_UNUSED(object); // FIXME: Need to check which properties need update
    auto command = new UpdateObjectCommand(parent);
    command->setUid(object->uid());
    command->setProperties(objectProperties(item));
    command->setProperty("name", item->name());
    return command;
}

QPen GraphicsItemFactory::createPen(Qs::StrokeWidth width, Qs::StrokeStyle style, const QColor &color)
{
    QPen pen = GraphicsScene::defaultPen();
    switch (width) {
        case Qs::SmallestStroke:
            pen.setWidthF(0.1);
            break;
        case Qs::SmallStroke:
            pen.setWidthF(0.25);
            break;
        case Qs::MediumStroke:
            pen.setWidthF(0.5);
            break;
        case Qs::LargeStroke:
            pen.setWidthF(1.0);
            break;
        case Qs::LargestStroke:
            pen.setWidthF(2.5);
            break;
    }
    switch (style) {
        case Qs::SolidStroke:
            pen.setStyle(Qt::SolidLine);
            break;
        case Qs::DashStroke:
            pen.setStyle(Qt::DashLine);
            break;
        case Qs::DotStroke:
            pen.setStyle(Qt::DotLine);
            break;
        case Qs::DashDotStroke:
            pen.setStyle(Qt::DashDotLine);
            break;
        case Qs::DasDotDotStroke:
            pen.setStyle(Qt::DashDotDotLine);
            break;
        case Qs::NoStroke:
            pen.setStyle(Qt::NoPen);
            break;
    }
    pen.setColor(color);
    return pen;
}

QBrush GraphicsItemFactory::createBrush(Qs::FillStyle style, const QColor &color)
{
    QBrush brush = GraphicsScene::defaultBrush();
    switch (style) {
        case Qs::SolidFill:
            brush.setStyle(Qt::SolidPattern);
            break;
        case Qs::HorLineFill:
            brush.setStyle(Qt::HorPattern);
            break;
        case Qs::VerLineFill:
            brush.setStyle(Qt::VerPattern);
            break;
        case Qs::FwLineFill:
            brush.setStyle(Qt::FDiagPattern);
            break;
        case Qs::BwLineFill:
            brush.setStyle(Qt::BDiagPattern);
            break;
        case Qs::HorVerLineFill:
            brush.setStyle(Qt::CrossPattern);
            break;
        case Qs::FwBwLineFill:
            brush.setStyle(Qt::DiagCrossPattern);
            break;
        case Qs::NoFill:
            brush.setStyle(Qt::NoBrush);
            break;
    }
    brush.setColor(color);
    return brush;
}

QMap<QString, QVariant> GraphicsItemFactory::objectProperties(const QGraphicsItem *item)
{
    QMap<QString, QVariant> result;
    result.insert("location", item->pos());
    result.insert("rotation", item->rotation());
    result.insert("mirrored", false);
    result.insert("visible", true);
    result.insert("locked", false);
    return result;
}

QMap<QString, QVariant> GraphicsItemFactory::strokeProperties(const QPen &pen)
{
    QMap<QString, QVariant> result;

    Qs::StrokeStyle strokeStyle = Qs::SolidStroke;
    switch (pen.style()) {
        case Qt::NoPen:
            strokeStyle = Qs::NoStroke;
            break;
        case Qt::SolidLine:
            strokeStyle = Qs::SolidStroke;
            break;
        case Qt::DashLine:
            strokeStyle = Qs::DashStroke;
            break;
        case Qt::DotLine:
            strokeStyle = Qs::DotStroke;
            break;
        case Qt::DashDotLine:
            strokeStyle = Qs::DashDotStroke;
            break;
        case Qt::DashDotDotLine:
            strokeStyle = Qs::DasDotDotStroke;
            break;
        default:
            break;
    }

    // FIXME: Hard coded, see GraphicsItemFactory::createPen()
    Qs::StrokeWidth strokeWidth = Qs::SmallestStroke;
    if (pen.widthF() >= 2.5)
        strokeWidth = Qs::LargestStroke;
    else if (pen.widthF() >= 1.0)
        strokeWidth = Qs::LargeStroke;
    else if (pen.widthF() >= 0.5)
        strokeWidth = Qs::MediumStroke;
    else if (pen.widthF() >= 0.25)
        strokeWidth = Qs::SmallStroke;

    result.insert("strokeWidth", strokeWidth);
    result.insert("strokeStyle", strokeStyle);
    result.insert("strokeColor", pen.color());

    return result;
}

QMap<QString, QVariant> GraphicsItemFactory::fillProperties(const QBrush &brush)
{
    QMap<QString, QVariant> result;

    Qs::FillStyle fillStyle = Qs::NoFill;
    switch (brush.style()) {
        case Qt::NoBrush:
            fillStyle = Qs::NoFill;
            break;
        case Qt::SolidPattern:
            fillStyle = Qs::SolidFill;
            break;
        case Qt::HorPattern:
            fillStyle = Qs::HorLineFill;
            break;
        case Qt::VerPattern:
            fillStyle = Qs::VerLineFill;
            break;
        case Qt::CrossPattern:
            fillStyle = Qs::HorVerLineFill;
            break;
        case Qt::BDiagPattern:
            fillStyle = Qs::BwLineFill;
            break;
        case Qt::FDiagPattern:
            fillStyle = Qs::FwLineFill;
            break;
        case Qt::DiagCrossPattern:
            fillStyle = Qs::FwBwLineFill;
            break;
        default:
            break;
    }

    result.insert("fillStyle", fillStyle);
    result.insert("fillColor", brush.color());

    return result;
}
