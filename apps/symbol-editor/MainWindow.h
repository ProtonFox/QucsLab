/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include <QtWidgets/QMainWindow>

class FileSystemNavigationWidget;
class GraphicsWidget;
class SymbolDocument;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    // TODO: Refactor all these action handling
    // with Document/Editor/Action managers pattern
private slots:
    void newRequested();
    void openRequested();
    void saveRequested();
    void saveAsRequested();
    void importRequested();
    void exportRequested();
    void closeRequested();
    void exitRequested();
    void undoRequested();
    void redoRequested();
    void cutRequested();
    void copyRequested();
    void pasteRequested();
    void moveRequested();
    void deleteRequested();
    void selectAllRequested();
    void clearSelectionRequested();
    void invertSelectionRequested();
    void preferencesRequested();
    void fullscreenRequested();
    void aboutRequested();
    void aboutQtRequested();
    void toolBarsSubMenuRequested();
    void dockWidgetsSubMenuRequested();
    void updateWindowTitle();
    void openDocument(const QString &path);

private:
    QMenu *m_fileMenu;
    QAction *m_newAction;
    QAction *m_openAction;
    QAction *m_saveAction;
    QAction *m_saveAsAction;
    QAction *m_importAction;
    QAction *m_exportAction;
    QAction *m_closeAction;
    QAction *m_exitAction;
    QMenu *m_editMenu;
    QAction *m_undoAction;
    QAction *m_redoAction;
    QAction *m_cutAction;
    QAction *m_copyAction;
    QAction *m_pasteAction;
    //QAction *m_moveAction;
    QAction *m_deleteAction;
    QAction *m_selectAllAction;
    QAction *m_clearSelectionAction;
    QAction *m_inverseSelectionAction;
    QMenu *m_viewMenu;
    QMenu *m_placeMenu;
    QMenu *m_toolsMenu;
    QAction *m_preferencesAction;
    QMenu *m_windowMenu;
    QMenu *m_toolBarsMenu;
    QAction *m_toolBarsAction;
    QMenu *m_dockWidgetsMenu;
    QAction *m_dockWidgetsAction;
    QAction *m_fullscreenAction;
    QMenu *m_helpMenu;
    QAction *m_aboutAction;
    QAction *m_aboutQtAction;

    void createMenus();
    void createActions();
    void populateMenus();
    void setupFsNavigator();
    void addPlacementActions(const QList<QAction *> &actions);
    void addEditActions(const QList<QAction *> &actions);
    void addViewActions(const QList<QAction *> &actions);
    void addToolBars(const QList<QToolBar *> &toolBars);
    void addDockWidgets(const QList<QDockWidget *> &dockWidgets);
    void connectActions();

    QList<QToolBar *> toolBars() const;
    QList<QDockWidget *> dockWidgets() const;

    QString m_filePath;
    GraphicsWidget *m_editor;

    FileSystemNavigationWidget *m_fsNavigationWidget;
    QDockWidget *m_fsNavigationDockWidget;

    void setCurrentEditor(/* IEditor *editor */);

    // QWidget interface
protected:
    virtual void closeEvent(QCloseEvent *event) override;
};
