/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#pragma once

#include "qlCore/Object.h"

#include <QtWidgets/QWidget>

class DocumentOutlineModel;
class ObjectPropertyBrowser;
class ObjectProxy;
class SymbolDocument;
class UndoCommand;

class QTreeView;

class DocumentNavigationWidget : public QWidget
{
    Q_OBJECT
public:
    explicit DocumentNavigationWidget(QWidget *parent = 0);
    ~DocumentNavigationWidget();

    void setDocument(const SymbolDocument *document);
    const SymbolDocument *document() const;

    UndoCommand *takeCommand();

signals:
    void commandAvailable();

public slots:

private slots:
    void onViewSelectionChanged();
    void onDocumentSelectionChanged(const QSet<ObjectUid> &current, const QSet<ObjectUid> &previous);

private:
    void beginMonitorDocument();
    void endMonitorDocument();

    DocumentOutlineModel *m_outlineModel;
    QTreeView *m_outlineView;
    ObjectPropertyBrowser *m_propertyBrowser;
    ObjectProxy *m_propertyObjectProxy;
};
