#!/bin/sh

set -e


icons=$(find . \( -name "*.h" -o -name "*.cpp" \) -exec \
	     sed -ne 's/^.*QIcon::fromTheme("\([^"]*\)".*$/\1/gp' {} \; | sort | uniq)

from=breeze
theme=QucsLabLight
theme_qrc=assets/${theme}IconTheme.qrc

generateTheme()
{
    from=$1
    theme=$2
    theme_qrc=assets/${theme}IconTheme.qrc
    theme_index=assets/icons/${theme}/index.theme
    cat > $theme_index <<EOF
[Icon Theme]
Name=$theme
Comment=$theme ($from)
Inherits=default
Directories=22

[22]
Size=22
EOF
    cat > $theme_qrc <<EOF
<!DOCTYPE RCC>
<RCC version="1.0">
 <qresource prefix="icons/$theme">
  <file alias="index.theme">icons/$theme/index.theme</file>
EOF
    for icon in $icons;
    do
	cp /usr/share/icons/$from/actions/22/$icon.svg assets/icons/$theme/22/$icon.svg
	cat >> $theme_qrc <<EOF
   <file alias="22/$icon.svg">icons/$theme/22/$icon.svg</file>
EOF
    done
    cat >> $theme_qrc <<EOF
 </qresource>
</RCC>
EOF
}

generateTheme breeze QucsLabLight
generateTheme breeze-dark QucsLabDark
