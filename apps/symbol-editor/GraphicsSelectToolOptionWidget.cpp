/****************************************************************************
 **
 ** Copyright (C) 2017 Christian Gagneraud <chgans AT gna DOT org>
 **
 ** This file is part of QucsLab.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include "GraphicsSelectToolOptionWidget.h"
#include "GraphicsTool.h"
#include "DocumentNavigationWidget.h"

#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>

GraphicsSelectToolOptionWidget::GraphicsSelectToolOptionWidget(QWidget *parent)
    : QWidget(parent)
    , m_navWidget(new DocumentNavigationWidget())
    , m_quickActionsBar(new QToolBar())
{
    setupLayout();
    setupQuickActions();
    connect(m_navWidget, &DocumentNavigationWidget::commandAvailable,
            this, &GraphicsSelectToolOptionWidget::commandAvailable);
}

GraphicsSelectToolOptionWidget::~GraphicsSelectToolOptionWidget()
{

}

void GraphicsSelectToolOptionWidget::setTool(GraphicsSelectTool *tool)
{
    if (m_tool == tool)
        return;

    m_tool = tool;
}

GraphicsSelectTool *GraphicsSelectToolOptionWidget::tool() const
{
    return m_tool;
}

void GraphicsSelectToolOptionWidget::setDocument(SymbolDocument *document)
{
    m_navWidget->setDocument(document);
}

void GraphicsSelectToolOptionWidget::setQuickActions(const QList<QAction *> actions)
{
    m_quickActionsBar->clear();
    m_quickActionsBar->addActions(actions);
}

DocumentNavigationWidget *GraphicsSelectToolOptionWidget::navigationWidget() const
{
    return m_navWidget;
}

UndoCommand *GraphicsSelectToolOptionWidget::takeCommand()
{
    return m_navWidget->takeCommand();
}

void GraphicsSelectToolOptionWidget::setupLayout()
{
    setLayout(new QVBoxLayout);
    layout()->setContentsMargins(0,0,0,0);
    layout()->setSpacing(0);
    layout()->addWidget(m_quickActionsBar);
    layout()->addWidget(m_navWidget);
}

void GraphicsSelectToolOptionWidget::setupQuickActions()
{
    m_quickActionsBar->setWindowTitle("Selection quick actions");
}
