#!/bin/bash

set -e

qucs=/home/krys/Projects/qucs/qucs/qucs/qucs/qucs

for simu in qucsator ngspice xyce spiceopus;
do
 out=$PWD/symbols-$simu
 rm -Rf $out/*
 mkdir -p $out
 pushd $out
 $qucs -doc --$simu
 for f in */*_data.csv;
 do
  echo Converting \"$f\" for $simu ...
  python3 ../csv2xml.py "$f" "${f//_data.csv/_draw.csv}" \
    | xmllint --format - > "${f//_data.csv/.qsym}";
#  python3 ../csv2xml_prop.py "$f" "${f//_data.csv/_prop.csv}" \
#    | xmllint --format - > "${f//_data.csv/_prop.qent}" ;
 done
 find $out -name '*.csv' -exec rm {} \;
 popd
done

