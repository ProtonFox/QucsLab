<Qucs Schematic 0.0.19>
<Properties>
  <View=0,-220,1403,800,1,0,171>
  <Grid=10,10,1>
  <DataSet=rotation-vac.dat>
  <DataDisplay=rotation-vac.dpl>
  <OpenDisplay=1>
  <Script=rotation-vac.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Vac V1 1 160 160 18 -26 0 1 "1 V" 1 "1 GHz" 0 "0" 0 "0" 0>
  <Vac V4 1 540 170 -26 18 0 0 "1 V" 1 "1 GHz" 0 "0" 0 "0" 0>
  <Vac V5 1 150 360 18 -26 1 3 "1 V" 1 "1 GHz" 0 "0" 0 "0" 0>
  <Vac V6 1 300 350 -26 -56 1 0 "1 V" 1 "1 GHz" 0 "0" 0 "0" 0>
  <Vac V8 1 540 350 -26 18 1 2 "1 V" 1 "1 GHz" 0 "0" 0 "0" 0>
  <Vac V10 1 300 540 -26 18 1 2 "1 V" 1 "1 GHz" 0 "0" 0 "0" 0>
  <Vac V12 1 540 540 -26 -56 1 0 "1 V" 1 "1 GHz" 0 "0" 0 "0" 0>
  <BiasT X1 1 830 160 -26 34 0 0 "1 uH" 0 "1 uF" 0>
  <BiasT X9 1 820 510 -26 34 1 2 "1 uH" 0 "1 uF" 0>
  <BiasT X2 1 930 170 34 -26 0 1 "1 uH" 0 "1 uF" 0>
  <BiasT X3 1 1060 180 -26 -53 0 2 "1 uH" 0 "1 uF" 0>
  <BiasT X4 1 1210 170 -53 -26 0 3 "1 uH" 0 "1 uF" 0>
  <BiasT X8 1 1190 350 34 -26 1 3 "1 uH" 0 "1 uF" 0>
  <BiasT X7 1 1060 330 -26 34 1 2 "1 uH" 0 "1 uF" 0>
  <BiasT X6 1 940 340 -53 -26 1 1 "1 uH" 0 "1 uF" 0>
  <BiasT X5 1 800 350 -26 -53 1 0 "1 uH" 0 "1 uF" 0>
  <BiasT X10 1 940 510 34 -26 1 3 "1 uH" 0 "1 uF" 0>
  <BiasT X11 1 1060 520 -26 -53 1 0 "1 uH" 0 "1 uF" 0>
  <BiasT X12 1 1200 520 -62 -26 1 1 "1 uH" 0 "1 uF" 0>
  <Vac V9 1 190 540 -62 -26 1 1 "1 V" 1 "1 GHz" 0 "0" 0 "0" 0>
  <Vac V11 1 410 540 18 -26 1 3 "1 V" 1 "1 GHz" 0 "0" 0 "0" 0>
  <Vac V7 1 450 360 -62 -26 1 1 "1 V" 1 "1 GHz" 0 "0" 0 "0" 0>
  <Vac V2 1 300 180 -26 -56 0 2 "1 V" 1 "1 GHz" 0 "0" 0 "0" 0>
  <Vac V3 1 440 170 -62 -26 0 3 "1 V" 1 "1 GHz" 0 "0" 0 "0" 0>
</Components>
<Wires>
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
  <Line 100 100 540 0 #000000 0 1>
  <Line 640 620 -540 0 #000000 0 1>
  <Line 100 100 0 520 #000000 0 1>
  <Line 240 100 0 520 #000000 0 1>
  <Line 100 460 540 0 #000000 0 1>
  <Text 140 60 12 #000000 0 "0 degrees">
  <Text 270 60 12 #000000 0 "90 degrees">
  <Text 10 160 12 #000000 0 "Not mirrored">
  <Line 363 104 0 520 #000000 0 1>
  <Line 483 104 0 520 #000000 0 1>
  <Line 103 264 540 0 #000000 0 1>
  <Line 643 104 0 520 #000000 0 1>
  <Line 740 100 540 0 #000000 0 1>
  <Line 1280 620 -540 0 #000000 0 1>
  <Line 740 100 0 520 #000000 0 1>
  <Line 880 100 0 520 #000000 0 1>
  <Line 1003 104 0 520 #000000 0 1>
  <Line 1123 104 0 520 #000000 0 1>
  <Line 743 264 540 0 #000000 0 1>
  <Line 1283 104 0 520 #000000 0 1>
  <Line 740 460 540 0 #000000 0 1>
  <Text 380 60 12 #000000 0 "180 degrees">
  <Text 530 60 12 #000000 0 "270 degrees">
  <Text 10 340 12 #000000 0 "X Mirrored">
  <Text 10 540 12 #000000 0 "Y Mirrored">
  <Text 1310 560 12 #000000 0 "Y Mirrored">
  <Text 1310 390 12 #000000 0 "X Mirrored">
  <Text 1300 180 12 #000000 0 "Not mirrored">
  <Text 770 60 12 #000000 0 "0 degrees">
  <Text 890 60 12 #000000 0 "90 degrees">
  <Text 1000 60 12 #000000 0 "180 degrees">
  <Text 1150 60 12 #000000 0 "270 degrees">
  <Text 200 680 12 #000000 0 "Special symbol with the "rotate(); // fix historical flaw"">
  <Text 880 670 12 #000000 0 "Symbol without the "rotate();  // fix historical flaw"">
</Paintings>
